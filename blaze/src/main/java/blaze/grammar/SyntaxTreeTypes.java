package blaze.grammar;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

/**
 * A common reference for all of the raw node types across the interpreter.
 * Also houses some minor logic, such as which nodes are operators, etc.
 */
public class SyntaxTreeTypes {
    public static final String PROGRAM          = "PROGRAM";
    public static final String BODY             = "BODY";
    public static final String BLOCK            = "BLOCK";
    public static final String STATEMENT        = "STATEMENT";
    
    public static final String IMPORT           = "IMPORT";
    public static final String NATIVE_IMPORT    = "NATIVE_IMPORT";
    public static final String IMPORT_WILDCARD  = "*";
    public static final String RETURN           = "RETURN";
    public static final String FREEZE           = "FREEZE";
    public static final String DELETE           = "DELETE";
    public static final String PUSH             = "PUSH";
    public static final String WHEN             = "WHEN";
    
    // LITERALS
    public static final String STRING           = "STRING_LITERAL";
    public static final String DICT             = "DICT_LITERAL";
    public static final String LIST             = "LIST_LITERAL";
    public static final String CHANNEL          = "CHANNEL_LITERAL";
    public static final String PARALLEL         = "PARALLEL_LITERAL";
    public static final String INTEGER          = "INT_LITERAL";
    public static final String FLOAT            = "FLOAT_LITERAL";
    public static final String BOOLEAN          = "BOOL_LITERAL";
    public static final String NULL             = "NULL_LITERAL";
    public static final String NATIVE_OBJECT    = "NATIVE_OBJECT";
    public static final String MELTED_COPY      = "MELTED_COPY";
    public static final String BLAZE_LITERAL    = "BLAZE_LITERAL";
    
    public static final String BOOLEAN_TRUE     = "true";
    public static final String BOOLEAN_FALSE    = "false";
    
    // OTHER EXPRESSIONS
    public static final String VARIABLE             = "VARIABLE";
    public static final String ASSIGNMENT           = "ASSIGNMENT";
    public static final String LOCAL_ASSIGNMENT     = "LOCAL_ASSIGNMENT";
    public static final String LAMBDA               = "LAMBDA";
    public static final String CLOSURE              = "CLOSURE";
    public static final String FUNCTION_CALL        = "FUNCTION_CALL";
    public static final String INSTANTIATE          = "INSTANTIATE";
    public static final String FIELD_CALL           = "FIELD_CALL";
    public static final String NATIVE_REFERENCE     = "NATIVE_REF";
    public static final String TYPECAST             = "TYPECAST";
    public static final String IF_ELSE              = "IF_ELSE";
    public static final String FOR_LOOP             = "FOR_LOOP";
    public static final String WHILE_LOOP           = "WHILE_LOOP";
    public static final String GET                  = "GET";
    public static final String PUT                  = "PUT";
    public static final String GET_FIELD            = "GET_FIELD";
    public static final String PUT_FIELD            = "PUT_FIELD";
    public static final String PROTOTYPE_PUT_FIELD  = "PROTOTYPE_PUT_FIELD";
    public static final String PARALLEL_EXPRESSION  = "PARALLEL_EXPRESSION";
    public static final String WITH                 = "WITH";
    public static final String PARALLELIZE          = "PARALLELIZE";
    
    // AST NODES FOR DESUGARING
    public static final String EXPR_LIST        = "EXPR_LIST";
    public static final String ARG_LIST         = "ARG_LIST";
    public static final String VARARG           = "VARARG";
    public static final String FIELD_CALL_IDS   = "FIELD_CALL_IDS";
    public static final String CALL_PARAMS      = "CALL_PARAMS";
    
    // UNARY OPERATORS
    // Arithmetic
    public static final String UNARY_PLUS       = "+";
    public static final String UNARY_MINUS      = "-";
    // Logic
    public static final String UNARY_NOT        = "not";
    
    
    // BINARY OPERATORS
    // Arithmetic
    public static final String BINARY_PLUS      = "+";
    public static final String BINARY_MINUS     = "-";
    public static final String BINARY_TIMES     = "*";
    public static final String BINARY_DIVIDE    = "/";
    public static final String BINARY_MOD       = "%";
    // Logic
    public static final String BINARY_EQ        = "==";
    public static final String BINARY_NEQ       = "!=";
    public static final String BINARY_LT        = "<";
    public static final String BINARY_LTE       = "<=";
    public static final String BINARY_GT        = ">";
    public static final String BINARY_GTE       = ">=";
    public static final String BINARY_AND       = "and";
    public static final String BINARY_OR        = "or";
    // Compound assignment
    public static final String BINARY_COMPOUND_PLUS     =   "+=";
    public static final String BINARY_COMPOUND_MINUS    =   "-=";
    public static final String BINARY_COMPOUND_TIMES    =   "*=";
    public static final String BINARY_COMPOUND_DIVIDE   =   "/=";
    public static final String BINARY_COMPOUND_MOD      =   "%=";
    
    // Sets for quickly identifying a given node type string
    // (HashSets have constant time search functions)
    private static final Set<String> UNARY_OPS = new HashSet<String>(Arrays.asList(new String[] {
        UNARY_PLUS,
        UNARY_MINUS,
        UNARY_NOT
    }));
    private static final Set<String> BINARY_OPS = new HashSet<String>(Arrays.asList(new String[] {
        BINARY_PLUS,
        BINARY_MINUS,
        BINARY_TIMES,
        BINARY_DIVIDE,
        BINARY_MOD,
        BINARY_EQ,
        BINARY_NEQ,
        BINARY_LT,
        BINARY_LTE,
        BINARY_GT,
        BINARY_GTE,
        BINARY_AND,
        BINARY_OR
    }));
    
    private static final Map<String, String> BLAZE_TYPES;
    
    static {
        BLAZE_TYPES = new HashMap<String, String>();
        BLAZE_TYPES.put(STRING,         "str");
        BLAZE_TYPES.put(INTEGER,        "int");
        BLAZE_TYPES.put(FLOAT,          "float");
        BLAZE_TYPES.put(BOOLEAN,        "bool");
        BLAZE_TYPES.put(NULL,           "nulltype");
        BLAZE_TYPES.put(DICT,           "dict");
        BLAZE_TYPES.put(LIST,           "list");
        BLAZE_TYPES.put(CLOSURE,        "closure");
        BLAZE_TYPES.put(CHANNEL,        "channel");
        BLAZE_TYPES.put(PARALLEL,       "parallel");
        BLAZE_TYPES.put(NATIVE_OBJECT,  "native");
        BLAZE_TYPES.put(BLAZE_LITERAL,  "__blaze_literal");
    }
    
    /**
     * Returns the Blaze type for a given AST node type.
     * @param   type    An AST node type, such as "STRING_LITERAL"
     * @return          The Blaze representation, such as "str"
     */
    public static String getBlazeType(String type) {
        return BLAZE_TYPES.get(type);
    }
    
    /**
     * The inverse operation of getBlazeType.
     * Looks up the AST node type for the Blaze type.
     * @param   type    A Blaze type, like "str"
     * @return          AST node type, like "STRING_LITERAL"
     */
    public static String getNodeTypeFromBlazeType(String type) {
        for(String k : BLAZE_TYPES.keySet())
            if(BLAZE_TYPES.get(k).equals(type))
                return k;
        return null;
    }
    
    public static boolean isUnaryOperator(String type) {
        return UNARY_OPS.contains(type);
    }
    
    public static boolean isBinaryOperator(String type) {
        return BINARY_OPS.contains(type);
    }
}