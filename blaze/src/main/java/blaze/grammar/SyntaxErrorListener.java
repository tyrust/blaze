package blaze.grammar;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;

/**
 * Custom error listener for ANTLR parser errors.
 * Prints a description of the syntax error to standard error.
 * Highlights the problematic token using carets.
 */
public class SyntaxErrorListener extends BaseErrorListener {
    /**
     * Gets called when a syntax error occurs.
     */
    @Override
    public void syntaxError(Recognizer<?,?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException ex) {
        System.err.println(String.format("Syntax error (line %d:%d):", line, charPositionInLine));
        System.err.println(msg);
        
        CommonTokenStream tokens = (CommonTokenStream)recognizer.getInputStream();
        String input = tokens.getTokenSource().getInputStream().toString();
        String[] lines = input.split("\n");
        String errorLine = lines[line-1];
        System.err.println("\t" + errorLine);
        System.err.print("\t");
        for(int i = 0; i < charPositionInLine; i++)
            System.err.print(" ");
        
        Token token = (Token)offendingSymbol;
        int start = token.getStartIndex();
        int end = token.getStopIndex();
        if(start >= 0 && end >= 0)
            for(int i = start; i <= end; i++)
                System.err.print("^");
        System.err.println();
    }
}