// Generated from grammar/Blaze.g4 by ANTLR 4.0
 package blaze.grammar; 
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.ParserRuleContext;

public class BlazeBaseVisitor<T> extends AbstractParseTreeVisitor<T> implements BlazeVisitor<T> {
	@Override public T visitExpression(BlazeParser.ExpressionContext ctx) { return visitChildren(ctx); }

	@Override public T visitAtom(BlazeParser.AtomContext ctx) { return visitChildren(ctx); }

	@Override public T visitChainCall(BlazeParser.ChainCallContext ctx) { return visitChildren(ctx); }

	@Override public T visitBody(BlazeParser.BodyContext ctx) { return visitChildren(ctx); }

	@Override public T visitLocalVariableAssignment(BlazeParser.LocalVariableAssignmentContext ctx) { return visitChildren(ctx); }

	@Override public T visitDereferenceIndexable(BlazeParser.DereferenceIndexableContext ctx) { return visitChildren(ctx); }

	@Override public T visitReturnStatement(BlazeParser.ReturnStatementContext ctx) { return visitChildren(ctx); }

	@Override public T visitMultiCall(BlazeParser.MultiCallContext ctx) { return visitChildren(ctx); }

	@Override public T visitLiteralIndexable(BlazeParser.LiteralIndexableContext ctx) { return visitChildren(ctx); }

	@Override public T visitTypecast(BlazeParser.TypecastContext ctx) { return visitChildren(ctx); }

	@Override public T visitCallParameters(BlazeParser.CallParametersContext ctx) { return visitChildren(ctx); }

	@Override public T visitNativeImport(BlazeParser.NativeImportContext ctx) { return visitChildren(ctx); }

	@Override public T visitVariableAssignment(BlazeParser.VariableAssignmentContext ctx) { return visitChildren(ctx); }

	@Override public T visitWithStatement(BlazeParser.WithStatementContext ctx) { return visitChildren(ctx); }

	@Override public T visitPackageDeclaration(BlazeParser.PackageDeclarationContext ctx) { return visitChildren(ctx); }

	@Override public T visitFullLambda(BlazeParser.FullLambdaContext ctx) { return visitChildren(ctx); }

	@Override public T visitFieldLookupVariable(BlazeParser.FieldLookupVariableContext ctx) { return visitChildren(ctx); }

	@Override public T visitZeroArgumentLambda(BlazeParser.ZeroArgumentLambdaContext ctx) { return visitChildren(ctx); }

	@Override public T visitStatement(BlazeParser.StatementContext ctx) { return visitChildren(ctx); }

	@Override public T visitTypecastIndexable(BlazeParser.TypecastIndexableContext ctx) { return visitChildren(ctx); }

	@Override public T visitProgram(BlazeParser.ProgramContext ctx) { return visitChildren(ctx); }

	@Override public T visitDictLiteral(BlazeParser.DictLiteralContext ctx) { return visitChildren(ctx); }

	@Override public T visitCallableCall(BlazeParser.CallableCallContext ctx) { return visitChildren(ctx); }

	@Override public T visitSeparator(BlazeParser.SeparatorContext ctx) { return visitChildren(ctx); }

	@Override public T visitPrototypeAssignment(BlazeParser.PrototypeAssignmentContext ctx) { return visitChildren(ctx); }

	@Override public T visitBlazeType(BlazeParser.BlazeTypeContext ctx) { return visitChildren(ctx); }

	@Override public T visitRelativeImport(BlazeParser.RelativeImportContext ctx) { return visitChildren(ctx); }

	@Override public T visitVariableCompoundAssignment(BlazeParser.VariableCompoundAssignmentContext ctx) { return visitChildren(ctx); }

	@Override public T visitBlock(BlazeParser.BlockContext ctx) { return visitChildren(ctx); }

	@Override public T visitBlockOrOneLiner(BlazeParser.BlockOrOneLinerContext ctx) { return visitChildren(ctx); }

	@Override public T visitIndexLookupVariable(BlazeParser.IndexLookupVariableContext ctx) { return visitChildren(ctx); }

	@Override public T visitPush(BlazeParser.PushContext ctx) { return visitChildren(ctx); }

	@Override public T visitIndexDelete(BlazeParser.IndexDeleteContext ctx) { return visitChildren(ctx); }

	@Override public T visitCallable(BlazeParser.CallableContext ctx) { return visitChildren(ctx); }

	@Override public T visitNativeCall(BlazeParser.NativeCallContext ctx) { return visitChildren(ctx); }

	@Override public T visitMeltedCopy(BlazeParser.MeltedCopyContext ctx) { return visitChildren(ctx); }

	@Override public T visitNativeInstantiation(BlazeParser.NativeInstantiationContext ctx) { return visitChildren(ctx); }

	@Override public T visitParallelLiteral(BlazeParser.ParallelLiteralContext ctx) { return visitChildren(ctx); }

	@Override public T visitWhileLoop(BlazeParser.WhileLoopContext ctx) { return visitChildren(ctx); }

	@Override public T visitParallelize(BlazeParser.ParallelizeContext ctx) { return visitChildren(ctx); }

	@Override public T visitListLiteral(BlazeParser.ListLiteralContext ctx) { return visitChildren(ctx); }

	@Override public T visitWhenStatement(BlazeParser.WhenStatementContext ctx) { return visitChildren(ctx); }

	@Override public T visitIndexableVariable(BlazeParser.IndexableVariableContext ctx) { return visitChildren(ctx); }

	@Override public T visitOneLineLambda(BlazeParser.OneLineLambdaContext ctx) { return visitChildren(ctx); }

	@Override public T visitForLoop(BlazeParser.ForLoopContext ctx) { return visitChildren(ctx); }

	@Override public T visitArgumentList(BlazeParser.ArgumentListContext ctx) { return visitChildren(ctx); }

	@Override public T visitExpressionIndexable(BlazeParser.ExpressionIndexableContext ctx) { return visitChildren(ctx); }

	@Override public T visitIfStatement(BlazeParser.IfStatementContext ctx) { return visitChildren(ctx); }

	@Override public T visitNativeIndexable(BlazeParser.NativeIndexableContext ctx) { return visitChildren(ctx); }

	@Override public T visitFreezeStatement(BlazeParser.FreezeStatementContext ctx) { return visitChildren(ctx); }

	@Override public T visitNativeReference(BlazeParser.NativeReferenceContext ctx) { return visitChildren(ctx); }

	@Override public T visitFunctionDeclaration(BlazeParser.FunctionDeclarationContext ctx) { return visitChildren(ctx); }

	@Override public T visitLibraryImport(BlazeParser.LibraryImportContext ctx) { return visitChildren(ctx); }

	@Override public T visitFieldCall(BlazeParser.FieldCallContext ctx) { return visitChildren(ctx); }

	@Override public T visitFieldDelete(BlazeParser.FieldDeleteContext ctx) { return visitChildren(ctx); }

	@Override public T visitLiteral(BlazeParser.LiteralContext ctx) { return visitChildren(ctx); }
}