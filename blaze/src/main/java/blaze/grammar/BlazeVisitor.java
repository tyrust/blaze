// Generated from grammar/Blaze.g4 by ANTLR 4.0
 package blaze.grammar; 
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface BlazeVisitor<T> extends ParseTreeVisitor<T> {
	T visitExpression(BlazeParser.ExpressionContext ctx);

	T visitAtom(BlazeParser.AtomContext ctx);

	T visitChainCall(BlazeParser.ChainCallContext ctx);

	T visitBody(BlazeParser.BodyContext ctx);

	T visitLocalVariableAssignment(BlazeParser.LocalVariableAssignmentContext ctx);

	T visitDereferenceIndexable(BlazeParser.DereferenceIndexableContext ctx);

	T visitReturnStatement(BlazeParser.ReturnStatementContext ctx);

	T visitMultiCall(BlazeParser.MultiCallContext ctx);

	T visitLiteralIndexable(BlazeParser.LiteralIndexableContext ctx);

	T visitTypecast(BlazeParser.TypecastContext ctx);

	T visitCallParameters(BlazeParser.CallParametersContext ctx);

	T visitNativeImport(BlazeParser.NativeImportContext ctx);

	T visitVariableAssignment(BlazeParser.VariableAssignmentContext ctx);

	T visitWithStatement(BlazeParser.WithStatementContext ctx);

	T visitPackageDeclaration(BlazeParser.PackageDeclarationContext ctx);

	T visitFullLambda(BlazeParser.FullLambdaContext ctx);

	T visitFieldLookupVariable(BlazeParser.FieldLookupVariableContext ctx);

	T visitZeroArgumentLambda(BlazeParser.ZeroArgumentLambdaContext ctx);

	T visitStatement(BlazeParser.StatementContext ctx);

	T visitTypecastIndexable(BlazeParser.TypecastIndexableContext ctx);

	T visitProgram(BlazeParser.ProgramContext ctx);

	T visitDictLiteral(BlazeParser.DictLiteralContext ctx);

	T visitCallableCall(BlazeParser.CallableCallContext ctx);

	T visitSeparator(BlazeParser.SeparatorContext ctx);

	T visitPrototypeAssignment(BlazeParser.PrototypeAssignmentContext ctx);

	T visitBlazeType(BlazeParser.BlazeTypeContext ctx);

	T visitRelativeImport(BlazeParser.RelativeImportContext ctx);

	T visitVariableCompoundAssignment(BlazeParser.VariableCompoundAssignmentContext ctx);

	T visitBlock(BlazeParser.BlockContext ctx);

	T visitBlockOrOneLiner(BlazeParser.BlockOrOneLinerContext ctx);

	T visitIndexLookupVariable(BlazeParser.IndexLookupVariableContext ctx);

	T visitPush(BlazeParser.PushContext ctx);

	T visitIndexDelete(BlazeParser.IndexDeleteContext ctx);

	T visitCallable(BlazeParser.CallableContext ctx);

	T visitNativeCall(BlazeParser.NativeCallContext ctx);

	T visitMeltedCopy(BlazeParser.MeltedCopyContext ctx);

	T visitNativeInstantiation(BlazeParser.NativeInstantiationContext ctx);

	T visitParallelLiteral(BlazeParser.ParallelLiteralContext ctx);

	T visitWhileLoop(BlazeParser.WhileLoopContext ctx);

	T visitParallelize(BlazeParser.ParallelizeContext ctx);

	T visitListLiteral(BlazeParser.ListLiteralContext ctx);

	T visitWhenStatement(BlazeParser.WhenStatementContext ctx);

	T visitIndexableVariable(BlazeParser.IndexableVariableContext ctx);

	T visitOneLineLambda(BlazeParser.OneLineLambdaContext ctx);

	T visitForLoop(BlazeParser.ForLoopContext ctx);

	T visitArgumentList(BlazeParser.ArgumentListContext ctx);

	T visitExpressionIndexable(BlazeParser.ExpressionIndexableContext ctx);

	T visitIfStatement(BlazeParser.IfStatementContext ctx);

	T visitNativeIndexable(BlazeParser.NativeIndexableContext ctx);

	T visitFreezeStatement(BlazeParser.FreezeStatementContext ctx);

	T visitNativeReference(BlazeParser.NativeReferenceContext ctx);

	T visitFunctionDeclaration(BlazeParser.FunctionDeclarationContext ctx);

	T visitLibraryImport(BlazeParser.LibraryImportContext ctx);

	T visitFieldCall(BlazeParser.FieldCallContext ctx);

	T visitFieldDelete(BlazeParser.FieldDeleteContext ctx);

	T visitLiteral(BlazeParser.LiteralContext ctx);
}