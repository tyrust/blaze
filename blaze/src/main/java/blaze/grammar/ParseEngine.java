package blaze.grammar;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import blaze.grammar.desugarers.DesugarVisitor;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.literals.NullLiteral;

import blaze.runtime.Configuration;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.FileReader;
import java.io.StreamCorruptedException;
import java.io.IOException;

/**
 * High-level interface to the lexer and parser
 * Translates raw Blaze code to a tree of BlazeExpressions
 */
public class ParseEngine {
    /**
     * Runs the lexer on the given program to tokenize it.
     * Runs the parser on the tokenized program to come up with an abstract syntax tree.
     * Runs the syntax tree through desugaring.
     * Then translates the resultant tree into a tree of BlazeExpressions.
     */
    private static BlazeExpression parse(CharStream inputStream) throws SyntaxError {
        try {
            // Calls the lexer on the input to generate a stream of tokens
            BlazeLexer lexer = new BlazeLexer(inputStream);
            TokenStream tokenStream = new CommonTokenStream(lexer);
            
            // Instantiate the parser on the token stream
            BlazeParser parser = new BlazeParser(tokenStream);
            
            // Install our custom error handling
            parser.removeErrorListeners();
            parser.addErrorListener(new SyntaxErrorListener());
            parser.setErrorHandler(new BailErrorStrategy());
            
            // `program` is the root node of the AST
            BlazeParser.ProgramContext program = parser.program();
            
            if(Configuration.PRINT_AST)
                System.out.println("AST = " + program.toStringTree(parser));
            
            DesugarVisitor visitor = new DesugarVisitor();
            return visitor.visit(program);
        } catch(RecognitionException ex) {
            // Indicates that the input was malformed in some way
            throw new SyntaxError(ex.getMessage());
        } catch(ParseCancellationException ex) {
            throw new SyntaxError(ex.getMessage());
        }
        // Possibly also throws other syntax errors, not handled here.
    }
    
    /**
     * Runs the lexer/parser on the given code.
     * @param   code    Input program, written in Blaze.
     * @return          A single BlazeExpression, representing the root of the tree.
     */
    public static BlazeExpression parseCode(String code) throws SyntaxError {
        CharStream inputStream = new ANTLRInputStream(code);
        return parse(inputStream);
    }
    
    /**
     * Runs the lexer/parser on the contents of a file.
     * @param   filePath    Path to the source code file.
     * @return              The BlazeExpression for the root of the AST.
     */
    public static BlazeExpression parseCodeFile(String filePath) throws SyntaxError {
        try {
            try {
                FileInputStream fileIn = new FileInputStream(filePath);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                BlazeExpression program = (BlazeExpression)in.readObject();
                in.close();
                fileIn.close();
                return program;
            } catch(StreamCorruptedException ex) {
                CharStream inputStream = new ANTLRInputStream(new FileReader(filePath));
                return parse(inputStream);
            } catch(IOException ex) {
                throw ex;
            } catch(ClassNotFoundException ex) {
                throw ex;
            }
        } catch(Exception ex) {
            // Well, this isn't exactly a syntax error...
            ex.printStackTrace();
            throw new SyntaxError("Cannot open file: " + filePath);
        }
    }
}