package blaze.grammar.desugarers;

import org.antlr.v4.runtime.tree.TerminalNode;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.VariableExpression;
import blaze.interpreter.expressions.GetExpression;
import blaze.interpreter.environment.Identifier;

/**
 * Desugars a variable lookup into an environment tree search.
 * "Variable" here is somewhat general; this also includes field lookups.
 */
public class VariableDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.DereferenceIndexableContext context, DesugarVisitor visitor) {
        Identifier id = new Identifier(context.ID().getText());
        return new VariableExpression(id);
    }
    
    public static BlazeExpression desugar(BlazeParser.IndexLookupVariableContext context, DesugarVisitor visitor) {
        BlazeParser.VariableContext parentCtx = context.variable();
        BlazeParser.ExpressionContext indexCtx = context.expression();
        BlazeExpression parent = visitor.visit(parentCtx);
        BlazeExpression index = visitor.visit(indexCtx);
        return new GetExpression(parent, index);
    }
    
    public static BlazeExpression desugar(BlazeParser.FieldLookupVariableContext context, DesugarVisitor visitor) {
        BlazeParser.VariableContext parentCtx = context.variable();
        TerminalNode varCtx = context.ID();
        Identifier id = new Identifier(varCtx.getText());
        
        if(parentCtx != null) {
            BlazeExpression parent = visitor.visit(parentCtx);
            return new VariableExpression(id, parent);
        }
        
        return new VariableExpression(id);
    }
}