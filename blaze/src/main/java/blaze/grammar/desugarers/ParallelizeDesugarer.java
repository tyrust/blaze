package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.ParallelizeExpression;
import blaze.interpreter.expressions.FunctionCallExpression;
import blaze.interpreter.environment.Identifier;

import java.util.ArrayList;

/**
 * Desugars a parallelization into an expression.
 */
public class ParallelizeDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.ParallelizeContext context, DesugarVisitor visitor) {
        ParallelizeExpression parallelizeClosure = new ParallelizeExpression(visitor.visit(context.expression()));
        return new FunctionCallExpression(parallelizeClosure, new ArrayList<BlazeExpression>());
    }
}