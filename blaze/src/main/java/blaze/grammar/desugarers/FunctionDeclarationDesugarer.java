package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BodyExpression;
import blaze.interpreter.expressions.AssignmentExpression;
import blaze.interpreter.expressions.ReturnStatement;

import java.util.List;
import java.util.ArrayList;

/**
 * Desugars a function declaration into an assignment involving a lambda.
 */
public class FunctionDeclarationDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.FunctionDeclarationContext context, DesugarVisitor visitor) {
        BlazeExpression variable = visitor.visit(context.variable());
        BlazeExpression body;
        if(context.expression() != null) {
            BlazeExpression expr = visitor.visit(context.expression());
            ReturnStatement ret = new ReturnStatement(expr);
            List<BlazeExpression> statements = new ArrayList<BlazeExpression>();
            statements.add(ret);
            body = new BodyExpression(statements, true);
        } else
            body = visitor.visit(context.body());
        BlazeExpression lambda = LambdaDesugarer.createLambda(context.argumentList(), body);
        return new AssignmentExpression(variable, lambda);
    }
}