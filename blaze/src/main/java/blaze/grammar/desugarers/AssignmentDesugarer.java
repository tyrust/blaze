package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.AssignmentExpression;
import blaze.interpreter.expressions.PutExpression;
import blaze.interpreter.environment.Identifier;

/**
 * Desugars a variable assignment into an environment binding operation.
 */
public class AssignmentDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.PrototypeAssignmentContext context, DesugarVisitor visitor) {
        String type = SyntaxTreeTypes.getNodeTypeFromBlazeType(context.blazeType().getText());
        Identifier id = new Identifier(context.ID().getText());
        BlazeExpression val = visitor.visit(context.expression());
        return new PutExpression(type, id, val);
    }
    
    public static BlazeExpression desugar(BlazeParser.VariableCompoundAssignmentContext context, DesugarVisitor visitor) {
        BinaryOperator operator = BinaryOperator.getOperatorForCompoundOperator(context.operator.getText());
        BlazeExpression var = visitor.visit(context.variable());
        BlazeExpression val = visitor.visit(context.expression());
        return new AssignmentExpression(var, val, operator);
    }
    
    public static BlazeExpression desugar(BlazeParser.LocalVariableAssignmentContext context, DesugarVisitor visitor) {
        Identifier id = new Identifier(context.ID().getText(), true);
        BlazeExpression val = visitor.visit(context.expression());
        return new AssignmentExpression(id, val);
    }
    
    public static BlazeExpression desugar(BlazeParser.VariableAssignmentContext context, DesugarVisitor visitor) {
        BlazeExpression var = visitor.visit(context.variable());
        BlazeExpression val = visitor.visit(context.expression());
        return new AssignmentExpression(var, val);
    }
}