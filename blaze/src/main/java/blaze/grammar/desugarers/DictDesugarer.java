package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.DictExpression;

import java.util.List;
import java.util.LinkedList;

/**
 * Desugars dict literal nodes into literal dict expressions.
 */
public class DictDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.DictLiteralContext context, DesugarVisitor visitor) {
        List<BlazeExpression> keys = new LinkedList<BlazeExpression>();
        List<BlazeExpression> vals = new LinkedList<BlazeExpression>();
        if(context.keys != null) {
            for(int i = 0; i < context.keys.size(); i++) {
                BlazeExpression key = visitor.visit(context.keys.get(i));
                BlazeExpression val = visitor.visit(context.values.get(i));
                keys.add(key);
                vals.add(val);
            }
        }
        return new DictExpression(keys, vals);
    }
}