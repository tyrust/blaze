package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.ParallelExpression;

import java.util.List;
import java.util.ArrayList;

/**
 * Desugars parallel literal nodes into parallel expressions.
 */
public class ParallelDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.ParallelLiteralContext context, DesugarVisitor visitor) {
        List<BlazeExpression> list = new ArrayList<BlazeExpression>();
        for(BlazeParser.ExpressionContext exprContext : context.expression())
            list.add(visitor.visit(exprContext));
        return new ParallelExpression(list);
    }
}