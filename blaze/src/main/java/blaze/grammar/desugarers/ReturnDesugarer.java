package blaze.grammar.desugarers;

import blaze.grammar.SyntaxError;
import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.ReturnStatement;

/**
 * Desugars return statement tree nodes into return expressions.
 */
public class ReturnDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.ReturnStatementContext context, DesugarVisitor visitor) {
        BlazeParser.ExpressionContext exprContext = context.expression();
        if(exprContext == null)
            return new ReturnStatement();
        BlazeExpression expr = visitor.visit(exprContext);
        return new ReturnStatement(expr);
    }
}