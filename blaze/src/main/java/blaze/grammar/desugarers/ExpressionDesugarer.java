package blaze.grammar.desugarers;

import org.antlr.v4.runtime.ParserRuleContext;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.BinaryOperationExpression;
import blaze.interpreter.expressions.UnaryOperationExpression;

/**
 * Desugars unary/binary operator expressions into runtime-evaluatable expressions.
 */
public class ExpressionDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.ExpressionContext context, DesugarVisitor visitor) {
        // There must always be an RHS
        BlazeExpression rhs = visitor.visit(context.rhs);
        
        // If there's no LHS, this is a unary operator
        if(context.lhs == null) {
            UnaryOperator operator = UnaryOperator.getOperatorForToken(context.operator.getText());
            return new UnaryOperationExpression(operator, rhs);
        }
        
        // Otherwise, it's a binary operator
        BinaryOperator operator = BinaryOperator.getOperatorForToken(context.operator.getText());
        BlazeExpression lhs = visitor.visit(context.lhs);
        return new BinaryOperationExpression(operator, lhs, rhs);
    }
}