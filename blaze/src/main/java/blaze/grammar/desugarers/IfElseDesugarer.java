package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.IfStatement;

/**
 * Desugars if/else trees into if/else expressions.
 */
public class IfElseDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.IfStatementContext context, DesugarVisitor visitor) {
        // These two must be present in a valid if statement
        BlazeExpression predicate = visitor.visit(context.expression());
        BlazeExpression consequent = visitor.visit(context.consequent);
        // This is optional, however, as we can have ifs without elses
        BlazeExpression alternative = null;
        if(context.alternative != null)
            alternative = visitor.visit(context.alternative);
        return new IfStatement(predicate, consequent, alternative);
    }
}