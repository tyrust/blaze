package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.ForLoopStatement;
import blaze.interpreter.expressions.WhileLoopStatement;
import blaze.interpreter.environment.Identifier;

import java.util.List;
import java.util.ArrayList;

public class LoopDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.ForLoopContext context, DesugarVisitor visitor) {
        BlazeExpression iterable = visitor.visit(context.expression());
        Identifier iterVar = new Identifier(context.var.getText(), true);
        BlazeExpression body = visitor.visit(context.blockOrOneLiner());
        return new ForLoopStatement(iterVar, iterable, body);
    }

    public static BlazeExpression desugar(BlazeParser.WhileLoopContext context, DesugarVisitor visitor) {
        BlazeExpression condition = visitor.visit(context.expression());
        BlazeExpression body = visitor.visit(context.blockOrOneLiner());
        return new WhileLoopStatement(condition, body);
    }
}