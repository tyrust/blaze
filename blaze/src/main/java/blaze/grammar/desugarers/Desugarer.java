package blaze.grammar.desugarers;

import org.antlr.runtime.tree.CommonTree;

import blaze.grammar.SyntaxError;
import blaze.interpreter.expressions.BlazeExpression;

/**
 * Represents an abstract desugarer, which maps raw AST to BlazeExpressions
 */
public abstract class Desugarer {
    // It is assumed going into this function that `tree` has the correct type for this desugarer
    // This invariant is maintained by the DesugarEngine
    // You should not call these desugarers directly, as this may put the interpreter in an invalid state
    public static BlazeExpression desugar(CommonTree tree, DesugarVisitor visitor) {
        return null;
    }
}