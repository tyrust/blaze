package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.InstantiationExpression;

import java.util.List;
import java.util.ArrayList;

/**
 * Desugars instantiations into instantiation expressions.
 */
public class InstantiationDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.NativeInstantiationContext context, DesugarVisitor visitor) {
        BlazeExpression clazz = visitor.visit(context.nativeReference());
        List<BlazeExpression> params;
        if(context.callParameters() != null)
            params = FunctionCallDesugarer.createParamsList(context.callParameters().expression(), visitor);
        else
            params = new ArrayList<BlazeExpression>();
        return new InstantiationExpression(clazz, params);
    }
}