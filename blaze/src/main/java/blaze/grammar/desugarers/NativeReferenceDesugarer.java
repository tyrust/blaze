package blaze.grammar.desugarers;

import org.antlr.v4.runtime.tree.TerminalNode;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.NativeReferenceExpression;

import java.util.List;
import java.util.LinkedList;

/**
 * Desugars native reference trees into native literal expressions.
 */
public class NativeReferenceDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.NativeReferenceContext context, DesugarVisitor visitor) {
        List<String> packageComponents = new LinkedList<String>();
        for(TerminalNode component : context.packageDeclaration().ID())
            packageComponents.add(component.getText());
        return new NativeReferenceExpression(packageComponents);
    }
}