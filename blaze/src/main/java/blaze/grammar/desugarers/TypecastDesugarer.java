package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.TypecastExpression;

/**
 * Desugars a typecast of the form (type)expr to a call to expr.coerceTo(type)
 */
public class TypecastDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.TypecastContext context, DesugarVisitor visitor) {
        String type = SyntaxTreeTypes.getNodeTypeFromBlazeType(context.blazeType().getText());
        BlazeExpression expr = visitor.visit(context.expression());
        return new TypecastExpression(type, expr);
    }
}