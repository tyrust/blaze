package blaze.grammar.desugarers;

import org.antlr.v4.runtime.Token;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BodyExpression;
import blaze.interpreter.expressions.LambdaExpression;
import blaze.interpreter.expressions.ReturnStatement;
import blaze.interpreter.environment.Identifier;

import java.util.List;
import java.util.LinkedList;

/**
 * Desugars lambda trees into lambda expressions.
 */
public class LambdaDesugarer extends Desugarer {
    private static List<Identifier> createArgumentList(List<Token> nodeList) {
        List<Identifier> args = new LinkedList<Identifier>();
        for(Token node : nodeList)
            args.add(new Identifier(node.getText(), true));
        return args;
    }
    
    private static Identifier getVarargIdentifier(Token varargTok) {
        Identifier vararg = null;
        if(varargTok != null)
            vararg = new Identifier(varargTok.getText(), true);
        return vararg;
    }
    
    public static BlazeExpression createLambda(BlazeParser.ArgumentListContext argCtx, BlazeExpression body) {
        List<Identifier> args = createArgumentList(argCtx.args);
        Identifier vararg = getVarargIdentifier(argCtx.vararg);
        return new LambdaExpression(args, vararg, body);
    }
    
    public static BlazeExpression desugar(BlazeParser.ZeroArgumentLambdaContext context, DesugarVisitor visitor) {
        BlazeExpression body = visitor.visit(context.body());
        return new LambdaExpression(new LinkedList<Identifier>(), null, body);
    }
    
    public static BlazeExpression desugar(BlazeParser.OneLineLambdaContext context, DesugarVisitor visitor) {
        BlazeExpression expr = visitor.visit(context.expression());
        // wrap the one line expression in a return statement
        // essentially, desugar lambdas like x->x into x->{return x}
        ReturnStatement ret = new ReturnStatement(expr);
        // have to wrap the return in a body, otherwise it'll return too far
        List<BlazeExpression> stmts = new LinkedList<BlazeExpression>();
        stmts.add(ret);
        BodyExpression body = new BodyExpression(stmts, true);
        return createLambda(context.argumentList(), body);
    }
    
    public static BlazeExpression desugar(BlazeParser.FullLambdaContext context, DesugarVisitor visitor) {
        BlazeExpression body = visitor.visit(context.body());
        return createLambda(context.argumentList(), body);
    }
}