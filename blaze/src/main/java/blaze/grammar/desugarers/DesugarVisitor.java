package blaze.grammar.desugarers;

import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.ParserRuleContext;

import blaze.grammar.SyntaxTreeTypes;
import blaze.grammar.BlazeBaseVisitor;
import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.ImportStatement;
import blaze.interpreter.expressions.FreezeStatement;
import blaze.interpreter.expressions.MeltedCopyExpression;
import blaze.interpreter.expressions.DeleteStatement;
import blaze.interpreter.expressions.BodyExpression;
import blaze.interpreter.expressions.literals.*;
import blaze.interpreter.environment.Identifier;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Visits each node of the abstract syntax tree, and desugars the node.
 * Produces a corresponding BlazeExpression that can be evaluated.
 */
public class DesugarVisitor extends BlazeBaseVisitor<BlazeExpression> {
    public BlazeExpression visitBody(BlazeParser.BodyContext ctx) {
        return BodyDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitBlock(BlazeParser.BlockContext ctx) {
        return BodyDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitBlockOrOneLiner(BlazeParser.BlockOrOneLinerContext ctx) {
        BlazeParser.StatementContext stmtCtx = ctx.statement();
        if(stmtCtx != null) {
            ArrayList<BlazeExpression> wrapper = new ArrayList<BlazeExpression>();
            wrapper.add(visit(stmtCtx));
            return new BodyExpression(wrapper, false);
        }
        return visit(ctx.block());
    }
    
    public BlazeExpression visitFreezeStatement(BlazeParser.FreezeStatementContext ctx) {
        BlazeExpression expr = visit(ctx.expression());
        return new FreezeStatement(expr);
    }
    
    public BlazeExpression visitStatement(BlazeParser.StatementContext ctx) {
        BlazeExpression stmt = super.visitStatement(ctx);
        stmt.setIsStatement(true);
        return stmt;
    }
    
    public BlazeExpression visitExpression(BlazeParser.ExpressionContext ctx) {
        if(ctx.assignment() !=  null)
            return visit(ctx.assignment());
        if(ctx.instantiation() != null)
            return visit(ctx.instantiation());
        if(ctx.atom() != null)
            return visit(ctx.atom());
        if(ctx.lambda() != null)
            return visit(ctx.lambda());
        return ExpressionDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitAtom(BlazeParser.AtomContext ctx) {
        if(ctx.typecast() != null)
            return visit(ctx.typecast());
        if(ctx.call() != null)
            return visit(ctx.call());
        if(ctx.variable() != null)
            return visit(ctx.variable());
        if(ctx.expression() != null)
            return visit(ctx.expression());
        if(ctx.channel != null)
            return new ChannelLiteral();
        if(ctx.parallelize() != null)
            return visit(ctx.parallelize());
        return super.visitAtom(ctx);
    }
    
    public BlazeExpression visitParallelize(BlazeParser.ParallelizeContext ctx) {
        return ParallelizeDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitLiteral(BlazeParser.LiteralContext ctx) {
        TerminalNode intNode, floatNode, stringNode, boolNode, nullNode;
        BlazeParser.ListLiteralContext listLiteral = ctx.listLiteral();
        
        if(listLiteral != null)
            return visit(listLiteral);
        
        intNode = ctx.INT();
        floatNode = ctx.FLOAT();
        stringNode = ctx.STRING();
        boolNode = ctx.BOOL();
        nullNode = ctx.NULL();
        
        if(intNode != null)
            return new IntegerLiteral(intNode.getText());
        if(floatNode != null)
            return new FloatLiteral(floatNode.getText());
        if(stringNode != null) {
            String s = stringNode.getText();
            // remove quotes at start and end
            s = s.substring(1, s.length()-1);
            // fix escape sequences
            s = StringEscapeUtils.unescapeJava(s);
            return new StringLiteral(s);
        }
        if(boolNode != null)
            return new BooleanLiteral(boolNode.getText().equals(SyntaxTreeTypes.BOOLEAN_TRUE));
        if(nullNode != null)
            return new NullLiteral();
        
        return super.visitLiteral(ctx);
    }
    
    public BlazeExpression visitListLiteral(BlazeParser.ListLiteralContext ctx) {
        return ListDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitDictLiteral(BlazeParser.DictLiteralContext ctx) {
        return DictDesugarer.desugar(ctx, this);
    }

    public BlazeExpression visitParallelLiteral(BlazeParser.ParallelLiteralContext ctx) {
        return ParallelDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitReturnStatement(BlazeParser.ReturnStatementContext ctx) {
        return ReturnDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitFieldDelete(BlazeParser.FieldDeleteContext ctx) {
        BlazeExpression var = visit(ctx.variable());
        Identifier id = new Identifier(ctx.ID().getText());
        return new DeleteStatement(var, id);
    }
    
    public BlazeExpression visitIndexDelete(BlazeParser.IndexDeleteContext ctx) {
        BlazeExpression var = visit(ctx.variable());
        BlazeExpression index = visit(ctx.expression());
        return new DeleteStatement(var, index);
    }
    
    public BlazeExpression visitNativeImport(BlazeParser.NativeImportContext ctx) {
        List<String> path = new LinkedList<String>();
        for(TerminalNode node : ctx.nativePackage.ID())
            path.add(node.getText());
        return new ImportStatement(path, ctx.hasWildcard);
    }
    
    public BlazeExpression visitLibraryImport(BlazeParser.LibraryImportContext ctx) {
        return new ImportStatement(ctx.path.getText(), true);
    }
    
    public BlazeExpression visitRelativeImport(BlazeParser.RelativeImportContext ctx) {
        String path = ctx.path.getText();
        return new ImportStatement(path.substring(1, path.length()-1), false);
    }
    
    public BlazeExpression visitIfStatement(BlazeParser.IfStatementContext ctx) {
        return IfElseDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitForLoop(BlazeParser.ForLoopContext ctx) {
        return LoopDesugarer.desugar(ctx, this);
    }

    public BlazeExpression visitWhileLoop(BlazeParser.WhileLoopContext ctx) {
        return LoopDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitDereferenceIndexable(BlazeParser.DereferenceIndexableContext ctx) {
        return VariableDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitLiteralIndexable(BlazeParser.LiteralIndexableContext ctx) {
        return visit(ctx.literal());
    }
    
    public BlazeExpression visitExpressionIndexable(BlazeParser.ExpressionIndexableContext ctx) {
        return visit(ctx.expression());
    }
    
    public BlazeExpression visitTypecastIndexable(BlazeParser.TypecastIndexableContext ctx) {
        return visit(ctx.typecast());
    }
    
    public BlazeExpression visitNativeIndexable(BlazeParser.NativeIndexableContext ctx) {
        return visit(ctx.nativeReference());
    }
    
    public BlazeExpression visitFieldLookupVariable(BlazeParser.FieldLookupVariableContext ctx) {
        return VariableDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitIndexLookupVariable(BlazeParser.IndexLookupVariableContext ctx) {
        return VariableDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitIndexableVariable(BlazeParser.IndexableVariableContext ctx) {
        return visit(ctx.indexable());
    }
    
    public BlazeExpression visitNativeReference(BlazeParser.NativeReferenceContext ctx) {
        return NativeReferenceDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitPrototypeAssignment(BlazeParser.PrototypeAssignmentContext ctx) {
        return AssignmentDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitVariableCompoundAssignment(BlazeParser.VariableCompoundAssignmentContext ctx) {
        return AssignmentDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitLocalVariableAssignment(BlazeParser.LocalVariableAssignmentContext ctx) {
        return AssignmentDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitVariableAssignment(BlazeParser.VariableAssignmentContext ctx) {
        return AssignmentDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitFullLambda(BlazeParser.FullLambdaContext ctx) {
        return LambdaDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitOneLineLambda(BlazeParser.OneLineLambdaContext ctx) {
        return LambdaDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitZeroArgumentLambda(BlazeParser.ZeroArgumentLambdaContext ctx) {
        return LambdaDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitMultiCall(BlazeParser.MultiCallContext ctx) {
        return FunctionCallDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitChainCall(BlazeParser.ChainCallContext ctx) {
        return FunctionCallDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitCallableCall(BlazeParser.CallableCallContext ctx) {
        return FunctionCallDesugarer.desugar(ctx, this);
    }
        
    public BlazeExpression visitFieldCall(BlazeParser.FieldCallContext ctx) {
        return FunctionCallDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitNativeCall(BlazeParser.NativeCallContext ctx) {
        return FunctionCallDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitNativeInstantiation(BlazeParser.NativeInstantiationContext ctx) {
        return InstantiationDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitMeltedCopy(BlazeParser.MeltedCopyContext ctx) {
        BlazeExpression expr = visit(ctx.expression());
        return new MeltedCopyExpression(expr);
    }
    
    public BlazeExpression visitTypecast(BlazeParser.TypecastContext ctx) {
        return TypecastDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitCallable(BlazeParser.CallableContext ctx) {
        if(ctx.expression() != null)
            return visit(ctx.expression());
        if(ctx.parallelize() != null)
            return visit(ctx.parallelize());
        return super.visitCallable(ctx);
    }

    public BlazeExpression visitWithStatement(BlazeParser.WithStatementContext ctx) {
        return WithDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitWhenStatement(BlazeParser.WhenStatementContext ctx) {
        return WhenDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitPush(BlazeParser.PushContext ctx) {
        return PushDesugarer.desugar(ctx, this);
    }
    
    public BlazeExpression visitFunctionDeclaration(BlazeParser.FunctionDeclarationContext ctx) {
        return FunctionDeclarationDesugarer.desugar(ctx, this);
    }
}