package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.ListExpression;

import java.util.List;
import java.util.ArrayList;

/**
 * Desugars list literal nodes into literal list expressions.
 */
public class ListDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.ListLiteralContext context, DesugarVisitor visitor) {
        List<BlazeExpression> list = new ArrayList<BlazeExpression>();
        for(BlazeParser.ExpressionContext exprContext : context.expression())
            list.add(visitor.visit(exprContext));
        return new ListExpression(list);
    }
}