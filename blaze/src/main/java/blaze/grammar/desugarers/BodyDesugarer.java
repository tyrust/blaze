package blaze.grammar.desugarers;

import blaze.grammar.SyntaxError;
import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BodyExpression;

import java.util.List;
import java.util.ArrayList;

/**
 * Desugars each of the sub-expressions in a body and creates a new body expression.
 */
public class BodyDesugarer extends Desugarer {
    private static BlazeExpression desugarHelper(List<BlazeParser.StatementContext> statements, boolean canReturn, DesugarVisitor visitor) {
        List<BlazeExpression> desugared = new ArrayList<BlazeExpression>();
        for(BlazeParser.StatementContext context : statements)
            desugared.add(visitor.visit(context));
        return new BodyExpression(desugared, canReturn);
    }
    
    public static BlazeExpression desugar(BlazeParser.BodyContext context, DesugarVisitor visitor) {
        return desugarHelper(context.statement(), true, visitor);
    }
    
    public static BlazeExpression desugar(BlazeParser.BlockContext context, DesugarVisitor visitor) {
        return desugarHelper(context.statement(), false, visitor);
    }
}