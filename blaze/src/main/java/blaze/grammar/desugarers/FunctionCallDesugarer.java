package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.FunctionCallExpression;
import blaze.interpreter.environment.Identifier;

import java.util.List;
import java.util.LinkedList;

/**
 * Desugars function call trees into function call expressions.
 */
public class FunctionCallDesugarer extends Desugarer {
    public static List<BlazeExpression> createParamsList(List<BlazeParser.ExpressionContext> exprs, DesugarVisitor visitor) {
        List<BlazeExpression> params = new LinkedList<BlazeExpression>();
        for(BlazeParser.ExpressionContext exprCtx : exprs)
            params.add(visitor.visit(exprCtx));
        return params;
    }
    
    public static BlazeExpression desugar(BlazeParser.ChainCallContext context, DesugarVisitor visitor) {
        BlazeExpression prevCall = visitor.visit(context.call());
        Identifier field = new Identifier(context.ID().getText());
        List<BlazeExpression> params = createParamsList(context.callParameters().expression(), visitor);
        return new FunctionCallExpression(prevCall, field, params);
    }

    
    public static BlazeExpression desugar(BlazeParser.MultiCallContext context, DesugarVisitor visitor) {
        BlazeExpression prevCall = visitor.visit(context.call());
        List<BlazeExpression> params = createParamsList(context.callParameters().expression(), visitor);
        return new FunctionCallExpression(prevCall, params);
    }
    
    // Native calls are separate from field calls so that they don't pass their LHS as first argument
    // (which is how other field calls work)
    public static BlazeExpression desugar(BlazeParser.NativeCallContext context, DesugarVisitor visitor) {
        BlazeExpression function = visitor.visit(context.nativeReference());
        List<BlazeExpression> params = createParamsList(context.callParameters().expression(), visitor);
        return new FunctionCallExpression(function, params, true);
    }
    
    public static BlazeExpression desugar(BlazeParser.FieldCallContext context, DesugarVisitor visitor) {
        BlazeExpression lhs = visitor.visit(context.callable());
        Identifier field = new Identifier(context.ID().getText());
        List<BlazeExpression> params = createParamsList(context.callParameters().expression(), visitor);
        return new FunctionCallExpression(lhs, field, params);
    }
    
    public static BlazeExpression desugar(BlazeParser.CallableCallContext context, DesugarVisitor visitor) {
        BlazeExpression function = visitor.visit(context.callable());
        List<BlazeExpression> params = createParamsList(context.callParameters().expression(), visitor);
        return new FunctionCallExpression(function, params);
    }
}