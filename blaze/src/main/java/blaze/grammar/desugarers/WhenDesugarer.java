package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.WhenStatement;
import blaze.interpreter.environment.Identifier;

/**
 * Desugars when trees into when statements.
 */
public class WhenDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.WhenStatementContext context, DesugarVisitor visitor) {
        Identifier id = new Identifier(context.ID().getText());
        BlazeExpression channel = visitor.visit(context.expression());
        BlazeExpression body = visitor.visit(context.blockOrOneLiner());
        return new WhenStatement(id, channel, body);
    }
}