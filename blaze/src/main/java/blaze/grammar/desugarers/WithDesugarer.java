package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.WithStatement;

/**
 * Desugars with tree into with statements.
 */
public class WithDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.WithStatementContext context, DesugarVisitor visitor) {
        BlazeExpression toLock = visitor.visit(context.expression());
        BlazeExpression body = visitor.visit(context.blockOrOneLiner());
        return new WithStatement(toLock, body);
    }
}