package blaze.grammar.desugarers;

import blaze.grammar.BlazeParser;
import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.PushStatement;
import blaze.interpreter.environment.Identifier;

import java.util.List;
import java.util.ArrayList;

/**
 * Desugars a push of expressions into a series of push operations.
 */
public class PushDesugarer extends Desugarer {
    public static BlazeExpression desugar(BlazeParser.PushContext context, DesugarVisitor visitor) {
        BlazeExpression channel = visitor.visit(context.channel);
        List<BlazeExpression> values = new ArrayList<BlazeExpression>();
        for(BlazeParser.ExpressionContext valueContext : context.values)
            values.add(visitor.visit(valueContext));
        return new PushStatement(channel, values);
    }
}