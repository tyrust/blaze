// Generated from grammar/Blaze.g4 by ANTLR 4.0
 package blaze.grammar; 
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BlazeParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__61=1, T__60=2, T__59=3, T__58=4, T__57=5, T__56=6, T__55=7, T__54=8, 
		T__53=9, T__52=10, T__51=11, T__50=12, T__49=13, T__48=14, T__47=15, T__46=16, 
		T__45=17, T__44=18, T__43=19, T__42=20, T__41=21, T__40=22, T__39=23, 
		T__38=24, T__37=25, T__36=26, T__35=27, T__34=28, T__33=29, T__32=30, 
		T__31=31, T__30=32, T__29=33, T__28=34, T__27=35, T__26=36, T__25=37, 
		T__24=38, T__23=39, T__22=40, T__21=41, T__20=42, T__19=43, T__18=44, 
		T__17=45, T__16=46, T__15=47, T__14=48, T__13=49, T__12=50, T__11=51, 
		T__10=52, T__9=53, T__8=54, T__7=55, T__6=56, T__5=57, T__4=58, T__3=59, 
		T__2=60, T__1=61, T__0=62, INT=63, FLOAT=64, BOOL=65, STRING=66, NULL=67, 
		ID=68, WHITESPACE=69, COMMENT=70, LINE_COMMENT=71, ESCAPED_NEWLINE=72;
	public static final String[] tokenNames = {
		"<INVALID>", "'local'", "'or'", "'*'", "'['", "'<'", "'!='", "'closure'", 
		"'<='", "'}'", "'\"'", "'float'", "'->'", "'%'", "'*='", "')'", "'bool'", 
		"'__blaze_literal'", "'='", "'prototype'", "'new'", "'when'", "'|'", "'str'", 
		"']'", "'-='", "','", "'\n'", "'while'", "'-'", "'('", "':'", "'not'", 
		"'\r'", "'if'", "'int'", "'''", "'...'", "'{'", "'and'", "'+='", "'native'", 
		"'freeze'", "'else'", "'<-'", "'import'", "'melted'", "'delete'", "'.'", 
		"'nulltype'", "'+'", "'list'", "'for'", "'return'", "';'", "'with'", "'>'", 
		"'%='", "'/='", "'dict'", "'=='", "'/'", "'>='", "INT", "FLOAT", "BOOL", 
		"STRING", "'null'", "ID", "WHITESPACE", "COMMENT", "LINE_COMMENT", "ESCAPED_NEWLINE"
	};
	public static final int
		RULE_program = 0, RULE_body = 1, RULE_block = 2, RULE_blockOrOneLiner = 3, 
		RULE_separator = 4, RULE_statement = 5, RULE_importStatement = 6, RULE_ifStatement = 7, 
		RULE_loopStatement = 8, RULE_withStatement = 9, RULE_whenStatement = 10, 
		RULE_returnStatement = 11, RULE_freezeStatement = 12, RULE_deleteStatement = 13, 
		RULE_packageDeclaration = 14, RULE_expression = 15, RULE_literal = 16, 
		RULE_listLiteral = 17, RULE_dictLiteral = 18, RULE_parallelLiteral = 19, 
		RULE_functionDeclaration = 20, RULE_lambda = 21, RULE_meltedCopy = 22, 
		RULE_typecast = 23, RULE_argumentList = 24, RULE_push = 25, RULE_assignment = 26, 
		RULE_indexable = 27, RULE_nativeReference = 28, RULE_variable = 29, RULE_callable = 30, 
		RULE_callParameters = 31, RULE_parallelize = 32, RULE_call = 33, RULE_instantiation = 34, 
		RULE_atom = 35, RULE_blazeType = 36;
	public static final String[] ruleNames = {
		"program", "body", "block", "blockOrOneLiner", "separator", "statement", 
		"importStatement", "ifStatement", "loopStatement", "withStatement", "whenStatement", 
		"returnStatement", "freezeStatement", "deleteStatement", "packageDeclaration", 
		"expression", "literal", "listLiteral", "dictLiteral", "parallelLiteral", 
		"functionDeclaration", "lambda", "meltedCopy", "typecast", "argumentList", 
		"push", "assignment", "indexable", "nativeReference", "variable", "callable", 
		"callParameters", "parallelize", "call", "instantiation", "atom", "blazeType"
	};

	@Override
	public String getGrammarFileName() { return "Blaze.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public BlazeParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74); body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public SeparatorContext separator(int i) {
			return getRuleContext(SeparatorContext.class,i);
		}
		public List<SeparatorContext> separator() {
			return getRuleContexts(SeparatorContext.class);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_body);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(91);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 4) | (1L << 7) | (1L << 11) | (1L << 12) | (1L << 16) | (1L << 17) | (1L << 20) | (1L << 21) | (1L << 22) | (1L << 23) | (1L << 26) | (1L << 27) | (1L << 28) | (1L << 29) | (1L << 30) | (1L << 32) | (1L << 33) | (1L << 34) | (1L << 35) | (1L << 38) | (1L << 41) | (1L << 42) | (1L << 45) | (1L << 46) | (1L << 47) | (1L << 49) | (1L << 50) | (1L << 51) | (1L << 52) | (1L << 53) | (1L << 54) | (1L << 55) | (1L << 59) | (1L << 61) | (1L << INT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (FLOAT - 64)) | (1L << (BOOL - 64)) | (1L << (STRING - 64)) | (1L << (NULL - 64)) | (1L << (ID - 64)))) != 0)) {
				{
				setState(77);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 27) | (1L << 33) | (1L << 54))) != 0)) {
					{
					setState(76); separator();
					}
				}

				setState(79); statement();
				setState(85);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
				while ( _alt!=2 && _alt!=-1 ) {
					if ( _alt==1 ) {
						{
						{
						setState(80); separator();
						setState(81); statement();
						}
						} 
					}
					setState(87);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
				}
				setState(89);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 27) | (1L << 33) | (1L << 54))) != 0)) {
					{
					setState(88); separator();
					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public SeparatorContext separator(int i) {
			return getRuleContext(SeparatorContext.class,i);
		}
		public List<SeparatorContext> separator() {
			return getRuleContexts(SeparatorContext.class);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_block);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(108);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 4) | (1L << 7) | (1L << 11) | (1L << 12) | (1L << 16) | (1L << 17) | (1L << 20) | (1L << 21) | (1L << 22) | (1L << 23) | (1L << 26) | (1L << 27) | (1L << 28) | (1L << 29) | (1L << 30) | (1L << 32) | (1L << 33) | (1L << 34) | (1L << 35) | (1L << 38) | (1L << 41) | (1L << 42) | (1L << 45) | (1L << 46) | (1L << 47) | (1L << 49) | (1L << 50) | (1L << 51) | (1L << 52) | (1L << 53) | (1L << 54) | (1L << 55) | (1L << 59) | (1L << 61) | (1L << INT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (FLOAT - 64)) | (1L << (BOOL - 64)) | (1L << (STRING - 64)) | (1L << (NULL - 64)) | (1L << (ID - 64)))) != 0)) {
				{
				setState(94);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 27) | (1L << 33) | (1L << 54))) != 0)) {
					{
					setState(93); separator();
					}
				}

				setState(96); statement();
				setState(102);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
				while ( _alt!=2 && _alt!=-1 ) {
					if ( _alt==1 ) {
						{
						{
						setState(97); separator();
						setState(98); statement();
						}
						} 
					}
					setState(104);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
				}
				setState(106);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 27) | (1L << 33) | (1L << 54))) != 0)) {
					{
					setState(105); separator();
					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockOrOneLinerContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public SeparatorContext separator(int i) {
			return getRuleContext(SeparatorContext.class,i);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<SeparatorContext> separator() {
			return getRuleContexts(SeparatorContext.class);
		}
		public BlockOrOneLinerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockOrOneLiner; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitBlockOrOneLiner(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockOrOneLinerContext blockOrOneLiner() throws RecognitionException {
		BlockOrOneLinerContext _localctx = new BlockOrOneLinerContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_blockOrOneLiner);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 27) | (1L << 33) | (1L << 54))) != 0)) {
				{
				setState(110); separator();
				}
			}

			setState(118);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(113); match(38);
				setState(114); block();
				setState(115); match(9);
				}
				break;

			case 2:
				{
				setState(117); statement();
				}
				break;
			}
			setState(121);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				{
				setState(120); separator();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SeparatorContext extends ParserRuleContext {
		public SeparatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_separator; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitSeparator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SeparatorContext separator() throws RecognitionException {
		SeparatorContext _localctx = new SeparatorContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_separator);
		int _la;
		try {
			int _alt;
			setState(136);
			switch (_input.LA(1)) {
			case 27:
			case 33:
				enterOuterAlt(_localctx, 1);
				{
				setState(127); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(124);
						_la = _input.LA(1);
						if (_la==33) {
							{
							setState(123); match(33);
							}
						}

						setState(126); match(27);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(129); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				} while ( _alt!=2 && _alt!=-1 );
				}
				break;
			case 54:
				enterOuterAlt(_localctx, 2);
				{
				setState(132); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(131); match(54);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(134); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
				} while ( _alt!=2 && _alt!=-1 );
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public WhenStatementContext whenStatement() {
			return getRuleContext(WhenStatementContext.class,0);
		}
		public LoopStatementContext loopStatement() {
			return getRuleContext(LoopStatementContext.class,0);
		}
		public FreezeStatementContext freezeStatement() {
			return getRuleContext(FreezeStatementContext.class,0);
		}
		public ReturnStatementContext returnStatement() {
			return getRuleContext(ReturnStatementContext.class,0);
		}
		public ImportStatementContext importStatement() {
			return getRuleContext(ImportStatementContext.class,0);
		}
		public FunctionDeclarationContext functionDeclaration() {
			return getRuleContext(FunctionDeclarationContext.class,0);
		}
		public WithStatementContext withStatement() {
			return getRuleContext(WithStatementContext.class,0);
		}
		public PushContext push() {
			return getRuleContext(PushContext.class,0);
		}
		public DeleteStatementContext deleteStatement() {
			return getRuleContext(DeleteStatementContext.class,0);
		}
		public IfStatementContext ifStatement() {
			return getRuleContext(IfStatementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_statement);
		try {
			setState(149);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(138); importStatement();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(139); functionDeclaration();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(140); ifStatement();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(141); loopStatement();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(142); withStatement();
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(143); returnStatement();
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(144); freezeStatement();
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(145); deleteStatement();
				}
				break;

			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(146); whenStatement();
				}
				break;

			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(147); push();
				}
				break;

			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(148); expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportStatementContext extends ParserRuleContext {
		public boolean hasWildcard = false;
		public ImportStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importStatement; }
	 
		public ImportStatementContext() { }
		public void copyFrom(ImportStatementContext ctx) {
			super.copyFrom(ctx);
			this.hasWildcard = ctx.hasWildcard;
		}
	}
	public static class RelativeImportContext extends ImportStatementContext {
		public Token path;
		public TerminalNode STRING() { return getToken(BlazeParser.STRING, 0); }
		public RelativeImportContext(ImportStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitRelativeImport(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NativeImportContext extends ImportStatementContext {
		public PackageDeclarationContext nativePackage;
		public PackageDeclarationContext packageDeclaration() {
			return getRuleContext(PackageDeclarationContext.class,0);
		}
		public NativeImportContext(ImportStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitNativeImport(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LibraryImportContext extends ImportStatementContext {
		public Token path;
		public LibraryImportContext(ImportStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitLibraryImport(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImportStatementContext importStatement() throws RecognitionException {
		ImportStatementContext _localctx = new ImportStatementContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_importStatement);
		int _la;
		try {
			int _alt;
			setState(167);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				_localctx = new NativeImportContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(151); match(45);
				setState(152); match(41);
				setState(153); ((NativeImportContext)_localctx).nativePackage = packageDeclaration();
				setState(157);
				switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
				case 1:
					{
					setState(154); match(48);
					setState(155); match(3);
					 ((NativeImportContext)_localctx).hasWildcard =  true; 
					}
					break;
				}
				}
				break;

			case 2:
				_localctx = new RelativeImportContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(159); match(45);
				setState(160); ((RelativeImportContext)_localctx).path = match(STRING);
				}
				break;

			case 3:
				_localctx = new LibraryImportContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(161); match(45);
				setState(163); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(162);
						((LibraryImportContext)_localctx).path = _input.LT(1);
						_la = _input.LA(1);
						if ( _la <= 0 || ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 10) | (1L << 27) | (1L << 33) | (1L << 36))) != 0)) ) {
							((LibraryImportContext)_localctx).path = (Token)_errHandler.recoverInline(this);
						}
						consume();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(165); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
				} while ( _alt!=2 && _alt!=-1 );
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStatementContext extends ParserRuleContext {
		public BlockOrOneLinerContext consequent;
		public BlockOrOneLinerContext alternative;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlockOrOneLinerContext blockOrOneLiner(int i) {
			return getRuleContext(BlockOrOneLinerContext.class,i);
		}
		public List<BlockOrOneLinerContext> blockOrOneLiner() {
			return getRuleContexts(BlockOrOneLinerContext.class);
		}
		public IfStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitIfStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfStatementContext ifStatement() throws RecognitionException {
		IfStatementContext _localctx = new IfStatementContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_ifStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(169); match(34);
			setState(170); expression(0);
			setState(171); ((IfStatementContext)_localctx).consequent = blockOrOneLiner();
			setState(174);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(172); match(43);
				setState(173); ((IfStatementContext)_localctx).alternative = blockOrOneLiner();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LoopStatementContext extends ParserRuleContext {
		public LoopStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loopStatement; }
	 
		public LoopStatementContext() { }
		public void copyFrom(LoopStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class WhileLoopContext extends LoopStatementContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlockOrOneLinerContext blockOrOneLiner() {
			return getRuleContext(BlockOrOneLinerContext.class,0);
		}
		public WhileLoopContext(LoopStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitWhileLoop(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ForLoopContext extends LoopStatementContext {
		public Token var;
		public Token in;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode ID(int i) {
			return getToken(BlazeParser.ID, i);
		}
		public List<TerminalNode> ID() { return getTokens(BlazeParser.ID); }
		public BlockOrOneLinerContext blockOrOneLiner() {
			return getRuleContext(BlockOrOneLinerContext.class,0);
		}
		public ForLoopContext(LoopStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitForLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LoopStatementContext loopStatement() throws RecognitionException {
		LoopStatementContext _localctx = new LoopStatementContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_loopStatement);
		try {
			setState(187);
			switch (_input.LA(1)) {
			case 52:
				_localctx = new ForLoopContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(176); match(52);
				setState(177); ((ForLoopContext)_localctx).var = match(ID);
				setState(178); ((ForLoopContext)_localctx).in = match(ID);
				setState(179); expression(0);
				setState(180); blockOrOneLiner();
				setState(181);
				if (!( ((ForLoopContext)_localctx).in.getText().equals("in") )) throw new FailedPredicateException(this, " $in.getText().equals(\"in\") ");
				}
				break;
			case 28:
				_localctx = new WhileLoopContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(183); match(28);
				setState(184); expression(0);
				setState(185); blockOrOneLiner();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WithStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlockOrOneLinerContext blockOrOneLiner() {
			return getRuleContext(BlockOrOneLinerContext.class,0);
		}
		public WithStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_withStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitWithStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WithStatementContext withStatement() throws RecognitionException {
		WithStatementContext _localctx = new WithStatementContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_withStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(189); match(55);
			setState(190); expression(0);
			setState(191); blockOrOneLiner();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhenStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode ID() { return getToken(BlazeParser.ID, 0); }
		public BlockOrOneLinerContext blockOrOneLiner() {
			return getRuleContext(BlockOrOneLinerContext.class,0);
		}
		public WhenStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whenStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitWhenStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhenStatementContext whenStatement() throws RecognitionException {
		WhenStatementContext _localctx = new WhenStatementContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_whenStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(193); match(21);
			setState(194); match(ID);
			setState(195); match(44);
			setState(196); expression(0);
			setState(197); blockOrOneLiner();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ReturnStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitReturnStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnStatementContext returnStatement() throws RecognitionException {
		ReturnStatementContext _localctx = new ReturnStatementContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_returnStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(199); match(53);
			setState(201);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				setState(200); expression(0);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FreezeStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public FreezeStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_freezeStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitFreezeStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FreezeStatementContext freezeStatement() throws RecognitionException {
		FreezeStatementContext _localctx = new FreezeStatementContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_freezeStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(203); match(42);
			setState(204); expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeleteStatementContext extends ParserRuleContext {
		public DeleteStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deleteStatement; }
	 
		public DeleteStatementContext() { }
		public void copyFrom(DeleteStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IndexDeleteContext extends DeleteStatementContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public IndexDeleteContext(DeleteStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitIndexDelete(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FieldDeleteContext extends DeleteStatementContext {
		public TerminalNode ID() { return getToken(BlazeParser.ID, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public FieldDeleteContext(DeleteStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitFieldDelete(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeleteStatementContext deleteStatement() throws RecognitionException {
		DeleteStatementContext _localctx = new DeleteStatementContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_deleteStatement);
		try {
			setState(217);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				_localctx = new FieldDeleteContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(206); match(47);
				setState(207); variable(0);
				setState(208); match(48);
				setState(209); match(ID);
				}
				break;

			case 2:
				_localctx = new IndexDeleteContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(211); match(47);
				setState(212); variable(0);
				setState(213); match(4);
				setState(214); expression(0);
				setState(215); match(24);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PackageDeclarationContext extends ParserRuleContext {
		public TerminalNode ID(int i) {
			return getToken(BlazeParser.ID, i);
		}
		public List<TerminalNode> ID() { return getTokens(BlazeParser.ID); }
		public PackageDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_packageDeclaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitPackageDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PackageDeclarationContext packageDeclaration() throws RecognitionException {
		PackageDeclarationContext _localctx = new PackageDeclarationContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_packageDeclaration);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(219); match(ID);
			setState(224);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(220); match(48);
					setState(221); match(ID);
					}
					} 
				}
				setState(226);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public int _p;
		public ExpressionContext lhs;
		public Token operator;
		public ExpressionContext rhs;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public LambdaContext lambda() {
			return getRuleContext(LambdaContext.class,0);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public InstantiationContext instantiation() {
			return getRuleContext(InstantiationContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ExpressionContext(ParserRuleContext parent, int invokingState, int _p) {
			super(parent, invokingState);
			this._p = _p;
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState, _p);
		ExpressionContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, RULE_expression);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(234);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				setState(228);
				((ExpressionContext)_localctx).operator = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 29) | (1L << 32) | (1L << 50))) != 0)) ) {
					((ExpressionContext)_localctx).operator = (Token)_errHandler.recoverInline(this);
				}
				consume();
				setState(229); ((ExpressionContext)_localctx).rhs = expression(11);
				}
				break;

			case 2:
				{
				setState(230); assignment();
				}
				break;

			case 3:
				{
				setState(231); instantiation();
				}
				break;

			case 4:
				{
				setState(232); atom();
				}
				break;

			case 5:
				{
				setState(233); lambda();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(256);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(254);
					switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
					case 1:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState, _p);
						_localctx.lhs = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(236);
						if (!(10 >= _localctx._p)) throw new FailedPredicateException(this, "10 >= $_p");
						setState(237);
						((ExpressionContext)_localctx).operator = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 13) | (1L << 61))) != 0)) ) {
							((ExpressionContext)_localctx).operator = (Token)_errHandler.recoverInline(this);
						}
						consume();
						setState(238); ((ExpressionContext)_localctx).rhs = expression(11);
						}
						break;

					case 2:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState, _p);
						_localctx.lhs = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(239);
						if (!(9 >= _localctx._p)) throw new FailedPredicateException(this, "9 >= $_p");
						setState(240);
						((ExpressionContext)_localctx).operator = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==29 || _la==50) ) {
							((ExpressionContext)_localctx).operator = (Token)_errHandler.recoverInline(this);
						}
						consume();
						setState(241); ((ExpressionContext)_localctx).rhs = expression(10);
						}
						break;

					case 3:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState, _p);
						_localctx.lhs = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(242);
						if (!(8 >= _localctx._p)) throw new FailedPredicateException(this, "8 >= $_p");
						setState(243);
						((ExpressionContext)_localctx).operator = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 5) | (1L << 8) | (1L << 56) | (1L << 62))) != 0)) ) {
							((ExpressionContext)_localctx).operator = (Token)_errHandler.recoverInline(this);
						}
						consume();
						setState(244); ((ExpressionContext)_localctx).rhs = expression(9);
						}
						break;

					case 4:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState, _p);
						_localctx.lhs = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(245);
						if (!(7 >= _localctx._p)) throw new FailedPredicateException(this, "7 >= $_p");
						setState(246);
						((ExpressionContext)_localctx).operator = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==6 || _la==60) ) {
							((ExpressionContext)_localctx).operator = (Token)_errHandler.recoverInline(this);
						}
						consume();
						setState(247); ((ExpressionContext)_localctx).rhs = expression(8);
						}
						break;

					case 5:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState, _p);
						_localctx.lhs = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(248);
						if (!(6 >= _localctx._p)) throw new FailedPredicateException(this, "6 >= $_p");
						setState(249); ((ExpressionContext)_localctx).operator = match(39);
						setState(250); ((ExpressionContext)_localctx).rhs = expression(7);
						}
						break;

					case 6:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState, _p);
						_localctx.lhs = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(251);
						if (!(5 >= _localctx._p)) throw new FailedPredicateException(this, "5 >= $_p");
						setState(252); ((ExpressionContext)_localctx).operator = match(2);
						setState(253); ((ExpressionContext)_localctx).rhs = expression(6);
						}
						break;
					}
					} 
				}
				setState(258);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public ParallelLiteralContext parallelLiteral() {
			return getRuleContext(ParallelLiteralContext.class,0);
		}
		public ListLiteralContext listLiteral() {
			return getRuleContext(ListLiteralContext.class,0);
		}
		public TerminalNode FLOAT() { return getToken(BlazeParser.FLOAT, 0); }
		public TerminalNode INT() { return getToken(BlazeParser.INT, 0); }
		public DictLiteralContext dictLiteral() {
			return getRuleContext(DictLiteralContext.class,0);
		}
		public TerminalNode NULL() { return getToken(BlazeParser.NULL, 0); }
		public TerminalNode STRING() { return getToken(BlazeParser.STRING, 0); }
		public TerminalNode BOOL() { return getToken(BlazeParser.BOOL, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_literal);
		try {
			setState(267);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(259); match(INT);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(260); match(FLOAT);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(261); match(STRING);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(262); match(BOOL);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(263); listLiteral();
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(264); dictLiteral();
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(265); parallelLiteral();
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(266); match(NULL);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListLiteralContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ListLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listLiteral; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitListLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListLiteralContext listLiteral() throws RecognitionException {
		ListLiteralContext _localctx = new ListLiteralContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_listLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(270);
			_la = _input.LA(1);
			if (_la==51) {
				{
				setState(269); match(51);
				}
			}

			setState(272); match(4);
			setState(281);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 4) | (1L << 7) | (1L << 11) | (1L << 12) | (1L << 16) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 23) | (1L << 26) | (1L << 29) | (1L << 30) | (1L << 32) | (1L << 35) | (1L << 38) | (1L << 41) | (1L << 46) | (1L << 49) | (1L << 50) | (1L << 51) | (1L << 59) | (1L << 61) | (1L << INT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (FLOAT - 64)) | (1L << (BOOL - 64)) | (1L << (STRING - 64)) | (1L << (NULL - 64)) | (1L << (ID - 64)))) != 0)) {
				{
				setState(273); expression(0);
				setState(278);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==26) {
					{
					{
					setState(274); match(26);
					setState(275); expression(0);
					}
					}
					setState(280);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(283); match(24);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DictLiteralContext extends ParserRuleContext {
		public ExpressionContext expression;
		public List<ExpressionContext> keys = new ArrayList<ExpressionContext>();
		public List<ExpressionContext> values = new ArrayList<ExpressionContext>();
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public DictLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dictLiteral; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitDictLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DictLiteralContext dictLiteral() throws RecognitionException {
		DictLiteralContext _localctx = new DictLiteralContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_dictLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(286);
			_la = _input.LA(1);
			if (_la==59) {
				{
				setState(285); match(59);
				}
			}

			setState(288); match(4);
			setState(302);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 4) | (1L << 7) | (1L << 11) | (1L << 12) | (1L << 16) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 23) | (1L << 26) | (1L << 29) | (1L << 30) | (1L << 32) | (1L << 35) | (1L << 38) | (1L << 41) | (1L << 46) | (1L << 49) | (1L << 50) | (1L << 51) | (1L << 59) | (1L << 61) | (1L << INT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (FLOAT - 64)) | (1L << (BOOL - 64)) | (1L << (STRING - 64)) | (1L << (NULL - 64)) | (1L << (ID - 64)))) != 0)) {
				{
				setState(289); ((DictLiteralContext)_localctx).expression = expression(0);
				((DictLiteralContext)_localctx).keys.add(((DictLiteralContext)_localctx).expression);
				setState(290); match(31);
				setState(291); ((DictLiteralContext)_localctx).expression = expression(0);
				((DictLiteralContext)_localctx).values.add(((DictLiteralContext)_localctx).expression);
				setState(299);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==26) {
					{
					{
					setState(292); match(26);
					setState(293); ((DictLiteralContext)_localctx).expression = expression(0);
					((DictLiteralContext)_localctx).keys.add(((DictLiteralContext)_localctx).expression);
					setState(294); match(31);
					setState(295); ((DictLiteralContext)_localctx).expression = expression(0);
					((DictLiteralContext)_localctx).values.add(((DictLiteralContext)_localctx).expression);
					}
					}
					setState(301);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(304); match(24);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParallelLiteralContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ParallelLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parallelLiteral; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitParallelLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParallelLiteralContext parallelLiteral() throws RecognitionException {
		ParallelLiteralContext _localctx = new ParallelLiteralContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_parallelLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(306); match(22);
			setState(315);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				{
				setState(307); expression(0);
				setState(312);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==26) {
					{
					{
					setState(308); match(26);
					setState(309); expression(0);
					}
					}
					setState(314);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			}
			setState(317); match(22);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDeclarationContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public SeparatorContext separator() {
			return getRuleContext(SeparatorContext.class,0);
		}
		public FunctionDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDeclaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitFunctionDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionDeclarationContext functionDeclaration() throws RecognitionException {
		FunctionDeclarationContext _localctx = new FunctionDeclarationContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_functionDeclaration);
		int _la;
		try {
			setState(341);
			switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(319); variable(0);
				setState(320); match(30);
				setState(321); argumentList();
				setState(322); match(15);
				setState(323); match(12);
				setState(325);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 27) | (1L << 33) | (1L << 54))) != 0)) {
					{
					setState(324); separator();
					}
				}

				setState(327); match(38);
				setState(328); body();
				setState(329); match(9);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(331); variable(0);
				setState(332); match(30);
				setState(333); argumentList();
				setState(334); match(15);
				setState(335); match(12);
				setState(337);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 27) | (1L << 33) | (1L << 54))) != 0)) {
					{
					setState(336); separator();
					}
				}

				setState(339); expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LambdaContext extends ParserRuleContext {
		public LambdaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lambda; }
	 
		public LambdaContext() { }
		public void copyFrom(LambdaContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FullLambdaContext extends LambdaContext {
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class,0);
		}
		public SeparatorContext separator() {
			return getRuleContext(SeparatorContext.class,0);
		}
		public FullLambdaContext(LambdaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitFullLambda(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OneLineLambdaContext extends LambdaContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ArgumentListContext argumentList() {
			return getRuleContext(ArgumentListContext.class,0);
		}
		public SeparatorContext separator() {
			return getRuleContext(SeparatorContext.class,0);
		}
		public OneLineLambdaContext(LambdaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitOneLineLambda(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ZeroArgumentLambdaContext extends LambdaContext {
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public ZeroArgumentLambdaContext(LambdaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitZeroArgumentLambda(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LambdaContext lambda() throws RecognitionException {
		LambdaContext _localctx = new LambdaContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_lambda);
		int _la;
		try {
			setState(363);
			switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
			case 1:
				_localctx = new FullLambdaContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(343); argumentList();
				setState(344); match(12);
				setState(346);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 27) | (1L << 33) | (1L << 54))) != 0)) {
					{
					setState(345); separator();
					}
				}

				setState(348); match(38);
				setState(349); body();
				setState(350); match(9);
				}
				break;

			case 2:
				_localctx = new OneLineLambdaContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(352); argumentList();
				setState(353); match(12);
				setState(355);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 27) | (1L << 33) | (1L << 54))) != 0)) {
					{
					setState(354); separator();
					}
				}

				setState(357); expression(0);
				}
				break;

			case 3:
				_localctx = new ZeroArgumentLambdaContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(359); match(38);
				setState(360); body();
				setState(361); match(9);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MeltedCopyContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public MeltedCopyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_meltedCopy; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitMeltedCopy(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MeltedCopyContext meltedCopy() throws RecognitionException {
		MeltedCopyContext _localctx = new MeltedCopyContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_meltedCopy);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(365); match(46);
			setState(366); match(30);
			setState(367); expression(0);
			setState(368); match(15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypecastContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlazeTypeContext blazeType() {
			return getRuleContext(BlazeTypeContext.class,0);
		}
		public TypecastContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typecast; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitTypecast(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypecastContext typecast() throws RecognitionException {
		TypecastContext _localctx = new TypecastContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_typecast);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(370); blazeType();
			setState(371); match(30);
			setState(372); expression(0);
			setState(373); match(15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentListContext extends ParserRuleContext {
		public Token ID;
		public List<Token> args = new ArrayList<Token>();
		public Token vararg;
		public TerminalNode ID(int i) {
			return getToken(BlazeParser.ID, i);
		}
		public List<TerminalNode> ID() { return getTokens(BlazeParser.ID); }
		public ArgumentListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argumentList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitArgumentList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentListContext argumentList() throws RecognitionException {
		ArgumentListContext _localctx = new ArgumentListContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_argumentList);
		int _la;
		try {
			int _alt;
			setState(413);
			switch ( getInterpreter().adaptivePredict(_input,48,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(383);
				_la = _input.LA(1);
				if (_la==ID) {
					{
					setState(375); ((ArgumentListContext)_localctx).ID = match(ID);
					((ArgumentListContext)_localctx).args.add(((ArgumentListContext)_localctx).ID);
					setState(380);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,42,_ctx);
					while ( _alt!=2 && _alt!=-1 ) {
						if ( _alt==1 ) {
							{
							{
							setState(376); match(26);
							setState(377); ((ArgumentListContext)_localctx).ID = match(ID);
							((ArgumentListContext)_localctx).args.add(((ArgumentListContext)_localctx).ID);
							}
							} 
						}
						setState(382);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,42,_ctx);
					}
					}
				}

				setState(388);
				_la = _input.LA(1);
				if (_la==26) {
					{
					setState(385); match(26);
					setState(386); ((ArgumentListContext)_localctx).vararg = match(ID);
					setState(387); match(37);
					}
				}

				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(390); match(30);
				setState(399);
				_la = _input.LA(1);
				if (_la==ID) {
					{
					setState(391); ((ArgumentListContext)_localctx).ID = match(ID);
					((ArgumentListContext)_localctx).args.add(((ArgumentListContext)_localctx).ID);
					setState(396);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
					while ( _alt!=2 && _alt!=-1 ) {
						if ( _alt==1 ) {
							{
							{
							setState(392); match(26);
							setState(393); ((ArgumentListContext)_localctx).ID = match(ID);
							((ArgumentListContext)_localctx).args.add(((ArgumentListContext)_localctx).ID);
							}
							} 
						}
						setState(398);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
					}
					}
				}

				setState(404);
				_la = _input.LA(1);
				if (_la==26) {
					{
					setState(401); match(26);
					setState(402); ((ArgumentListContext)_localctx).vararg = match(ID);
					setState(403); match(37);
					}
				}

				setState(406); match(15);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(407); ((ArgumentListContext)_localctx).vararg = match(ID);
				setState(408); match(37);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(409); match(30);
				setState(410); ((ArgumentListContext)_localctx).vararg = match(ID);
				setState(411); match(37);
				setState(412); match(15);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PushContext extends ParserRuleContext {
		public ExpressionContext channel;
		public ExpressionContext expression;
		public List<ExpressionContext> values = new ArrayList<ExpressionContext>();
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public PushContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_push; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitPush(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PushContext push() throws RecognitionException {
		PushContext _localctx = new PushContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_push);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(415); ((PushContext)_localctx).channel = expression(0);
			setState(416); match(44);
			setState(417); ((PushContext)_localctx).expression = expression(0);
			((PushContext)_localctx).values.add(((PushContext)_localctx).expression);
			setState(422);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(418); match(26);
					setState(419); ((PushContext)_localctx).expression = expression(0);
					((PushContext)_localctx).values.add(((PushContext)_localctx).expression);
					}
					} 
				}
				setState(424);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
	 
		public AssignmentContext() { }
		public void copyFrom(AssignmentContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PrototypeAssignmentContext extends AssignmentContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlazeTypeContext blazeType() {
			return getRuleContext(BlazeTypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(BlazeParser.ID, 0); }
		public PrototypeAssignmentContext(AssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitPrototypeAssignment(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LocalVariableAssignmentContext extends AssignmentContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode ID() { return getToken(BlazeParser.ID, 0); }
		public LocalVariableAssignmentContext(AssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitLocalVariableAssignment(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableCompoundAssignmentContext extends AssignmentContext {
		public Token operator;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public VariableCompoundAssignmentContext(AssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitVariableCompoundAssignment(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableAssignmentContext extends AssignmentContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public VariableAssignmentContext(AssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitVariableAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_assignment);
		int _la;
		try {
			setState(445);
			switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
			case 1:
				_localctx = new PrototypeAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(425); blazeType();
				setState(426); match(48);
				setState(427); match(19);
				setState(428); match(48);
				setState(429); match(ID);
				setState(430); match(18);
				setState(431); expression(0);
				}
				break;

			case 2:
				_localctx = new VariableAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(433); variable(0);
				setState(434); match(18);
				setState(435); expression(0);
				}
				break;

			case 3:
				_localctx = new VariableCompoundAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(437); variable(0);
				setState(438);
				((VariableCompoundAssignmentContext)_localctx).operator = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 14) | (1L << 25) | (1L << 40) | (1L << 57) | (1L << 58))) != 0)) ) {
					((VariableCompoundAssignmentContext)_localctx).operator = (Token)_errHandler.recoverInline(this);
				}
				consume();
				setState(439); expression(0);
				}
				break;

			case 4:
				_localctx = new LocalVariableAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(441); match(1);
				setState(442); match(ID);
				setState(443); match(18);
				setState(444); expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IndexableContext extends ParserRuleContext {
		public IndexableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexable; }
	 
		public IndexableContext() { }
		public void copyFrom(IndexableContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class TypecastIndexableContext extends IndexableContext {
		public TypecastContext typecast() {
			return getRuleContext(TypecastContext.class,0);
		}
		public TypecastIndexableContext(IndexableContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitTypecastIndexable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NativeIndexableContext extends IndexableContext {
		public NativeReferenceContext nativeReference() {
			return getRuleContext(NativeReferenceContext.class,0);
		}
		public NativeIndexableContext(IndexableContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitNativeIndexable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DereferenceIndexableContext extends IndexableContext {
		public TerminalNode ID() { return getToken(BlazeParser.ID, 0); }
		public DereferenceIndexableContext(IndexableContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitDereferenceIndexable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpressionIndexableContext extends IndexableContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExpressionIndexableContext(IndexableContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitExpressionIndexable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LiteralIndexableContext extends IndexableContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public LiteralIndexableContext(IndexableContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitLiteralIndexable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IndexableContext indexable() throws RecognitionException {
		IndexableContext _localctx = new IndexableContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_indexable);
		try {
			setState(455);
			switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
			case 1:
				_localctx = new ExpressionIndexableContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(447); match(30);
				setState(448); expression(0);
				setState(449); match(15);
				}
				break;

			case 2:
				_localctx = new TypecastIndexableContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(451); typecast();
				}
				break;

			case 3:
				_localctx = new LiteralIndexableContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(452); literal();
				}
				break;

			case 4:
				_localctx = new DereferenceIndexableContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(453); match(ID);
				}
				break;

			case 5:
				_localctx = new NativeIndexableContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(454); nativeReference();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NativeReferenceContext extends ParserRuleContext {
		public PackageDeclarationContext packageDeclaration() {
			return getRuleContext(PackageDeclarationContext.class,0);
		}
		public NativeReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nativeReference; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitNativeReference(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NativeReferenceContext nativeReference() throws RecognitionException {
		NativeReferenceContext _localctx = new NativeReferenceContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_nativeReference);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(457); match(41);
			setState(458); packageDeclaration();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public int _p;
		public VariableContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public VariableContext(ParserRuleContext parent, int invokingState, int _p) {
			super(parent, invokingState);
			this._p = _p;
		}
		@Override public int getRuleIndex() { return RULE_variable; }
	 
		public VariableContext() { }
		public void copyFrom(VariableContext ctx) {
			super.copyFrom(ctx);
			this._p = ctx._p;
		}
	}
	public static class IndexableVariableContext extends VariableContext {
		public IndexableContext indexable() {
			return getRuleContext(IndexableContext.class,0);
		}
		public IndexableVariableContext(VariableContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitIndexableVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FieldLookupVariableContext extends VariableContext {
		public TerminalNode ID() { return getToken(BlazeParser.ID, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public FieldLookupVariableContext(VariableContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitFieldLookupVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IndexLookupVariableContext extends VariableContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public IndexLookupVariableContext(VariableContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitIndexLookupVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableContext variable(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		VariableContext _localctx = new VariableContext(_ctx, _parentState, _p);
		VariableContext _prevctx = _localctx;
		int _startState = 58;
		enterRecursionRule(_localctx, RULE_variable);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new IndexableVariableContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(461); indexable();
			}
			_ctx.stop = _input.LT(-1);
			setState(473);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,53,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(471);
					switch ( getInterpreter().adaptivePredict(_input,52,_ctx) ) {
					case 1:
						{
						_localctx = new FieldLookupVariableContext(new VariableContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_variable);
						setState(463);
						if (!(3 >= _localctx._p)) throw new FailedPredicateException(this, "3 >= $_p");
						setState(464); match(48);
						setState(465); match(ID);
						}
						break;

					case 2:
						{
						_localctx = new IndexLookupVariableContext(new VariableContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_variable);
						setState(466);
						if (!(2 >= _localctx._p)) throw new FailedPredicateException(this, "2 >= $_p");
						setState(467); match(4);
						setState(468); expression(0);
						setState(469); match(24);
						}
						break;
					}
					} 
				}
				setState(475);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,53,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class CallableContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ParallelizeContext parallelize() {
			return getRuleContext(ParallelizeContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public CallableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callable; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitCallable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallableContext callable() throws RecognitionException {
		CallableContext _localctx = new CallableContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_callable);
		try {
			setState(482);
			switch ( getInterpreter().adaptivePredict(_input,54,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(476); match(30);
				setState(477); expression(0);
				setState(478); match(15);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(480); parallelize();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(481); variable(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallParametersContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public CallParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callParameters; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitCallParameters(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallParametersContext callParameters() throws RecognitionException {
		CallParametersContext _localctx = new CallParametersContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_callParameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(484); match(30);
			setState(493);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 4) | (1L << 7) | (1L << 11) | (1L << 12) | (1L << 16) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 23) | (1L << 26) | (1L << 29) | (1L << 30) | (1L << 32) | (1L << 35) | (1L << 38) | (1L << 41) | (1L << 46) | (1L << 49) | (1L << 50) | (1L << 51) | (1L << 59) | (1L << 61) | (1L << INT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (FLOAT - 64)) | (1L << (BOOL - 64)) | (1L << (STRING - 64)) | (1L << (NULL - 64)) | (1L << (ID - 64)))) != 0)) {
				{
				setState(485); expression(0);
				setState(490);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==26) {
					{
					{
					setState(486); match(26);
					setState(487); expression(0);
					}
					}
					setState(492);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(495); match(15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParallelizeContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ParallelizeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parallelize; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitParallelize(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParallelizeContext parallelize() throws RecognitionException {
		ParallelizeContext _localctx = new ParallelizeContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_parallelize);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(497); match(61);
			setState(498); expression(0);
			setState(499); match(61);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallContext extends ParserRuleContext {
		public int _p;
		public CallContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public CallContext(ParserRuleContext parent, int invokingState, int _p) {
			super(parent, invokingState);
			this._p = _p;
		}
		@Override public int getRuleIndex() { return RULE_call; }
	 
		public CallContext() { }
		public void copyFrom(CallContext ctx) {
			super.copyFrom(ctx);
			this._p = ctx._p;
		}
	}
	public static class ChainCallContext extends CallContext {
		public CallParametersContext callParameters() {
			return getRuleContext(CallParametersContext.class,0);
		}
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public TerminalNode ID() { return getToken(BlazeParser.ID, 0); }
		public ChainCallContext(CallContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitChainCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NativeCallContext extends CallContext {
		public CallParametersContext callParameters() {
			return getRuleContext(CallParametersContext.class,0);
		}
		public NativeReferenceContext nativeReference() {
			return getRuleContext(NativeReferenceContext.class,0);
		}
		public NativeCallContext(CallContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitNativeCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultiCallContext extends CallContext {
		public CallParametersContext callParameters() {
			return getRuleContext(CallParametersContext.class,0);
		}
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public MultiCallContext(CallContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitMultiCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CallableCallContext extends CallContext {
		public CallParametersContext callParameters() {
			return getRuleContext(CallParametersContext.class,0);
		}
		public CallableContext callable() {
			return getRuleContext(CallableContext.class,0);
		}
		public CallableCallContext(CallContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitCallableCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FieldCallContext extends CallContext {
		public CallParametersContext callParameters() {
			return getRuleContext(CallParametersContext.class,0);
		}
		public CallableContext callable() {
			return getRuleContext(CallableContext.class,0);
		}
		public TerminalNode ID() { return getToken(BlazeParser.ID, 0); }
		public FieldCallContext(CallContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitFieldCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallContext call(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		CallContext _localctx = new CallContext(_ctx, _parentState, _p);
		CallContext _prevctx = _localctx;
		int _startState = 66;
		enterRecursionRule(_localctx, RULE_call);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(513);
			switch ( getInterpreter().adaptivePredict(_input,57,_ctx) ) {
			case 1:
				{
				_localctx = new FieldCallContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(502); callable();
				setState(503); match(48);
				setState(504); match(ID);
				setState(505); callParameters();
				}
				break;

			case 2:
				{
				_localctx = new CallableCallContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(507); callable();
				setState(508); callParameters();
				}
				break;

			case 3:
				{
				_localctx = new NativeCallContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(510); nativeReference();
				setState(511); callParameters();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(523);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,59,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(521);
					switch ( getInterpreter().adaptivePredict(_input,58,_ctx) ) {
					case 1:
						{
						_localctx = new MultiCallContext(new CallContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_call);
						setState(515);
						if (!(2 >= _localctx._p)) throw new FailedPredicateException(this, "2 >= $_p");
						setState(516); callParameters();
						}
						break;

					case 2:
						{
						_localctx = new ChainCallContext(new CallContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_call);
						setState(517);
						if (!(1 >= _localctx._p)) throw new FailedPredicateException(this, "1 >= $_p");
						setState(518); match(48);
						setState(519); match(ID);
						setState(520); callParameters();
						}
						break;
					}
					} 
				}
				setState(525);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,59,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class InstantiationContext extends ParserRuleContext {
		public InstantiationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instantiation; }
	 
		public InstantiationContext() { }
		public void copyFrom(InstantiationContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NativeInstantiationContext extends InstantiationContext {
		public CallParametersContext callParameters() {
			return getRuleContext(CallParametersContext.class,0);
		}
		public NativeReferenceContext nativeReference() {
			return getRuleContext(NativeReferenceContext.class,0);
		}
		public NativeInstantiationContext(InstantiationContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitNativeInstantiation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstantiationContext instantiation() throws RecognitionException {
		InstantiationContext _localctx = new InstantiationContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_instantiation);
		try {
			_localctx = new NativeInstantiationContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(526); match(20);
			setState(527); nativeReference();
			setState(529);
			switch ( getInterpreter().adaptivePredict(_input,60,_ctx) ) {
			case 1:
				{
				setState(528); callParameters();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public Token channel;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public MeltedCopyContext meltedCopy() {
			return getRuleContext(MeltedCopyContext.class,0);
		}
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public ParallelizeContext parallelize() {
			return getRuleContext(ParallelizeContext.class,0);
		}
		public TerminalNode ID() { return getToken(BlazeParser.ID, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public TypecastContext typecast() {
			return getRuleContext(TypecastContext.class,0);
		}
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_atom);
		try {
			setState(543);
			switch ( getInterpreter().adaptivePredict(_input,61,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(531); meltedCopy();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(532); typecast();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(533); parallelize();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(534); call(0);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(535); variable(0);
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(536); match(20);
				setState(537); ((AtomContext)_localctx).channel = match(ID);
				setState(538);
				if (!( ((AtomContext)_localctx).channel.getText().equals("Channel") )) throw new FailedPredicateException(this, " $channel.getText().equals(\"Channel\") ");
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(539); match(30);
				setState(540); expression(0);
				setState(541); match(15);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlazeTypeContext extends ParserRuleContext {
		public BlazeTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blazeType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BlazeVisitor ) return ((BlazeVisitor<? extends T>)visitor).visitBlazeType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlazeTypeContext blazeType() throws RecognitionException {
		BlazeTypeContext _localctx = new BlazeTypeContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_blazeType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(545);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 7) | (1L << 11) | (1L << 16) | (1L << 17) | (1L << 23) | (1L << 35) | (1L << 41) | (1L << 49) | (1L << 51) | (1L << 59))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 8: return loopStatement_sempred((LoopStatementContext)_localctx, predIndex);

		case 15: return expression_sempred((ExpressionContext)_localctx, predIndex);

		case 29: return variable_sempred((VariableContext)_localctx, predIndex);

		case 33: return call_sempred((CallContext)_localctx, predIndex);

		case 35: return atom_sempred((AtomContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1: return 10 >= _localctx._p;

		case 2: return 9 >= _localctx._p;

		case 3: return 8 >= _localctx._p;

		case 4: return 7 >= _localctx._p;

		case 5: return 6 >= _localctx._p;

		case 6: return 5 >= _localctx._p;
		}
		return true;
	}
	private boolean atom_sempred(AtomContext _localctx, int predIndex) {
		switch (predIndex) {
		case 11: return  ((AtomContext)_localctx).channel.getText().equals("Channel") ;
		}
		return true;
	}
	private boolean call_sempred(CallContext _localctx, int predIndex) {
		switch (predIndex) {
		case 9: return 2 >= _localctx._p;

		case 10: return 1 >= _localctx._p;
		}
		return true;
	}
	private boolean loopStatement_sempred(LoopStatementContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return  ((ForLoopContext)_localctx).in.getText().equals("in") ;
		}
		return true;
	}
	private boolean variable_sempred(VariableContext _localctx, int predIndex) {
		switch (predIndex) {
		case 7: return 3 >= _localctx._p;

		case 8: return 2 >= _localctx._p;
		}
		return true;
	}

	public static final String _serializedATN =
		"\2\3J\u0226\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4"+
		"\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20"+
		"\4\21\t\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27"+
		"\4\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36"+
		"\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\3\2\3\2\3\3\5\3"+
		"P\n\3\3\3\3\3\3\3\3\3\7\3V\n\3\f\3\16\3Y\13\3\3\3\5\3\\\n\3\5\3^\n\3\3"+
		"\4\5\4a\n\4\3\4\3\4\3\4\3\4\7\4g\n\4\f\4\16\4j\13\4\3\4\5\4m\n\4\5\4o"+
		"\n\4\3\5\5\5r\n\5\3\5\3\5\3\5\3\5\3\5\5\5y\n\5\3\5\5\5|\n\5\3\6\5\6\177"+
		"\n\6\3\6\6\6\u0082\n\6\r\6\16\6\u0083\3\6\6\6\u0087\n\6\r\6\16\6\u0088"+
		"\5\6\u008b\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0098\n"+
		"\7\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00a0\n\b\3\b\3\b\3\b\3\b\6\b\u00a6\n\b"+
		"\r\b\16\b\u00a7\5\b\u00aa\n\b\3\t\3\t\3\t\3\t\3\t\5\t\u00b1\n\t\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00be\n\n\3\13\3\13\3\13\3"+
		"\13\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\5\r\u00cc\n\r\3\16\3\16\3\16\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u00dc\n\17\3\20"+
		"\3\20\3\20\7\20\u00e1\n\20\f\20\16\20\u00e4\13\20\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\5\21\u00ed\n\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\7\21\u0101\n\21\f\21"+
		"\16\21\u0104\13\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u010e"+
		"\n\22\3\23\5\23\u0111\n\23\3\23\3\23\3\23\3\23\7\23\u0117\n\23\f\23\16"+
		"\23\u011a\13\23\5\23\u011c\n\23\3\23\3\23\3\24\5\24\u0121\n\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\7\24\u012c\n\24\f\24\16\24\u012f"+
		"\13\24\5\24\u0131\n\24\3\24\3\24\3\25\3\25\3\25\3\25\7\25\u0139\n\25\f"+
		"\25\16\25\u013c\13\25\5\25\u013e\n\25\3\25\3\25\3\26\3\26\3\26\3\26\3"+
		"\26\3\26\5\26\u0148\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\5\26\u0154\n\26\3\26\3\26\5\26\u0158\n\26\3\27\3\27\3\27\5\27\u015d"+
		"\n\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27\u0166\n\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\5\27\u016e\n\27\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31"+
		"\3\31\3\31\3\32\3\32\3\32\7\32\u017d\n\32\f\32\16\32\u0180\13\32\5\32"+
		"\u0182\n\32\3\32\3\32\3\32\5\32\u0187\n\32\3\32\3\32\3\32\3\32\7\32\u018d"+
		"\n\32\f\32\16\32\u0190\13\32\5\32\u0192\n\32\3\32\3\32\3\32\5\32\u0197"+
		"\n\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32\u01a0\n\32\3\33\3\33\3\33"+
		"\3\33\3\33\7\33\u01a7\n\33\f\33\16\33\u01aa\13\33\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\5\34\u01c0\n\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35"+
		"\u01ca\n\35\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\7\37\u01da\n\37\f\37\16\37\u01dd\13\37\3 \3 \3 \3 \3 \3 \5"+
		" \u01e5\n \3!\3!\3!\3!\7!\u01eb\n!\f!\16!\u01ee\13!\5!\u01f0\n!\3!\3!"+
		"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\5#\u0204\n#\3#\3"+
		"#\3#\3#\3#\3#\7#\u020c\n#\f#\16#\u020f\13#\3$\3$\3$\5$\u0214\n$\3%\3%"+
		"\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\5%\u0222\n%\3&\3&\3&\2\'\2\4\6\b\n\f\16"+
		"\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJ\2\n\6\f\f\35\35"+
		"##&&\5\37\37\"\"\64\64\5\5\5\17\17??\4\37\37\64\64\6\7\7\n\n::@@\4\b\b"+
		">>\6\20\20\33\33**;<\13\t\t\r\r\22\23\31\31%%++\63\63\65\65==\u0264\2"+
		"L\3\2\2\2\4]\3\2\2\2\6n\3\2\2\2\bq\3\2\2\2\n\u008a\3\2\2\2\f\u0097\3\2"+
		"\2\2\16\u00a9\3\2\2\2\20\u00ab\3\2\2\2\22\u00bd\3\2\2\2\24\u00bf\3\2\2"+
		"\2\26\u00c3\3\2\2\2\30\u00c9\3\2\2\2\32\u00cd\3\2\2\2\34\u00db\3\2\2\2"+
		"\36\u00dd\3\2\2\2 \u00ec\3\2\2\2\"\u010d\3\2\2\2$\u0110\3\2\2\2&\u0120"+
		"\3\2\2\2(\u0134\3\2\2\2*\u0157\3\2\2\2,\u016d\3\2\2\2.\u016f\3\2\2\2\60"+
		"\u0174\3\2\2\2\62\u019f\3\2\2\2\64\u01a1\3\2\2\2\66\u01bf\3\2\2\28\u01c9"+
		"\3\2\2\2:\u01cb\3\2\2\2<\u01ce\3\2\2\2>\u01e4\3\2\2\2@\u01e6\3\2\2\2B"+
		"\u01f3\3\2\2\2D\u0203\3\2\2\2F\u0210\3\2\2\2H\u0221\3\2\2\2J\u0223\3\2"+
		"\2\2LM\5\4\3\2M\3\3\2\2\2NP\5\n\6\2ON\3\2\2\2OP\3\2\2\2PQ\3\2\2\2QW\5"+
		"\f\7\2RS\5\n\6\2ST\5\f\7\2TV\3\2\2\2UR\3\2\2\2VY\3\2\2\2WU\3\2\2\2WX\3"+
		"\2\2\2X[\3\2\2\2YW\3\2\2\2Z\\\5\n\6\2[Z\3\2\2\2[\\\3\2\2\2\\^\3\2\2\2"+
		"]O\3\2\2\2]^\3\2\2\2^\5\3\2\2\2_a\5\n\6\2`_\3\2\2\2`a\3\2\2\2ab\3\2\2"+
		"\2bh\5\f\7\2cd\5\n\6\2de\5\f\7\2eg\3\2\2\2fc\3\2\2\2gj\3\2\2\2hf\3\2\2"+
		"\2hi\3\2\2\2il\3\2\2\2jh\3\2\2\2km\5\n\6\2lk\3\2\2\2lm\3\2\2\2mo\3\2\2"+
		"\2n`\3\2\2\2no\3\2\2\2o\7\3\2\2\2pr\5\n\6\2qp\3\2\2\2qr\3\2\2\2rx\3\2"+
		"\2\2st\7(\2\2tu\5\6\4\2uv\7\13\2\2vy\3\2\2\2wy\5\f\7\2xs\3\2\2\2xw\3\2"+
		"\2\2y{\3\2\2\2z|\5\n\6\2{z\3\2\2\2{|\3\2\2\2|\t\3\2\2\2}\177\7#\2\2~}"+
		"\3\2\2\2~\177\3\2\2\2\177\u0080\3\2\2\2\u0080\u0082\7\35\2\2\u0081~\3"+
		"\2\2\2\u0082\u0083\3\2\2\2\u0083\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084"+
		"\u008b\3\2\2\2\u0085\u0087\78\2\2\u0086\u0085\3\2\2\2\u0087\u0088\3\2"+
		"\2\2\u0088\u0086\3\2\2\2\u0088\u0089\3\2\2\2\u0089\u008b\3\2\2\2\u008a"+
		"\u0081\3\2\2\2\u008a\u0086\3\2\2\2\u008b\13\3\2\2\2\u008c\u0098\5\16\b"+
		"\2\u008d\u0098\5*\26\2\u008e\u0098\5\20\t\2\u008f\u0098\5\22\n\2\u0090"+
		"\u0098\5\24\13\2\u0091\u0098\5\30\r\2\u0092\u0098\5\32\16\2\u0093\u0098"+
		"\5\34\17\2\u0094\u0098\5\26\f\2\u0095\u0098\5\64\33\2\u0096\u0098\5 \21"+
		"\2\u0097\u008c\3\2\2\2\u0097\u008d\3\2\2\2\u0097\u008e\3\2\2\2\u0097\u008f"+
		"\3\2\2\2\u0097\u0090\3\2\2\2\u0097\u0091\3\2\2\2\u0097\u0092\3\2\2\2\u0097"+
		"\u0093\3\2\2\2\u0097\u0094\3\2\2\2\u0097\u0095\3\2\2\2\u0097\u0096\3\2"+
		"\2\2\u0098\r\3\2\2\2\u0099\u009a\7/\2\2\u009a\u009b\7+\2\2\u009b\u009f"+
		"\5\36\20\2\u009c\u009d\7\62\2\2\u009d\u009e\7\5\2\2\u009e\u00a0\b\b\1"+
		"\2\u009f\u009c\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\u00aa\3\2\2\2\u00a1\u00a2"+
		"\7/\2\2\u00a2\u00aa\7D\2\2\u00a3\u00a5\7/\2\2\u00a4\u00a6\n\2\2\2\u00a5"+
		"\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a7\u00a8\3\2"+
		"\2\2\u00a8\u00aa\3\2\2\2\u00a9\u0099\3\2\2\2\u00a9\u00a1\3\2\2\2\u00a9"+
		"\u00a3\3\2\2\2\u00aa\17\3\2\2\2\u00ab\u00ac\7$\2\2\u00ac\u00ad\5 \21\2"+
		"\u00ad\u00b0\5\b\5\2\u00ae\u00af\7-\2\2\u00af\u00b1\5\b\5\2\u00b0\u00ae"+
		"\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\21\3\2\2\2\u00b2\u00b3\7\66\2\2\u00b3"+
		"\u00b4\7F\2\2\u00b4\u00b5\7F\2\2\u00b5\u00b6\5 \21\2\u00b6\u00b7\5\b\5"+
		"\2\u00b7\u00b8\6\n\2\3\u00b8\u00be\3\2\2\2\u00b9\u00ba\7\36\2\2\u00ba"+
		"\u00bb\5 \21\2\u00bb\u00bc\5\b\5\2\u00bc\u00be\3\2\2\2\u00bd\u00b2\3\2"+
		"\2\2\u00bd\u00b9\3\2\2\2\u00be\23\3\2\2\2\u00bf\u00c0\79\2\2\u00c0\u00c1"+
		"\5 \21\2\u00c1\u00c2\5\b\5\2\u00c2\25\3\2\2\2\u00c3\u00c4\7\27\2\2\u00c4"+
		"\u00c5\7F\2\2\u00c5\u00c6\7.\2\2\u00c6\u00c7\5 \21\2\u00c7\u00c8\5\b\5"+
		"\2\u00c8\27\3\2\2\2\u00c9\u00cb\7\67\2\2\u00ca\u00cc\5 \21\2\u00cb\u00ca"+
		"\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\31\3\2\2\2\u00cd\u00ce\7,\2\2\u00ce"+
		"\u00cf\5 \21\2\u00cf\33\3\2\2\2\u00d0\u00d1\7\61\2\2\u00d1\u00d2\5<\37"+
		"\2\u00d2\u00d3\7\62\2\2\u00d3\u00d4\7F\2\2\u00d4\u00dc\3\2\2\2\u00d5\u00d6"+
		"\7\61\2\2\u00d6\u00d7\5<\37\2\u00d7\u00d8\7\6\2\2\u00d8\u00d9\5 \21\2"+
		"\u00d9\u00da\7\32\2\2\u00da\u00dc\3\2\2\2\u00db\u00d0\3\2\2\2\u00db\u00d5"+
		"\3\2\2\2\u00dc\35\3\2\2\2\u00dd\u00e2\7F\2\2\u00de\u00df\7\62\2\2\u00df"+
		"\u00e1\7F\2\2\u00e0\u00de\3\2\2\2\u00e1\u00e4\3\2\2\2\u00e2\u00e0\3\2"+
		"\2\2\u00e2\u00e3\3\2\2\2\u00e3\37\3\2\2\2\u00e4\u00e2\3\2\2\2\u00e5\u00e6"+
		"\b\21\1\2\u00e6\u00e7\t\3\2\2\u00e7\u00ed\5 \21\2\u00e8\u00ed\5\66\34"+
		"\2\u00e9\u00ed\5F$\2\u00ea\u00ed\5H%\2\u00eb\u00ed\5,\27\2\u00ec\u00e5"+
		"\3\2\2\2\u00ec\u00e8\3\2\2\2\u00ec\u00e9\3\2\2\2\u00ec\u00ea\3\2\2\2\u00ec"+
		"\u00eb\3\2\2\2\u00ed\u0102\3\2\2\2\u00ee\u00ef\6\21\3\3\u00ef\u00f0\t"+
		"\4\2\2\u00f0\u0101\5 \21\2\u00f1\u00f2\6\21\4\3\u00f2\u00f3\t\5\2\2\u00f3"+
		"\u0101\5 \21\2\u00f4\u00f5\6\21\5\3\u00f5\u00f6\t\6\2\2\u00f6\u0101\5"+
		" \21\2\u00f7\u00f8\6\21\6\3\u00f8\u00f9\t\7\2\2\u00f9\u0101\5 \21\2\u00fa"+
		"\u00fb\6\21\7\3\u00fb\u00fc\7)\2\2\u00fc\u0101\5 \21\2\u00fd\u00fe\6\21"+
		"\b\3\u00fe\u00ff\7\4\2\2\u00ff\u0101\5 \21\2\u0100\u00ee\3\2\2\2\u0100"+
		"\u00f1\3\2\2\2\u0100\u00f4\3\2\2\2\u0100\u00f7\3\2\2\2\u0100\u00fa\3\2"+
		"\2\2\u0100\u00fd\3\2\2\2\u0101\u0104\3\2\2\2\u0102\u0100\3\2\2\2\u0102"+
		"\u0103\3\2\2\2\u0103!\3\2\2\2\u0104\u0102\3\2\2\2\u0105\u010e\7A\2\2\u0106"+
		"\u010e\7B\2\2\u0107\u010e\7D\2\2\u0108\u010e\7C\2\2\u0109\u010e\5$\23"+
		"\2\u010a\u010e\5&\24\2\u010b\u010e\5(\25\2\u010c\u010e\7E\2\2\u010d\u0105"+
		"\3\2\2\2\u010d\u0106\3\2\2\2\u010d\u0107\3\2\2\2\u010d\u0108\3\2\2\2\u010d"+
		"\u0109\3\2\2\2\u010d\u010a\3\2\2\2\u010d\u010b\3\2\2\2\u010d\u010c\3\2"+
		"\2\2\u010e#\3\2\2\2\u010f\u0111\7\65\2\2\u0110\u010f\3\2\2\2\u0110\u0111"+
		"\3\2\2\2\u0111\u0112\3\2\2\2\u0112\u011b\7\6\2\2\u0113\u0118\5 \21\2\u0114"+
		"\u0115\7\34\2\2\u0115\u0117\5 \21\2\u0116\u0114\3\2\2\2\u0117\u011a\3"+
		"\2\2\2\u0118\u0116\3\2\2\2\u0118\u0119\3\2\2\2\u0119\u011c\3\2\2\2\u011a"+
		"\u0118\3\2\2\2\u011b\u0113\3\2\2\2\u011b\u011c\3\2\2\2\u011c\u011d\3\2"+
		"\2\2\u011d\u011e\7\32\2\2\u011e%\3\2\2\2\u011f\u0121\7=\2\2\u0120\u011f"+
		"\3\2\2\2\u0120\u0121\3\2\2\2\u0121\u0122\3\2\2\2\u0122\u0130\7\6\2\2\u0123"+
		"\u0124\5 \21\2\u0124\u0125\7!\2\2\u0125\u012d\5 \21\2\u0126\u0127\7\34"+
		"\2\2\u0127\u0128\5 \21\2\u0128\u0129\7!\2\2\u0129\u012a\5 \21\2\u012a"+
		"\u012c\3\2\2\2\u012b\u0126\3\2\2\2\u012c\u012f\3\2\2\2\u012d\u012b\3\2"+
		"\2\2\u012d\u012e\3\2\2\2\u012e\u0131\3\2\2\2\u012f\u012d\3\2\2\2\u0130"+
		"\u0123\3\2\2\2\u0130\u0131\3\2\2\2\u0131\u0132\3\2\2\2\u0132\u0133\7\32"+
		"\2\2\u0133\'\3\2\2\2\u0134\u013d\7\30\2\2\u0135\u013a\5 \21\2\u0136\u0137"+
		"\7\34\2\2\u0137\u0139\5 \21\2\u0138\u0136\3\2\2\2\u0139\u013c\3\2\2\2"+
		"\u013a\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u013e\3\2\2\2\u013c\u013a"+
		"\3\2\2\2\u013d\u0135\3\2\2\2\u013d\u013e\3\2\2\2\u013e\u013f\3\2\2\2\u013f"+
		"\u0140\7\30\2\2\u0140)\3\2\2\2\u0141\u0142\5<\37\2\u0142\u0143\7 \2\2"+
		"\u0143\u0144\5\62\32\2\u0144\u0145\7\21\2\2\u0145\u0147\7\16\2\2\u0146"+
		"\u0148\5\n\6\2\u0147\u0146\3\2\2\2\u0147\u0148\3\2\2\2\u0148\u0149\3\2"+
		"\2\2\u0149\u014a\7(\2\2\u014a\u014b\5\4\3\2\u014b\u014c\7\13\2\2\u014c"+
		"\u0158\3\2\2\2\u014d\u014e\5<\37\2\u014e\u014f\7 \2\2\u014f\u0150\5\62"+
		"\32\2\u0150\u0151\7\21\2\2\u0151\u0153\7\16\2\2\u0152\u0154\5\n\6\2\u0153"+
		"\u0152\3\2\2\2\u0153\u0154\3\2\2\2\u0154\u0155\3\2\2\2\u0155\u0156\5 "+
		"\21\2\u0156\u0158\3\2\2\2\u0157\u0141\3\2\2\2\u0157\u014d\3\2\2\2\u0158"+
		"+\3\2\2\2\u0159\u015a\5\62\32\2\u015a\u015c\7\16\2\2\u015b\u015d\5\n\6"+
		"\2\u015c\u015b\3\2\2\2\u015c\u015d\3\2\2\2\u015d\u015e\3\2\2\2\u015e\u015f"+
		"\7(\2\2\u015f\u0160\5\4\3\2\u0160\u0161\7\13\2\2\u0161\u016e\3\2\2\2\u0162"+
		"\u0163\5\62\32\2\u0163\u0165\7\16\2\2\u0164\u0166\5\n\6\2\u0165\u0164"+
		"\3\2\2\2\u0165\u0166\3\2\2\2\u0166\u0167\3\2\2\2\u0167\u0168\5 \21\2\u0168"+
		"\u016e\3\2\2\2\u0169\u016a\7(\2\2\u016a\u016b\5\4\3\2\u016b\u016c\7\13"+
		"\2\2\u016c\u016e\3\2\2\2\u016d\u0159\3\2\2\2\u016d\u0162\3\2\2\2\u016d"+
		"\u0169\3\2\2\2\u016e-\3\2\2\2\u016f\u0170\7\60\2\2\u0170\u0171\7 \2\2"+
		"\u0171\u0172\5 \21\2\u0172\u0173\7\21\2\2\u0173/\3\2\2\2\u0174\u0175\5"+
		"J&\2\u0175\u0176\7 \2\2\u0176\u0177\5 \21\2\u0177\u0178\7\21\2\2\u0178"+
		"\61\3\2\2\2\u0179\u017e\7F\2\2\u017a\u017b\7\34\2\2\u017b\u017d\7F\2\2"+
		"\u017c\u017a\3\2\2\2\u017d\u0180\3\2\2\2\u017e\u017c\3\2\2\2\u017e\u017f"+
		"\3\2\2\2\u017f\u0182\3\2\2\2\u0180\u017e\3\2\2\2\u0181\u0179\3\2\2\2\u0181"+
		"\u0182\3\2\2\2\u0182\u0186\3\2\2\2\u0183\u0184\7\34\2\2\u0184\u0185\7"+
		"F\2\2\u0185\u0187\7\'\2\2\u0186\u0183\3\2\2\2\u0186\u0187\3\2\2\2\u0187"+
		"\u01a0\3\2\2\2\u0188\u0191\7 \2\2\u0189\u018e\7F\2\2\u018a\u018b\7\34"+
		"\2\2\u018b\u018d\7F\2\2\u018c\u018a\3\2\2\2\u018d\u0190\3\2\2\2\u018e"+
		"\u018c\3\2\2\2\u018e\u018f\3\2\2\2\u018f\u0192\3\2\2\2\u0190\u018e\3\2"+
		"\2\2\u0191\u0189\3\2\2\2\u0191\u0192\3\2\2\2\u0192\u0196\3\2\2\2\u0193"+
		"\u0194\7\34\2\2\u0194\u0195\7F\2\2\u0195\u0197\7\'\2\2\u0196\u0193\3\2"+
		"\2\2\u0196\u0197\3\2\2\2\u0197\u0198\3\2\2\2\u0198\u01a0\7\21\2\2\u0199"+
		"\u019a\7F\2\2\u019a\u01a0\7\'\2\2\u019b\u019c\7 \2\2\u019c\u019d\7F\2"+
		"\2\u019d\u019e\7\'\2\2\u019e\u01a0\7\21\2\2\u019f\u0181\3\2\2\2\u019f"+
		"\u0188\3\2\2\2\u019f\u0199\3\2\2\2\u019f\u019b\3\2\2\2\u01a0\63\3\2\2"+
		"\2\u01a1\u01a2\5 \21\2\u01a2\u01a3\7.\2\2\u01a3\u01a8\5 \21\2\u01a4\u01a5"+
		"\7\34\2\2\u01a5\u01a7\5 \21\2\u01a6\u01a4\3\2\2\2\u01a7\u01aa\3\2\2\2"+
		"\u01a8\u01a6\3\2\2\2\u01a8\u01a9\3\2\2\2\u01a9\65\3\2\2\2\u01aa\u01a8"+
		"\3\2\2\2\u01ab\u01ac\5J&\2\u01ac\u01ad\7\62\2\2\u01ad\u01ae\7\25\2\2\u01ae"+
		"\u01af\7\62\2\2\u01af\u01b0\7F\2\2\u01b0\u01b1\7\24\2\2\u01b1\u01b2\5"+
		" \21\2\u01b2\u01c0\3\2\2\2\u01b3\u01b4\5<\37\2\u01b4\u01b5\7\24\2\2\u01b5"+
		"\u01b6\5 \21\2\u01b6\u01c0\3\2\2\2\u01b7\u01b8\5<\37\2\u01b8\u01b9\t\b"+
		"\2\2\u01b9\u01ba\5 \21\2\u01ba\u01c0\3\2\2\2\u01bb\u01bc\7\3\2\2\u01bc"+
		"\u01bd\7F\2\2\u01bd\u01be\7\24\2\2\u01be\u01c0\5 \21\2\u01bf\u01ab\3\2"+
		"\2\2\u01bf\u01b3\3\2\2\2\u01bf\u01b7\3\2\2\2\u01bf\u01bb\3\2\2\2\u01c0"+
		"\67\3\2\2\2\u01c1\u01c2\7 \2\2\u01c2\u01c3\5 \21\2\u01c3\u01c4\7\21\2"+
		"\2\u01c4\u01ca\3\2\2\2\u01c5\u01ca\5\60\31\2\u01c6\u01ca\5\"\22\2\u01c7"+
		"\u01ca\7F\2\2\u01c8\u01ca\5:\36\2\u01c9\u01c1\3\2\2\2\u01c9\u01c5\3\2"+
		"\2\2\u01c9\u01c6\3\2\2\2\u01c9\u01c7\3\2\2\2\u01c9\u01c8\3\2\2\2\u01ca"+
		"9\3\2\2\2\u01cb\u01cc\7+\2\2\u01cc\u01cd\5\36\20\2\u01cd;\3\2\2\2\u01ce"+
		"\u01cf\b\37\1\2\u01cf\u01d0\58\35\2\u01d0\u01db\3\2\2\2\u01d1\u01d2\6"+
		"\37\t\3\u01d2\u01d3\7\62\2\2\u01d3\u01da\7F\2\2\u01d4\u01d5\6\37\n\3\u01d5"+
		"\u01d6\7\6\2\2\u01d6\u01d7\5 \21\2\u01d7\u01d8\7\32\2\2\u01d8\u01da\3"+
		"\2\2\2\u01d9\u01d1\3\2\2\2\u01d9\u01d4\3\2\2\2\u01da\u01dd\3\2\2\2\u01db"+
		"\u01d9\3\2\2\2\u01db\u01dc\3\2\2\2\u01dc=\3\2\2\2\u01dd\u01db\3\2\2\2"+
		"\u01de\u01df\7 \2\2\u01df\u01e0\5 \21\2\u01e0\u01e1\7\21\2\2\u01e1\u01e5"+
		"\3\2\2\2\u01e2\u01e5\5B\"\2\u01e3\u01e5\5<\37\2\u01e4\u01de\3\2\2\2\u01e4"+
		"\u01e2\3\2\2\2\u01e4\u01e3\3\2\2\2\u01e5?\3\2\2\2\u01e6\u01ef\7 \2\2\u01e7"+
		"\u01ec\5 \21\2\u01e8\u01e9\7\34\2\2\u01e9\u01eb\5 \21\2\u01ea\u01e8\3"+
		"\2\2\2\u01eb\u01ee\3\2\2\2\u01ec\u01ea\3\2\2\2\u01ec\u01ed\3\2\2\2\u01ed"+
		"\u01f0\3\2\2\2\u01ee\u01ec\3\2\2\2\u01ef\u01e7\3\2\2\2\u01ef\u01f0\3\2"+
		"\2\2\u01f0\u01f1\3\2\2\2\u01f1\u01f2\7\21\2\2\u01f2A\3\2\2\2\u01f3\u01f4"+
		"\7?\2\2\u01f4\u01f5\5 \21\2\u01f5\u01f6\7?\2\2\u01f6C\3\2\2\2\u01f7\u01f8"+
		"\b#\1\2\u01f8\u01f9\5> \2\u01f9\u01fa\7\62\2\2\u01fa\u01fb\7F\2\2\u01fb"+
		"\u01fc\5@!\2\u01fc\u0204\3\2\2\2\u01fd\u01fe\5> \2\u01fe\u01ff\5@!\2\u01ff"+
		"\u0204\3\2\2\2\u0200\u0201\5:\36\2\u0201\u0202\5@!\2\u0202\u0204\3\2\2"+
		"\2\u0203\u01f7\3\2\2\2\u0203\u01fd\3\2\2\2\u0203\u0200\3\2\2\2\u0204\u020d"+
		"\3\2\2\2\u0205\u0206\6#\13\3\u0206\u020c\5@!\2\u0207\u0208\6#\f\3\u0208"+
		"\u0209\7\62\2\2\u0209\u020a\7F\2\2\u020a\u020c\5@!\2\u020b\u0205\3\2\2"+
		"\2\u020b\u0207\3\2\2\2\u020c\u020f\3\2\2\2\u020d\u020b\3\2\2\2\u020d\u020e"+
		"\3\2\2\2\u020eE\3\2\2\2\u020f\u020d\3\2\2\2\u0210\u0211\7\26\2\2\u0211"+
		"\u0213\5:\36\2\u0212\u0214\5@!\2\u0213\u0212\3\2\2\2\u0213\u0214\3\2\2"+
		"\2\u0214G\3\2\2\2\u0215\u0222\5.\30\2\u0216\u0222\5\60\31\2\u0217\u0222"+
		"\5B\"\2\u0218\u0222\5D#\2\u0219\u0222\5<\37\2\u021a\u021b\7\26\2\2\u021b"+
		"\u021c\7F\2\2\u021c\u0222\6%\r\3\u021d\u021e\7 \2\2\u021e\u021f\5 \21"+
		"\2\u021f\u0220\7\21\2\2\u0220\u0222\3\2\2\2\u0221\u0215\3\2\2\2\u0221"+
		"\u0216\3\2\2\2\u0221\u0217\3\2\2\2\u0221\u0218\3\2\2\2\u0221\u0219\3\2"+
		"\2\2\u0221\u021a\3\2\2\2\u0221\u021d\3\2\2\2\u0222I\3\2\2\2\u0223\u0224"+
		"\t\t\2\2\u0224K\3\2\2\2@OW[]`hlnqx{~\u0083\u0088\u008a\u0097\u009f\u00a7"+
		"\u00a9\u00b0\u00bd\u00cb\u00db\u00e2\u00ec\u0100\u0102\u010d\u0110\u0118"+
		"\u011b\u0120\u012d\u0130\u013a\u013d\u0147\u0153\u0157\u015c\u0165\u016d"+
		"\u017e\u0181\u0186\u018e\u0191\u0196\u019f\u01a8\u01bf\u01c9\u01d9\u01db"+
		"\u01e4\u01ec\u01ef\u0203\u020b\u020d\u0213\u0221";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
	}
}