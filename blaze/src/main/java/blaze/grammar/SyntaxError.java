package blaze.grammar;

/**
 * A generic syntax error.
 */
public class SyntaxError extends Exception {
    public SyntaxError(String message) {
        super(message);
    }
}