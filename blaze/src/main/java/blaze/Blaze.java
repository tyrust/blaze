package blaze;

import akka.kernel.Bootable;

import blaze.grammar.ParseEngine;
import blaze.grammar.SyntaxError;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BodyExpression;
import blaze.interpreter.expressions.literals.IntegerLiteral;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.InterpreterEngine;
import blaze.interpreter.signals.Signal;
import blaze.runtime.Configuration;
import blaze.concurrent.ConcurrencyEngine;

import jline.console.ConsoleReader;
import jline.console.UserInterruptException;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

import org.apache.commons.cli.Options;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.ParseException;

/**
 * The Blaze kernel: the highest-level interface to Blaze.
 * Provides a bootable layer atop the Akka kernel which the user interacts with.
 * Bridges the parser and interpreter and runs them on source code.
 */
public class Blaze implements Bootable {
    public static void main(String[] args) {
        Options options = new Options();
        
        Option compileOption = new Option("c", true, "compile Blaze source code to parsed file");
        compileOption.setArgs(Option.UNLIMITED_VALUES);
        compileOption.setLongOpt("compile");
        
        Option replOption = new Option("r", false, "run Blaze in REPL mode");
        replOption.setLongOpt("repl");
        
        Option helpOption = new Option("h", false, "print help message");
        helpOption.setLongOpt("help");
        
        options.addOption(helpOption);
        options.addOption(compileOption);
        options.addOption(replOption);
        
        CommandLineParser parser = new BasicParser();
        CommandLine cmd;
        
        String usage = "blaze [opts...] file1 file2 ...";
        
        try {
            cmd = parser.parse(options, args);
        } catch(ParseException ex) {
            System.err.println(ex.getMessage());
            new HelpFormatter().printHelp(usage, options);
            return;
        }
        
        if(cmd.hasOption("h")) {
            new HelpFormatter().printHelp(usage, options);
            return;
        }
        
        Blaze blaze = new Blaze();
        
        if(cmd.hasOption("c")) {
            for(String file : cmd.getOptionValues("c")) {
                System.out.println("Compiling '" + file + "' to '" + file + "c'");
                blaze.compileFile(file);
            }
            return;
        }
        
        blaze.startup();
        
        if(cmd.hasOption("r") || cmd.getArgs().length == 0) {
            blaze.repl();
            return;
        }
        
        if(cmd.getArgs().length > 0)
            blaze.runFile(cmd.getArgs()[0]);
        if(cmd.getArgs().length > 1)
            System.err.println("warning: command line arguments to Blaze programs currently unsupported");
    }
    
    /**
     * Provides a Read-Eval-Print Loop (REPL) for Blaze. Lets users interact with the language
     * directly and provides immediate output.
     */
    public void repl() {
        String input;
        try {
            ConsoleReader in = new ConsoleReader();
            while(true) {
                try {
                    input = in.readLine("blaze> ");
                    if(input.equals("exit"))
                        break;
                    BodyExpression program = (BodyExpression)ParseEngine.parseCode(input);
                    for(BlazeExpression expr : program.getExpressions()) {
                        BlazeExpression result = InterpreterEngine.interpretExpression(expr);
                        System.out.println("=> " + result.toString());
                    }
                } catch(SyntaxError err) {
                } catch(RuntimeError err) {
                    System.err.println("Runtime error: " + err.getMessage());
                    InterpreterEngine.printStackTrace();
                } catch(NullPointerException ex) {
                    // Assume this is an EOF from Ctrl+D
                    break;
                } catch(UserInterruptException ex) {
                    continue;
                } catch(Signal sig) {
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch(IOException ex) {
            ex.printStackTrace();
        } finally {
            shutdown();
        }
    }
    
    /**
     * Loads the contents of a source code file into memory, parses it, and interprets it.
     */
    public void runFile(String filePath) {
        try {
            InterpreterEngine.runProgram(ParseEngine.parseCodeFile(filePath));
        } catch(SyntaxError err) {
            System.err.println("Syntax error in '" + filePath + "'");
            System.err.println(err.getMessage());
        } catch(RuntimeError err) {
            System.err.println("Runtime error: " + err.getMessage());
            InterpreterEngine.printStackTrace();
        } catch(Signal sig) {
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            shutdown();
        }
    }
    
    /**
     * Loads the contents of a source code file into memory and parses it into an evaluatable
     * expression tree. Writes the tree out to a file so that it can be loaded and interpreted
     * very quickly (without any of the overhead of parsing or assembling the expression tree).
     */
    public void compileFile(String filePath) {
        try {
            BlazeExpression program = ParseEngine.parseCodeFile(filePath);
            FileOutputStream fileOut = new FileOutputStream(filePath + "c");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(program);
            out.close();
            fileOut.close();
        } catch(SyntaxError err) {
            System.err.println("Syntax error in '" + filePath + "'");
            System.err.println(err.getMessage());
        } catch(IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Initializes the Blaze kernel.
     */
    public void startup() {
        // Set up interpreter-level things:
        // Namely, the global environment, the callstack, main libraries.
        InterpreterEngine.startup();
        
        // Set up the concurrency engine and the Akka actor system.
        ConcurrencyEngine.startup();
    }
    
    /**
     * Shuts down the Blaze kernel.
     */
    public void shutdown() {
        // Tear down the concurrency engine.
        ConcurrencyEngine.shutdown();
    }
}
