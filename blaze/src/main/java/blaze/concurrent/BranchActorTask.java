package blaze.concurrent;

import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.environment.Environment;

/**
 * Contains all of the information necessary for a branch actor to execute part of
 * a body in parallel.
 */
public class BranchActorTask {
    BlazeExpression _expression;
    Environment _environment;
    
    public BranchActorTask(BlazeExpression expression, Environment context) {
        _expression = expression;
        _environment = context;
    }
    
    public BlazeExpression getExpression() {
        return _expression;
    }
    
    public Environment getEnvironment() {
        return _environment;
    }
}