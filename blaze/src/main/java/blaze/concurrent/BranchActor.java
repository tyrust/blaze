package blaze.concurrent;

import akka.actor.UntypedActor;
import akka.actor.Props;

import blaze.interpreter.RuntimeError;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.signals.ReturnSignal;
import blaze.interpreter.signals.BranchSignal;

/**
 * An actor that runs a BranchActorTask. Essentially, runs a piece of
 * Blaze code on a separate worker (be it a thread, process, ...)
 */
public class BranchActor extends UntypedActor {
    public static enum BranchActorMessage {
        FINISHED
    };
    
    /**
     * Upon receiving a BranchActorTask, runs the encapsulated Blaze code.
     */
    @Override
    public void onReceive(Object message) {
        if(message instanceof BranchActorTask) {
            BranchActorTask bat = (BranchActorTask)message;
            
            try {
                getSender().tell(bat.getExpression().evaluate(bat.getEnvironment()), getSelf());
            } catch(RuntimeError err) {
                getSender().tell(err, getSelf());
            } catch(Signal sig) {
                getSender().tell(sig, getSelf());
            }
            
            // Signal to the caller that we are done
            // This is very important: if nothing was messaged to the ask()ing method,
            // Akka doesn't know we are done yet because it has received nothing, and
            // it will block until the timeout (which is infinite).
            getSender().tell(BranchActorMessage.FINISHED, getSelf());
        } else
            unhandled(message);
    }
}