package blaze.concurrent;

import akka.actor.ActorSystem;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.dispatch.Futures;
import akka.dispatch.Recover;
import akka.util.Timeout;
import static akka.pattern.Patterns.ask;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import blaze.interpreter.InterpreterEngine;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.literals.ClosureLiteral;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.runtime.Console;

import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.Callable;
import static java.util.concurrent.TimeUnit.DAYS;

/**
 * High-level access to the underlying Akka microkernel and actor system that powers Blaze's concurrency.
 * Schedules pieces of Blaze code to run in parallel.
 * The actor system model used here is extremely extensible and powerful. This detaches the concurrency
 * directives in the Blaze language from the language and from the interpreter. The interpreter has no
 * knowledge of how the work is done, and does not busy-wait for the work to complete; it simply is
 * signalled when work completes, and work can be deployed in whatever way.
 */
public class ConcurrencyEngine {
    private static ActorSystem _actorSystem;
    private static int _currentWorkerId = 0, _currentJobId = 0;
    private static Map<Integer, List<ActorRef>> _actorsForJobs = new HashMap<Integer, List<ActorRef>>();
    
    private static String nextWorkerName() {
        return "worker" + _currentWorkerId++;
    }
    
    private static int nextJobId() {
        return _currentJobId++;
    }
    
    /**
     * Allocates and returns a list of worker actors for a job ID. You must free this job
     * or the actors will persist in memory idly.
     */
    private static synchronized List<ActorRef> allocateWorkers(int jobId, int amount) {
        if(_actorsForJobs.get(jobId) == null)
            _actorsForJobs.put(jobId, new LinkedList<ActorRef>());
        for(int i = 0; i < amount; i++) {
            ActorRef actor = _actorSystem.actorOf(new Props(BranchActor.class), nextWorkerName());
            _actorsForJobs.get(jobId).add(actor);
        }
        return _actorsForJobs.get(jobId);
    }
    
    /**
     * Frees the workers allocated for a job.
     */
    private static synchronized void freeWorkers(int jobId) {
        if(_actorsForJobs.get(jobId) == null)
            return;
        for(ActorRef actor : _actorsForJobs.get(jobId))
            _actorSystem.stop(actor);
        _actorsForJobs.remove(jobId);
    }
    
    /**
     * Creates a job with actors for each of a list of tasks, and schedules the tasks to be
     * completed. How the actors deploy their tasks, in what order, and so on, is up to the
     * Akka runtime.
     */
    public static Iterable<Object> runConcurrently(List<BranchActorTask> tasks) {
        int id = nextJobId();
        List<ActorRef> workers = allocateWorkers(id, tasks.size());
        
        List<Future<Object>> futureReturnValues = new LinkedList<Future<Object>>();
        for(int i = 0; i < tasks.size(); i++)
            // ugly hack because Akka does not have an infinite timeout; breaks after 250 years
            futureReturnValues.add(ask(workers.get(i), tasks.get(i), new Timeout(91250, DAYS)));
        Future<Iterable<Object>> aggregateFuture = Futures.sequence(futureReturnValues, _actorSystem.dispatcher());
        
        try {
            return (Iterable<Object>)Await.result(aggregateFuture, Duration.Inf());
        } catch(Exception ex) {
            ex.printStackTrace();
            // TODO: handle these better
            // (also figure out what they mean)
        } finally {
            freeWorkers(id);
        }
        
        return new LinkedList<Object>();
    }
    
    /**
     * Calls a Blaze closure asynchronously at an indeterminate time in the future.
     * Returns a promise to the result of invoking the closure.
     */
    public static Future<BlazeExpression> invokeInFuture(final ClosureLiteral closure, final List<BlazeExpression> arguments) {
        return Futures.future(new Callable<BlazeExpression>() {
            public BlazeExpression call() throws RuntimeError, Signal {
                return closure.invoke(arguments);
            }
        }, _actorSystem.dispatcher()).recover(new Recover<BlazeExpression>() {
            public BlazeExpression recover(Throwable t) {
                if(t instanceof RuntimeError) {
                    System.err.println("Runtime error: " + t.getMessage());
                    System.err.println("On a secondary worker, running code:");
                    Console.printIndentedError(closure.toIndentedString());
                    System.err.println("(not terminating program)");
                }
                return new NullLiteral();
            }
        }, _actorSystem.dispatcher());
    }
    
    /**
     * Initializes the concurrency engine and the backing actor system.
     */
    public static void startup() {
        _actorSystem = ActorSystem.create();
    }
    
    /**
     * Shuts down the concurrency engine and tears down the backing actor system.
     */
    public static void shutdown() {
        _actorSystem.shutdown();
    }
}