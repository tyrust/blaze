package blaze.runtime;

import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.literals.ListLiteral;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * A collection of runtime helpers for implementing Blaze libraries.
 * Enable otherwise impossible things, such as interacting directly with
 * Blaze types in terms of their native implementations.
 */
public class RuntimeHelpers {
    public static Object[] subList(Object[] list, Integer start, Integer end) {
        return Arrays.copyOfRange(list, start.intValue(), end.intValue());
    }
    
    public static void listAppend(ListLiteral list, BlazeExpression expr) {
        list.getValue().add(expr);
    }
}