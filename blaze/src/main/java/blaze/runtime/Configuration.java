package blaze.runtime;

import java.io.File;
import java.util.Stack;

/**
 * Global configuration options.
 */
public class Configuration {
    private static Stack<String> _workingDirectories = new Stack<String>();
    public static boolean PRINT_AST = false;
    
    public static void pushWorkingDirectory(String dir) {
        _workingDirectories.push(dir);
    }
    
    public static void popWorkingDirectory() {
        _workingDirectories.pop();
    }
    
    public static String getBlazePath() {
        String path = System.getenv("BLAZEPATH");
        if(path == null)
            return "./lib";
        return path;
    }
    
    public static String getPathForLibraryPackage(String name) {
        return new File(getBlazePath(), name + ".blaze").getAbsolutePath();
    }
    
    public static String getCompiledPathForLibraryPackage(String name) {
        return getPathForLibraryPackage(name) + "c";
    }
    
    public static String getAbsolutePath(String path) {
        File file = new File(path);
        if(file.isAbsolute())
            return path;
        if(!_workingDirectories.isEmpty())
            return new File(_workingDirectories.peek(), path).getAbsolutePath();
        return file.getAbsolutePath();
    }
}