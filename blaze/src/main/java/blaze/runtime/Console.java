package blaze.runtime;

import java.io.PrintStream;

/**
 * A collection of console utilities for use within the Blaze interpreter / in the language.
 */
public class Console {
    private static int _indentationLevel = 0;
    
    /**
     * Increases the global indentation level for the indented printer.
     */
    public static void indent() {
        _indentationLevel++;
    }
    
    /**
     * Decreases the global indentation level for the indented printer.
     */
    public static void dedent() {
        _indentationLevel--;
    }
    
    private static void printIndentedHelper(PrintStream stream, String str) {
        for(String line : str.split("\n"))
            stream.println(line);
    }
    
    /**
     * Indents the string appropriately.
     */
    public static String indent(String str) {
        String s = "";
        for(int i = 0; i < _indentationLevel*4; i++)
            s += " ";
        return s + str;
    }
    
    /**
     * Prints the given string to standard output, with appropriate indentation.
     */
    public static void printIndented(String str) {
        printIndentedHelper(System.out, str);
    }
    
    /**
     * Prints the given string to standard error, with appropriate indentation.
     */
    public static void printIndentedError(String str) {
        printIndentedHelper(System.err, str);
    }
}