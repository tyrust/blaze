package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.ChannelLiteral;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;

import java.util.List;
import java.util.ArrayList;

/**
 * A push statement. Pushes the given expressions to the target variable.
 */
public class PushStatement extends BlazeExpression {
    private BlazeExpression _channel;
    private List<BlazeExpression> _values;
    
    public PushStatement(BlazeExpression channel, List<BlazeExpression> values) {
        _channel = channel;
        _values = values;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        BlazeExpression channel = _channel.evaluate(context);
        if(channel instanceof ChannelLiteral) {
            for(BlazeExpression expr : _values)
                ((ChannelLiteral)channel).push(expr.evaluate(context));
            return new NullLiteral();
        }
        throw new RuntimeError("cannot push to object of type " + channel.getBlazeType());
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.PUSH;
    }
    
    public String toString() {
        return _channel.toString() + " <- " + _values.toString();
    }
}