package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.LiteralExpression;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;

/**
 * A freeze statement. Freezes an expression so that its value is guaranteeably fixed.
 */
public class FreezeStatement extends BlazeExpression {
    private BlazeExpression _freezeMe;
    
    public FreezeStatement(BlazeExpression freezeMe) {
        _freezeMe = freezeMe;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        BlazeExpression expr = _freezeMe.evaluate(context);
        if(expr instanceof LiteralExpression) {
            ((LiteralExpression)expr).setFrozen(true);
            return new NullLiteral();
        }
        throw new RuntimeError("cannot freeze object of type " + expr.getBlazeType());
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.FREEZE;
    }
    
    public String toString() {
        return "freeze " + _freezeMe.toString();
    }
}