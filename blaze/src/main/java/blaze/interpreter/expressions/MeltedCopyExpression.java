package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.expressions.literals.LiteralExpression;

/**
 * "Melts" a frozen expression by returning a mutable copy.
 */
public class MeltedCopyExpression extends BlazeExpression {
    private BlazeExpression _meltMe;
    
    public MeltedCopyExpression(BlazeExpression meltMe) {
        _meltMe = meltMe;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        BlazeExpression expr = _meltMe.evaluate(context);
        if(expr instanceof LiteralExpression)
            return ((LiteralExpression)expr).meltedCopy();
        throw new RuntimeError("cannot melt object of type " + expr.getNodeType());
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.MELTED_COPY;
    }
    
    public String toString() {
        return "melted(" + _meltMe.toString() + ")";
    }
}