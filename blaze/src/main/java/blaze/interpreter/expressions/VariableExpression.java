package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.environment.Environment;

/**
 * A variable dereference expression. Searches an environment for a particular identifier.
 */
public class VariableExpression extends BlazeExpression {
    private BlazeExpression _parent;
    private Identifier _id;
    
    public VariableExpression(Identifier id, BlazeExpression parent) {
        _id = id;
        _parent = parent;
    }
    
    public VariableExpression(Identifier id) {
        this(id, null);
    }
    
    public Identifier getIdentifier() {
        return _id;
    }
    
    public BlazeExpression getParent() {
        return _parent;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        if(_parent != null)
            return new GetExpression(_parent, _id).evaluateInternal(context);
        else
            return context.lookup(_id);
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.VARIABLE;
    }
    
    public String toString() {
        String s = "";
        if(_parent != null)
            s += _parent + ".";
        return s + _id.toString();
    }
}