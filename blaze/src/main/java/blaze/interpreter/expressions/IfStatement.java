package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.expressions.literals.BooleanLiteral;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.runtime.Console;

import java.util.List;

/**
 * An if/else statement. Evaluates its predicate and branches conditionally.
 */
public class IfStatement extends BlazeExpression {
    private BlazeExpression _predicate, _consequent, _alternative;
    
    public IfStatement(BlazeExpression predicate, BlazeExpression consequent, BlazeExpression alternative) {
        _predicate = predicate;
        _consequent = consequent;
        _alternative = alternative;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        boolean pred = ((BooleanLiteral)_predicate.evaluate(context).coerceTo(SyntaxTreeTypes.BOOLEAN)).getValue();
        
        // Note that the branch that isn't visited is not even evaluated
        if(pred)
            return _consequent.evaluate(context);
        else if(_alternative != null)
            return _alternative.evaluate(context);
        else
            return new NullLiteral();
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.IF_ELSE;
    }
    
    public String toString() {
        return String.format("if %s %s else %s", _predicate.toString(), _consequent.toString(), _alternative == null ? "{}" : _alternative.toString());
    }
    
    public String toIndentedString() {
        String s = Console.indent("if " + _predicate + "\r\n");
        Console.indent();
        s += _consequent.toIndentedString();
        Console.dedent();
        if(_alternative != null) {
            s += "\r\n" + Console.indent("else\r\n");
            Console.indent();
            s += _alternative.toIndentedString();
            Console.dedent();
        }
        return s;
    }
}