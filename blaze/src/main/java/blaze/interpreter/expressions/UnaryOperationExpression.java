package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.environment.Environment;

import java.util.List;

/**
 * An expression involving a unary operator being applied to a sub-expression.
 */
public class UnaryOperationExpression extends BlazeExpression {
    private UnaryOperator _operator;
    private BlazeExpression _rhs;
    
    public UnaryOperationExpression(UnaryOperator operator, BlazeExpression rhs) {
        _operator = operator;
        _rhs = rhs;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        return _rhs.evaluate(context).doUnaryOperation(context, _operator);
    }
    
    public String getNodeType() {
        return _operator.getToken();
    }
    
    public String toString() {
        return _operator + " " + _rhs;
    }
}