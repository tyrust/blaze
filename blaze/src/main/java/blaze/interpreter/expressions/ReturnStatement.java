package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.signals.ReturnSignal;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.runtime.Console;

/**
 * A return statement. Simply returns whatever it's supposed to.
 */
public class ReturnStatement extends BlazeExpression {
    // The return statement returns something (possibly null)
    private BlazeExpression _returnMe;
    
    public ReturnStatement() {}
    
    public ReturnStatement(BlazeExpression returnMe) {
        _returnMe = returnMe;
        setIsStatement(true);
    }
    
    // Doesn't actually return anything here; rather, signals that a return is being made.
    // This allows the interpreter to later handle this signal appropriately.
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        BlazeExpression returnValue;
        if(_returnMe == null)
            returnValue = new NullLiteral();
        else
            returnValue = _returnMe.evaluate(context);
        throw new ReturnSignal(returnValue);
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.RETURN;
    }
    
    public String toString() {
        return "return " + _returnMe.toString();
    }
}