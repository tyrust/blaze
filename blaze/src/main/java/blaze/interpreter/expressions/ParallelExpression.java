package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.signals.BranchSignal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.expressions.literals.ParallelLiteral;

import java.util.List;
import java.util.ArrayList;

/**
 * A parallel expression. Evaluates all of the members and produces a literal parallel expression.
 */
public class ParallelExpression extends BlazeExpression {
    private List<BlazeExpression> _expressions;
    
    // Cache the returned parallel literal.
    // This prevents the expression from returning different literals every time it is evaluated.
    // That way, the "outward caching" algorithm that makes inside-out parallelism work can function
    // correctly and not infinitely recurse.
    private ParallelLiteral _literal;
    
    /**
     * Flattens a deep parallel expression into a single parallel expression.
     * (e.g. ||1,2|,|3,4|| -> |1,2,3,4|).
     * The flat representation is functionally equivalent but more performant in terms of memory
     * as it does not spawn workers that wait around for child workers to return.
     */
    private static List<BlazeExpression> flatten(List<BlazeExpression> expressions) {
        List<BlazeExpression> flattenedList = new ArrayList<BlazeExpression>();
        for(BlazeExpression expression : expressions) {
            if(expression.getNodeType().equals(SyntaxTreeTypes.PARALLEL_EXPRESSION))
                flattenedList.addAll(flatten(((ParallelExpression)expression)._expressions));
            else
                flattenedList.add(expression);
        }
        return flattenedList;
    }
    
    public ParallelExpression(List<BlazeExpression> expressions) {
        _expressions = expressions;
    }
    
    // Synchronized to solve an issue where the expression was evaluated while being evaluated elsewhere,
    // causing it to return a different parallel literal before assigning _literal.
    // This used to result in a race condition in which something like |1,2|,|3,4| could possibly
    // cause |3,4| to evaluate twice, resulting in output like (1,3),(1,4),(2,3),(2,4),(2,3),(2,4).
    protected synchronized BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        if(_literal != null)
            return _literal;
        
        List<BlazeExpression> list = new ArrayList<BlazeExpression>();
        for(BlazeExpression expr : flatten(_expressions))
            list.add(expr.evaluate(context));
        return _literal = new ParallelLiteral(list);
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.PARALLEL_EXPRESSION;
    }
    
    public String toString() {
        String s = "|";
        for(int i = 0; i < _expressions.size(); i++) {
            s += _expressions.get(i).toString();
            if(i+1 < _expressions.size())
                s += ", ";
        }
        return s + "|";
    }
}