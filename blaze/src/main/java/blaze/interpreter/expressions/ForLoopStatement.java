package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.ListLiteral;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.runtime.Console;

/**
 * A for loop, of the form `for x in y { z }`
 */
public class ForLoopStatement extends BlazeExpression {
    private Identifier _variable;
    private BlazeExpression _iterable, _body;
    
    public ForLoopStatement(Identifier variable, BlazeExpression iterable, BlazeExpression body) {
        _variable = variable;
        _iterable = iterable;
        _body = body;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        // coerce the iterable into a list
        ListLiteral list = (ListLiteral)_iterable.evaluate(context).coerceTo(SyntaxTreeTypes.LIST);
        
        // iterate through each value
        for(BlazeExpression value : list.getValue()) {
            Environment child = context.extend();
            // bind the iteration variable
            child.bind(_variable, value);
            // execute the loop body
            _body.evaluate(child);
        }
        
        // loops return nothing
        return new NullLiteral();
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.FOR_LOOP;
    }
    
    public String toString() {
        return String.format("for %s in %s\r\n\t%s", _variable.toString(), _iterable.toString(), _body.toString());
    }
    
    public String toIndentedString() {
        String s = Console.indent("for " + _variable + " in " + _iterable + "\r\n");
        Console.indent();
        s += _body.toIndentedString();
        Console.dedent();
        return s;
    }
}