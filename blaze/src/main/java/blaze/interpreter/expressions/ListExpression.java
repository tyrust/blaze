package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.expressions.literals.ListLiteral;

import java.util.List;
import java.util.ArrayList;

/**
 * A list expression. Evaluates all of the list members and produces a literal list expression.
 */
public class ListExpression extends BlazeExpression {
    private List<BlazeExpression> _expressions;
    
    public ListExpression(List<BlazeExpression> expressions) {
        _expressions = expressions;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        List<BlazeExpression> list = new ArrayList<BlazeExpression>();
        for(BlazeExpression expr : _expressions)
            list.add(expr.evaluate(context));
        return new ListLiteral(list);
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.LIST;
    }
    
    public String toString() {
        String s = "[";
        for(int i = 0; i < _expressions.size(); i++) {
            s += _expressions.get(i).toString();
            if(i+1 < _expressions.size())
                s += ", ";
        }
        return s + "]";
    }
}