package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.LiteralExpression;
import blaze.interpreter.expressions.literals.ListLiteral;
import blaze.interpreter.expressions.literals.ParallelLiteral;
import blaze.interpreter.expressions.literals.ChannelLiteral;
import blaze.interpreter.expressions.literals.ClosureLiteral;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.concurrent.ConcurrencyEngine;

import java.util.List;
import java.util.ArrayList;

/**
 * A parallelize expression. Parallelizes a serial iterable or channel.
 */
public class ParallelizeExpression extends BlazeExpression {
    private BlazeExpression _parallelizeMe, _lastValue;
    private ParallelLiteral _parallelLiteral;
    
    public ParallelizeExpression(BlazeExpression parallelizeMe) {
        _parallelizeMe = parallelizeMe;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(final Environment context) throws RuntimeError, Signal {
        // Workaround to get parallels to be created in an isolated environment and cache correctly:
        // we create the parallel in a closure and return the closure.
        // This is a special kind of closure that houses native code directly via an anonymous
        // interface. The parallelize call gets wrapped in a function call automatically
        // by the desugarer.
        ClosureLiteral closure = new ClosureLiteral(new ClosureLiteral.Callable() {
            public BlazeExpression call(List<BlazeExpression> args) throws RuntimeError {
                try {
                    final BlazeExpression evaluated = _parallelizeMe.evaluate(context);
                    if(_lastValue == null || !evaluated.equals(_lastValue)) {
                        _lastValue = evaluated;
                        _parallelLiteral = null;
                    }
                    if(_parallelLiteral != null)
                        return _parallelLiteral;
                    
                    // Parallellizing a channel simply returns a parallelized copy of it.
                    if(evaluated instanceof ChannelLiteral) {
                        ChannelLiteral parallelChannel = (ChannelLiteral)((ChannelLiteral)evaluated).copy();
                        parallelChannel.parallelize();
                        return parallelChannel;
                    }
                    
                    // Parallelizing a closure wraps it in a native closure that sends it to
                    // the ConcurrencyEngine to be run on a worker.
                    if(evaluated instanceof ClosureLiteral) {
                        return new ClosureLiteral(new ClosureLiteral.Callable() {
                            public BlazeExpression call(List<BlazeExpression> closureArgs) throws RuntimeError {
                                ConcurrencyEngine.invokeInFuture((ClosureLiteral)evaluated, closureArgs);
                                return new NullLiteral();
                            }
                        });
                    }
                    
                    ListLiteral list = (ListLiteral)evaluated.coerceTo(SyntaxTreeTypes.LIST);
                    return _parallelLiteral = new ParallelLiteral(new ArrayList<BlazeExpression>(list.getValue()));
                } catch(Signal sig) {
                    // ignore signal, it's meaningless in here
                    return new NullLiteral();
                }
            }
        });
        return closure;
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.PARALLELIZE;
    }
    
    public String toString() {
        return "/" + _parallelizeMe + "/";
    }
}