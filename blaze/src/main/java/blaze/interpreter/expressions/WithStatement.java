package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.expressions.literals.LiteralExpression;
import blaze.runtime.Console;

import java.util.List;

/**
 * A with statement. Locks whatever _toLock evaluates to and runs body.
 */
public class WithStatement extends BlazeExpression {
    private BlazeExpression _toLock, _body;

    public WithStatement(BlazeExpression toLock, BlazeExpression body) {
        _toLock = toLock;
        _body = body;
        setIsStatement(true);
    }
    
    public BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        LiteralExpression literal = (LiteralExpression)_toLock.evaluate(context);
        synchronized(literal) {
            return _body.evaluate(context);
        }
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.WITH;
    }
    
    public String toString() {
        return String.format("with %s %s", _toLock.toString(), _body.toString());
    }
    
    public String toIndentedString() {
        String s = Console.indent("with " + _toLock + "\r\n");
        Console.indent();
        s += _body.toIndentedString();
        Console.dedent();
        return s;
    }
}