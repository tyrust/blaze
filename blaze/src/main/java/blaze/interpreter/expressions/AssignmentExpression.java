package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.expressions.literals.ClosureLiteral;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.environment.Environment;

/**
 * A variable assignment expression. Binds an expression to a variable in a given environment.
 */
public class AssignmentExpression extends BlazeExpression {
    private Identifier _identifier;
    private BlazeExpression _variable;
    private BlazeExpression _expression;
    private BinaryOperator _compoundOperator;
    
    public AssignmentExpression(BlazeExpression variable, BlazeExpression expression, BinaryOperator compoundOperator) {
        _variable = variable;
        _expression = expression;
        _compoundOperator = compoundOperator;
    }
    
    public AssignmentExpression(Identifier id, BlazeExpression expression) {
        _identifier = id;
        _expression = expression;
    }
    
    public AssignmentExpression(BlazeExpression variable, BlazeExpression expression) {
        this(variable, expression, null);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        BlazeExpression value = _expression.evaluate(context);
        
        // Note: If _identifier is present, this means the variable is being locally defined
        // for the first time, and so it is not necessary to consider _compoundOperator.
        if(_identifier != null) {
            context.bind(_identifier, value);
            // If the RHS is a function, set its name
            if(value.getNodeType().equals(SyntaxTreeTypes.CLOSURE))
                ((ClosureLiteral)value).setIdentifier(_identifier);
            
            // For C-style assignment chaining (i.e. x = y = 5)
            return value;
        }
        
        if(_compoundOperator != null) {
            BlazeExpression varVal = _variable.evaluate(context);
            value = varVal.doBinaryOperation(context, _compoundOperator, value);
        }
        
        // This covers cases where the LHS is a variable or a field
        if(_variable.getNodeType().equals(SyntaxTreeTypes.VARIABLE)) {
            VariableExpression var = (VariableExpression)_variable;
            
            // If the RHS is a function, set its name
            if(value.getNodeType().equals(SyntaxTreeTypes.CLOSURE))
                ((ClosureLiteral)value).setIdentifier(var.getIdentifier());
            
            // If the variable has a parent, do a field assignment
            if(var.getParent() != null)
                return new PutExpression(var.getParent(), var.getIdentifier(), value).evaluateInternal(context);
            else {
                context.bind(var.getIdentifier(), value);
                return value;
            }
        }
        
        // In this case, the LHS is a index get (i.e. of the form x[y] = z)
        GetExpression get = (GetExpression)_variable;
        return get.convertToPutExpression(value).evaluateInternal(context);
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.ASSIGNMENT;
    }
    
    public String toString() {
        String lhs = "";
        if(_identifier != null)
            lhs += _identifier;
        else
            lhs += _variable;
        lhs += " ";
        if(_compoundOperator != null)
            lhs += _compoundOperator;
        lhs += "=";
        return String.format("%s %s", lhs, _expression.toString());
    }
}