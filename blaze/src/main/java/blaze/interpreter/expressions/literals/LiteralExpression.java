package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BlazeObject;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

/**
 * Represents an abstract literal value, such as null or "hello" or 3.14
 */
public abstract class LiteralExpression extends BlazeExpression implements BlazeObject {
    protected Map<Identifier, BlazeExpression> _instanceVariables;
    protected boolean _frozen = false;
    
    // Allows for OOP-esque interactions, like [1,2,3].length()
    // These are largely registered at runtime(!) using a partial bootstrapping kind of idea
    // (i.e. native calls that allow Blaze libraries to interact with the interpreter)
    
    protected Map<Identifier, BlazeExpression> getStaticVariablesReference() throws RuntimeError {
        throw new RuntimeError("cannot access static variables of " + this);
    }
    
    protected Map<Identifier, BlazeExpression> getInstanceVariables() throws RuntimeError {
        if(_instanceVariables == null)
            throw new RuntimeError("cannot access instance variables of " + this);
        return _instanceVariables;
    }
    
    /**
     * Indicates whether or not the value of this object is fixed.
     * @return Whether or not the object is frozen (immutable).
     */
    public boolean isFrozen() {
        return _frozen;
    }
    
    /**
     * Freezes/melts a literal object.
     * If frozen, the object's value is guaranteed not to change at runtime.
     * (Well, technically you can cast it into a native object and manually hack its _frozen
     * flag to make it mutable again, but this totally breaks contracts/invariants across the
     * interpreter and the results of doing this are undefined).
     * Attempting to modify the value of a frozen object at runtime triggers an error.
     * @param   frozen      Boolean indicating whether or not to freeze the object.
     */
    public void setFrozen(boolean frozen) {
        _frozen = frozen;
    }
    
    public abstract LiteralExpression copy();
    
    public LiteralExpression meltedCopy() {
        LiteralExpression newEx = copy();
        newEx.setFrozen(false);
        return newEx;
    }
    
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        if(newType.equals(getNodeType()))
            return copy();
        if(newType.equals(SyntaxTreeTypes.BLAZE_LITERAL))
            return wrapNativeObject(this);
        return super.coerceTo(newType);
    }
    
    public BlazeExpression getField(Identifier id) throws RuntimeError {
        BlazeExpression var = getInstanceVariables().get(id);
        if(var == null)
            var = getStaticVariablesReference().get(id);
        if(var == null)
            throw new RuntimeError("field " + id + " not found in " + this);
        return var;
    }
    
    public void setField(Identifier id, BlazeExpression value) throws RuntimeError {
        if(_frozen)
            throw new RuntimeError(this + " is frozen (immutable) -- you cannot assign to its field '" + id + "'");
        getInstanceVariables().put(id, value);
    }
    
    public void deleteField(Identifier id) throws RuntimeError {
        if(_frozen)
            throw new RuntimeError(this + " is frozen (immutable) -- you cannot delete its field '" + id + "'");
        getInstanceVariables().remove(id);
    }
    
    public LiteralExpression() {}
    
    /**
     * Simply returns a reference to the literal expression, as these cannot be evaluated further.
     */
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        return this;
    }

    public static LiteralExpression wrapNativeObject(Object object) {
        if(object == null)
            return new NullLiteral();
        if(object instanceof String)
            return new StringLiteral(object.toString());
        if(object instanceof Integer || object instanceof Long)
            return new IntegerLiteral(object.toString());
        if(object instanceof Double || object instanceof Float)
            return new FloatLiteral(object.toString());
        if(object instanceof List)
            object = ((List)object).toArray();
        if(object instanceof Map) {
            Map<BlazeExpression, BlazeExpression> map = new HashMap<BlazeExpression, BlazeExpression>();
            for(Object key : ((Map)object).keySet())
                map.put(wrapNativeObject(key), wrapNativeObject(((Map)object).get(key)));
            return new DictLiteral(map);
        }
        if(object instanceof Object[]) {
            List<BlazeExpression> list = new ArrayList<BlazeExpression>();
            for(Object obj : (Object[])object)
                list.add(wrapNativeObject(obj));
            return new ListLiteral(list);
        }
        
        // If all else fails, just wrap it in a native literal
        return new NativeLiteral(object);
    }

}