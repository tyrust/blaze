package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

/**
 * Represents a literal dictionary value, such as ["a":4, 3:2.6].
 */
public class DictLiteral extends LiteralExpression implements KeyValueMap {
    private static Map<Identifier, BlazeExpression> _staticVariables = new HashMap<Identifier, BlazeExpression>();
    private Map<BlazeExpression, BlazeExpression> _map;
    
    public static Map<Identifier, BlazeExpression> getStaticVariables() throws RuntimeError {
        return _staticVariables;
    }
    
    protected Map<Identifier, BlazeExpression> getStaticVariablesReference() throws RuntimeError {
        return _staticVariables;
    }
    
    public DictLiteral(Map<BlazeExpression, BlazeExpression> map) {
        _map = map;
        _instanceVariables = new HashMap<Identifier, BlazeExpression>();
        
        // Dicts are inherently mutable.
        // (They'd be pretty useless if not...)
        _frozen = false;
    }
    
    public LiteralExpression copy() {
        DictLiteral copy = new DictLiteral(new HashMap<BlazeExpression, BlazeExpression>(_map));
        copy._instanceVariables = new HashMap<Identifier, BlazeExpression>(_instanceVariables);
        return copy;
    }
    
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        // If the operator is an AND or OR, we coerce to booleans
        if(operator == BinaryOperator.AND || operator == BinaryOperator.OR)
            return coerceTo(SyntaxTreeTypes.BOOLEAN).doBinaryOperation(context, operator, operand);
        
        // We immediately evaluate the operand here
        operand = operand.evaluate(context);
                
        // If the RHS is a string, we coerce to strings
        if(operand.getNodeType().equals(SyntaxTreeTypes.STRING))
            return coerceTo(SyntaxTreeTypes.STRING).doBinaryOperation(context, operator, operand);
        
        if(!operand.getNodeType().equals(getNodeType())) {
            switch(operator) {
                case EQ:    return new BooleanLiteral(false);
                case NEQ:   return new BooleanLiteral(true);
            }
        }
        
        // We only support comparisons with other dicts
        Map<BlazeExpression, BlazeExpression> rhs = ((DictLiteral)coerce(operand)).getValue();
        switch(operator) {
            case EQ:        return new BooleanLiteral(_map.equals(rhs));
            case NEQ:       return new BooleanLiteral(!_map.equals(rhs));
            
            // TODO: LT,GT,LTE,GTE using containment or something?
        }
        
        return super.doBinaryOperation(context, operator, operand);
    }
    
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        switch(operator) {
            case NOT:
                return coerceTo(SyntaxTreeTypes.BOOLEAN).doUnaryOperation(context, operator);
        }
        
        return super.doUnaryOperation(context, operator);
    }
    
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        if(newType.equals(SyntaxTreeTypes.NATIVE_OBJECT))
            return new NativeLiteral(_map);
        
        if(newType.equals(SyntaxTreeTypes.LIST))
            return new ListLiteral(new ArrayList<BlazeExpression>(_map.keySet()));
        
        if(newType.equals(SyntaxTreeTypes.BOOLEAN))
            return new BooleanLiteral(_map.size() > 0);
        
        if(newType.equals(SyntaxTreeTypes.STRING))
            return new StringLiteral(toString());
        
        return super.coerceTo(newType);
    }
    
    public Object coerceToNativeType() throws RuntimeError {
        Map<Object, Object> nativeMap = new HashMap<Object, Object>();
        for(BlazeExpression key : _map.keySet())
            nativeMap.put(key.coerceToNativeType(), _map.get(key).coerceToNativeType());
        return nativeMap;
    }
    
    public Map<BlazeExpression, BlazeExpression> getValue() {
        return _map;
    }
    
    public int hashCode() {
        return _map.hashCode();
    }
    
    public boolean equals(Object other) {
        return other instanceof DictLiteral && ((DictLiteral)other)._map.equals(_map);
    }
    
    public String toString() {
        String s = "[";
        List<BlazeExpression> keys = new ArrayList<BlazeExpression>(_map.keySet());
        for(int i = 0; i < _map.size(); i++) {
            BlazeExpression key = keys.get(i);
            s += key + ": " + _map.get(key);
            if(i+1 < _map.size())
                s += ", ";
        }
        return s + "]";
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.DICT;
    }
    
    public BlazeExpression get(BlazeExpression key) throws RuntimeError {
        BlazeExpression value = _map.get(key);
        if(value == null)
            return new NullLiteral();
        return value;
    }
    
    public void put(BlazeExpression key, BlazeExpression value) throws RuntimeError {
        if(_frozen)
            throw new RuntimeError("dictionary is frozen (immutable) -- it cannot be assigned to");
        if(!(key instanceof LiteralExpression) || !((LiteralExpression)key).isFrozen())
            throw new RuntimeError("dictionary key must be a frozen literal -- cannot assign to mutable/invalid key " + key);
        _map.put(key, value);
    }
    
    public void delete(BlazeExpression key) throws RuntimeError {
        if(_frozen)
            throw new RuntimeError("dictionary is frozen (immutable) -- it cannot be deleted from");
        _map.remove(key);
    }
}