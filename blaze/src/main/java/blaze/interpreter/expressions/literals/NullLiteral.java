package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;

/**
 * Represents a null value.
 */
public class NullLiteral extends LiteralExpression {
    public NullLiteral() {
        // Obviously, null values cannot change in any way.
        _frozen = true;
    }
    
    public LiteralExpression copy() {
        // all null values are the same
        return this;
    }
    
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        // The only binary operations a null value supports are string concatenation and logical operations
        if(operand.getNodeType().equals(SyntaxTreeTypes.STRING) && operator == BinaryOperator.PLUS)
            return coerceTo(SyntaxTreeTypes.STRING).doBinaryOperation(context, operator, operand);
        
        operand = operand.evaluate(context);
        
        switch(operator) {
            case EQ:    return new BooleanLiteral(operand.getNodeType().equals(SyntaxTreeTypes.NULL));
            case NEQ:   return new BooleanLiteral(!operand.getNodeType().equals(SyntaxTreeTypes.NULL));
            case AND:
            case OR:
                return coerceTo(SyntaxTreeTypes.BOOLEAN).doBinaryOperation(context, operator, operand);
        }
        
        return super.doBinaryOperation(context, operator, operand);
    }
    
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        // The only unary operator supported is NOT
        if(operator == UnaryOperator.NOT)
            return coerceTo(SyntaxTreeTypes.BOOLEAN).doUnaryOperation(context, operator);
        
        return super.doUnaryOperation(context, operator);
    }
    
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        if(newType.equals(SyntaxTreeTypes.NATIVE_OBJECT))
            return new NativeLiteral(null);
        
        if(newType.equals(SyntaxTreeTypes.STRING))
            return new StringLiteral("null");
        
        if(newType.equals(SyntaxTreeTypes.BOOLEAN))
            return new BooleanLiteral(false);
        
        return super.coerceTo(newType);
    }
    
    public Object coerceToNativeType() throws RuntimeError {
        return null;
    }
    
    public boolean equals(Object other) {
        return other instanceof NullLiteral;
    }
    
    public String toString() {
        return "null";
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.NULL;
    }
}