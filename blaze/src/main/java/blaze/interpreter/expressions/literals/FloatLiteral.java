package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.util.Map;
import java.util.HashMap;

/**
 * Represents a literal arbitrary-precision floating point value, such as 5.5.
 */
public class FloatLiteral extends LiteralExpression {
    private static Map<Identifier, BlazeExpression> _staticVariables = new HashMap<Identifier, BlazeExpression>();
    private BigDecimal _value;
    
    public static Map<Identifier, BlazeExpression> getStaticVariables() throws RuntimeError {
        return _staticVariables;
    }
    
    protected Map<Identifier, BlazeExpression> getStaticVariablesReference() throws RuntimeError {
        return _staticVariables;
    }
    
    public FloatLiteral(BigDecimal value) {
        _value = value;
        _instanceVariables = new HashMap<Identifier, BlazeExpression>();
        // floats are frozen by default
        _frozen = true;
    }
    
    public FloatLiteral(String value) {
        this(new BigDecimal(value));
    }
    
    public LiteralExpression copy() {
        FloatLiteral copy = new FloatLiteral(_value);
        copy._instanceVariables = new HashMap<Identifier, BlazeExpression>(_instanceVariables);
        return copy;
    }
    
    /**
     * Basically, does floating point arithmetic, plus string concatenation, etc.
     */
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        // We immediately evaluate the operand here
        operand = operand.evaluate(context);
        
        // If the operator is an AND or OR, we coerce to booleans
        if(operator == BinaryOperator.AND || operator == BinaryOperator.OR)
            return coerceTo(SyntaxTreeTypes.BOOLEAN).doBinaryOperation(context, operator, operand);
        
        // If the RHS is a list, swap roles
        if(operand.getNodeType().equals(SyntaxTreeTypes.LIST) && operator == BinaryOperator.TIMES)
            return operand.doBinaryOperation(context, operator, this);
        
        if(!operand.getNodeType().equals(getNodeType())) {
            switch(operator) {
                case EQ:    return new BooleanLiteral(false);
                case NEQ:   return new BooleanLiteral(true);
            }
        }
        
        // If the operand is a string, we coerce to strings
        // (unless it's multiplication, which has a special meaning)
        if(operand.getNodeType().equals(SyntaxTreeTypes.STRING)) {
            switch(operator) {
                case TIMES:
                    // swap roles, let the string handle this
                    return operand.doBinaryOperation(context, operator, this);
                default:
                    return coerceTo(SyntaxTreeTypes.STRING).doBinaryOperation(context, operator, operand);
            }
        }
        
        BigDecimal rhs = ((FloatLiteral)coerce(operand)).getValue();
        switch(operator) {
            case PLUS:      return new FloatLiteral(_value.add(rhs));
            case MINUS:     return new FloatLiteral(_value.subtract(rhs));
            case TIMES:     return new FloatLiteral(_value.multiply(rhs));
            case DIVIDE:
                if(rhs.equals(BigDecimal.ZERO))
                    throw new RuntimeError("Division by zero.");
                return new FloatLiteral(_value.divide(rhs));
            case MOD:
                if(rhs.equals(BigDecimal.ZERO))
                    throw new RuntimeError("Modulo by zero.");
                BigDecimal mod = _value;
                while(mod.compareTo(BigDecimal.ZERO) == -1)
                    mod.add(rhs);
                while(mod.compareTo(rhs) == 1)
                    mod.subtract(rhs);
                return new FloatLiteral(mod);
            
            case EQ:        return new BooleanLiteral(_value.compareTo(rhs) == 0);
            case NEQ:       return new BooleanLiteral(_value.compareTo(rhs) != 0);
            case LT:        return new BooleanLiteral(_value.compareTo(rhs) == -1);
            case LTE:       return new BooleanLiteral(_value.compareTo(rhs) <= 0);
            case GT:        return new BooleanLiteral(_value.compareTo(rhs) == 1);
            case GTE:       return new BooleanLiteral(_value.compareTo(rhs) >= 0);
        }
        
        return super.doBinaryOperation(context, operator, operand);
    }
    
    /**
     * Applies the unary operator to this float value, such as - to 4.4 -> -4.4
     */
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        switch(operator) {
            case PLUS:      return new FloatLiteral(_value);
            case MINUS:     return new FloatLiteral(_value.negate());
            case NOT:       return coerceTo(SyntaxTreeTypes.BOOLEAN).doUnaryOperation(context, operator);
        }
        
        return super.doUnaryOperation(context, operator);
    }
    
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        if(newType.equals(SyntaxTreeTypes.NATIVE_OBJECT))
            return new NativeLiteral(_value);
        
        if(newType.equals(SyntaxTreeTypes.INTEGER))
            return new IntegerLiteral(_value.toBigInteger());
        
        if(newType.equals(SyntaxTreeTypes.BOOLEAN))
            return new BooleanLiteral(!_value.equals(BigDecimal.ZERO));
        
        if(newType.equals(SyntaxTreeTypes.STRING))
            return new StringLiteral(_value.toString());
        
        return super.coerceTo(newType);
    }
    
    public Object coerceToNativeType() throws RuntimeError {
        return new Double(_value.doubleValue());
    }
    
    /**
     * Returns the raw float value of this float literal.
     */
    public BigDecimal getValue() {
        return _value;
    }
    
    public int hashCode() {
        return _value.hashCode();
    }
    
    public boolean equals(Object other) {
        return other instanceof FloatLiteral && ((FloatLiteral)other)._value.equals(_value);
    }
    
    public String toString() {
        return _value.toString();
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.FLOAT;
    }
}