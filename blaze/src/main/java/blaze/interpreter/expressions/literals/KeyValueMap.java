package blaze.interpreter.expressions.literals;

import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.RuntimeError;

/**
 * An abstract key-value mapping literal, such as a list or a map.
 * (Essentially, anything that supports x[y]=z syntax)
 */
public interface KeyValueMap {
    /**
     * @param   key     The key to look up in this key-value map.
     * @return          The value for this key; a null literal expression if it isn't found.
     */
    public BlazeExpression get(BlazeExpression key) throws RuntimeError;
    
    /**
     * @param   key     The key for the given value.
     * @param   value   The value to map this key to.
     */
    public void put(BlazeExpression key, BlazeExpression value) throws RuntimeError;
    
    /**
     * @param   key     The key to remove.
     */
    public void delete(BlazeExpression key) throws RuntimeError;
}