package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.signals.BranchSignal;
import blaze.interpreter.signals.ReturnSignal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;
import blaze.concurrent.ConcurrencyEngine;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

/**
 * Represents a parallel literal value (i.e. a "superposition" of values).
 */
public class ParallelLiteral extends LiteralExpression {
    private List<BlazeExpression> _expressions;
    
    public ParallelLiteral(List<BlazeExpression> expressions) {
        _expressions = expressions;
        _frozen = true;
    }
    
    /**
     * See ParallelExpression.flatten for an explanation of why this exists.
     */
    private static List<BlazeExpression> flatten(List<BlazeExpression> expressions) {
        List<BlazeExpression> flattenedList = new ArrayList<BlazeExpression>();
        for(BlazeExpression expression : expressions) {
            if(expression.getNodeType().equals(SyntaxTreeTypes.PARALLEL))
                flattenedList.addAll(flatten(((ParallelLiteral)expression)._expressions));
            else
                flattenedList.add(expression);
        }
        return flattenedList;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        List<Environment> environments = new ArrayList<Environment>();
        for(BlazeExpression possibleValue : flatten(_expressions)) {
            Environment env = context.parallelExtend();
            env.cacheReturnValue(this, possibleValue.evaluate(context));
            environments.add(env);
        }
        throw new BranchSignal(environments);
    }
    
    public LiteralExpression copy() {
        return new ParallelLiteral(new ArrayList<BlazeExpression>(_expressions));
    }
    
    public String toString() {
        List<BlazeExpression> expressions = flatten(_expressions);
        String s = "|";
        for(int i = 0; i < expressions.size(); i++) {
            s += expressions.get(i).toString();
            if(i+1 < expressions.size())
                s += ", ";
        }
        return s + "|";
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.PARALLEL;
    }
}