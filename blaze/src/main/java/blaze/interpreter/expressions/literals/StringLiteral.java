package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;

/**
 * Represents a literal string value, such as "hello".
 */
public class StringLiteral extends LiteralExpression implements KeyValueMap {
    private static Map<Identifier, BlazeExpression> _staticVariables = new HashMap<Identifier, BlazeExpression>();
    private String _value;
    
    public static Map<Identifier, BlazeExpression> getStaticVariables() throws RuntimeError {
        return _staticVariables;
    }
    
    protected Map<Identifier, BlazeExpression> getStaticVariablesReference() throws RuntimeError {
        return _staticVariables;
    }
    
    public StringLiteral(String value) {
        _value = value;
        _instanceVariables = new HashMap<Identifier, BlazeExpression>();
        
        // Strings are immutable by default
        _frozen = true;
    }
    
    public LiteralExpression copy() {
        StringLiteral copy = new StringLiteral(_value);
        copy._instanceVariables = new HashMap<Identifier, BlazeExpression>(_instanceVariables);
        return copy;
    }
    
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        // If the operator is an AND or OR, we coerce to booleans
        if(operator == BinaryOperator.AND || operator == BinaryOperator.OR)
            return coerceTo(SyntaxTreeTypes.BOOLEAN).doBinaryOperation(context, operator, operand);
        
        // We immediately evaluate the operand here
        operand = operand.evaluate(context);
        
        // If the operator is TIMES and the RHS is a number, we duplicate the string
        // This enables the Pythonic syntax "abc"*5 -> "abcabcabcabcabc"
        // We even support floating point numbers, so that "asdf"*2.5 -> "asdfasdfas"
        // Similarly, "helloworld"/2 -> "hello"
        if(operator == BinaryOperator.TIMES || operator == BinaryOperator.DIVIDE) {
            double value = ((FloatLiteral)operand.coerceTo(SyntaxTreeTypes.FLOAT)).getValue().doubleValue();
            if(operator == BinaryOperator.DIVIDE)
                value = 1./value;
            
            int repeat = (int)value;
            double frac = value - repeat;
            String s = "";
            for(int i = 0; i < repeat; i++)
                s += _value;
            int index = (int)(_value.length() * frac);
            s += _value.substring(0, index);
            return new StringLiteral(s);
        }
        
        if(!operand.getNodeType().equals(getNodeType())) {
            switch(operator) {
                case EQ:    return new BooleanLiteral(false);
                case NEQ:   return new BooleanLiteral(true);
            }
        }
        
        // Otherwise we coerce to strings, as string operations take precedence over all others
        String rhs = ((StringLiteral)coerce(operand)).getValue();
        switch(operator) {
            // String concatenation
            case PLUS:      return new StringLiteral(_value + rhs);
            
            // In Blaze, subtracting a string R from a string S removes all occurrences of R from S
            case MINUS:     return new StringLiteral(_value.replace(rhs, ""));
            
            case EQ:        return new BooleanLiteral(_value.equals(rhs));
            case NEQ:       return new BooleanLiteral(!_value.equals(rhs));
            
            // TODO: one day support LT,GT,LTE,GTE using lexicographical ordering?
        }
        
        return super.doBinaryOperation(context, operator, operand);
    }
    
    /**
     * Applies the unary operator to this string
     */
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        switch(operator) {
            // +"asdf" -> "asdf"; this simply has no effect
            case PLUS:      return new StringLiteral(_value);
            // -"asdf" -> "fdsa"; this is a string reverse operation
            case MINUS:     return new StringLiteral(new StringBuffer(_value).reverse().toString());
            case NOT:       return coerceTo(SyntaxTreeTypes.BOOLEAN).doUnaryOperation(context, operator);
        }
        
        return super.doUnaryOperation(context, operator);
    }
    
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        if(newType.equals(getNodeType()))
            return new StringLiteral(_value);
        
        if(newType.equals(SyntaxTreeTypes.NATIVE_OBJECT))
            return new NativeLiteral(_value);
        
        if(newType.equals(SyntaxTreeTypes.BOOLEAN))
            return new BooleanLiteral(_value.length() > 0);
        
        if(newType.equals(SyntaxTreeTypes.FLOAT)) {
            try {
                return new FloatLiteral(_value);
            } catch(NumberFormatException ex) {
                throw new RuntimeError(String.format("string '%s' cannot be coerced into float", _value));
            }
        }
        
        if(newType.equals(SyntaxTreeTypes.INTEGER)) {
            try {
                return new IntegerLiteral(_value);
            } catch(NumberFormatException ex) {
                throw new RuntimeError(String.format("string '%s' cannot be coerced into integer", _value));
            }
        }
        
        if(newType.equals(SyntaxTreeTypes.LIST)) {
            List<BlazeExpression> list = new LinkedList<BlazeExpression>();
            for(int i = 0; i < _value.length(); i++)
                list.add(new StringLiteral(_value.charAt(i) + ""));
            return new ListLiteral(list);
        }
        
        return super.coerceTo(newType);
    }
    
    public Object coerceToNativeType() throws RuntimeError {
        return _value;
    }
    
    /**
     * Returns the raw string value of this string literal.
     */
    public String getValue() {
        return _value;
    }
    
    public boolean equals(Object other) {
        return other instanceof StringLiteral && ((StringLiteral)other)._value.equals(_value);
    }
    
    public int hashCode() {
        return _value.hashCode();
    }
    
    public String toString() {
        return "\"" + _value + "\"";
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.STRING;
    }
    
    private int getIndexFromKey(BlazeExpression key) throws RuntimeError {
        int index = ((IntegerLiteral)key).getValue().intValue();
        while(index < 0 && _value.length() > 0)
            index += _value.length();
        if(index < 0 || index >= _value.length())
            throw new RuntimeError("index " + index + " out of bounds of string");
        return index;
    }
    
    public BlazeExpression get(BlazeExpression key) throws RuntimeError {
        if(key.getNodeType().equals(SyntaxTreeTypes.INTEGER)) {
            int index = getIndexFromKey(key);
            return new StringLiteral(_value.charAt(index) + "");
        }
        throw new RuntimeError("cannot index string using " + key);
    }
    public void put(BlazeExpression key, BlazeExpression value) throws RuntimeError {
        if(_frozen)
            throw new RuntimeError("string is frozen (immutable) -- it cannot be assigned to");
        if(key.getNodeType().equals(SyntaxTreeTypes.INTEGER)) {
            int index = getIndexFromKey(key);
            String s = ((StringLiteral)coerce(value)).getValue();
            _value = _value.substring(0, index) + s + _value.substring(index+1);
        } else if(key.getNodeType().equals(SyntaxTreeTypes.STRING)) {
            String find = ((StringLiteral)key).getValue();
            String replace = ((StringLiteral)coerce(value)).getValue();
            _value = _value.replace(find, replace);
        } else
            throw new RuntimeError("cannot index string using " + key);
    }
    
    public void delete(BlazeExpression key) throws RuntimeError {
        if(_frozen)
            throw new RuntimeError("string is frozen (immutable) -- it cannot be modified");
        if(key.getNodeType().equals(SyntaxTreeTypes.INTEGER)) {
            int index = getIndexFromKey(key);
            _value = _value.substring(0, index) + _value.substring(index+1);
        } else if(key.getNodeType().equals(SyntaxTreeTypes.STRING)) {
            String remove = ((StringLiteral)key).getValue();
            _value = _value.replace(remove, "");
        } else
            throw new RuntimeError("cannot index string using " + key);
    }
}