package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

import java.util.Map;
import java.util.HashMap;

/**
 * Represents a literal boolean value.
 */
public class BooleanLiteral extends LiteralExpression {
    private static Map<Identifier, BlazeExpression> _staticVariables = new HashMap<Identifier, BlazeExpression>();
    private boolean _value;
    
    public static Map<Identifier, BlazeExpression> getStaticVariables() throws RuntimeError {
        return _staticVariables;
    }
    
    protected Map<Identifier, BlazeExpression> getStaticVariablesReference() throws RuntimeError {
        return _staticVariables;
    }
    
    public BooleanLiteral(boolean value) {
        _value = value;
        _instanceVariables = new HashMap<Identifier, BlazeExpression>();
        
        // boolean values are immutable by default
        _frozen = true;
    }
    
    public LiteralExpression copy() {
        BooleanLiteral copy = new BooleanLiteral(_value);
        copy._instanceVariables = new HashMap<Identifier, BlazeExpression>(_instanceVariables);
        return copy;
    }
    
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        if(operator == BinaryOperator.AND)
            if(!_value)
                // Short-circuit AND; do not evaluate the operand
                return new BooleanLiteral(false);
        
        if(operator == BinaryOperator.OR)
            if(_value)
                // Short-circuit OR; do not evaluate the operand
                return new BooleanLiteral(true);
        
        // Otherwise, we must evaluate the operand
        operand = operand.evaluate(context);
        
        if(!operand.getNodeType().equals(getNodeType())) {
            switch(operator) {
                case EQ:    return new BooleanLiteral(false);
                case NEQ:   return new BooleanLiteral(true);
            }
        }
        
        // If the operand is a string, we coerce to strings
        // (unless it's multiplication, which has a special meaning)
        if(operand.getNodeType().equals(SyntaxTreeTypes.STRING)) {
            switch(operator) {
                case TIMES:
                    // swap roles, let the string handle this
                    return operand.doBinaryOperation(context, operator, coerceTo(SyntaxTreeTypes.INTEGER));
                case EQ:
                    // numbers and strings are never equal
                    return new BooleanLiteral(false);
                case NEQ:
                    return new BooleanLiteral(true);
                default:
                    return coerceTo(SyntaxTreeTypes.STRING).doBinaryOperation(context, operator, operand);
            }
        }
        
        switch(operator) {
            case AND:   return coerce(operand);
            case OR:    return coerce(operand);
            
            case PLUS:
            case MINUS:
            case TIMES:
            case DIVIDE:
            case MOD:
            case EQ:
            case NEQ:
            case LT:
            case LTE:
            case GT:
            case GTE:
                return coerceTo(SyntaxTreeTypes.INTEGER).doBinaryOperation(context, operator, operand);
        }
        
        return super.doBinaryOperation(context, operator, operand);
    }
    
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        if(operator == UnaryOperator.NOT)
            return new BooleanLiteral(!_value);
        
        return super.doUnaryOperation(context, operator);
    }
    
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        if(newType.equals(SyntaxTreeTypes.NATIVE_OBJECT))
            return new NativeLiteral(_value);
        
        if(newType.equals(SyntaxTreeTypes.INTEGER)) {
            if(_value)
                return new IntegerLiteral("1");
            else
                return new IntegerLiteral("0");
        }
        
        if(newType.equals(SyntaxTreeTypes.FLOAT))
            return coerceTo(SyntaxTreeTypes.INTEGER).coerceTo(SyntaxTreeTypes.FLOAT);
        
        if(newType.equals(SyntaxTreeTypes.STRING))
            return new StringLiteral(_value + "");
        
        return super.coerceTo(newType);
    }
    
    public Object coerceToNativeType() throws RuntimeError {
        return new Boolean(_value);
    }
    
    public int hashCode() {
        return new Boolean(_value).hashCode();
    }
    
    /**
     * Returns the raw boolean value of this boolean literal.
     */
    public boolean getValue() {
        return _value;
    }
    
    public boolean equals(Object other) {
        return other instanceof BooleanLiteral && ((BooleanLiteral)other)._value == _value;
    }
    
    public String toString() {
        return _value + "";
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.BOOLEAN;
    }
}