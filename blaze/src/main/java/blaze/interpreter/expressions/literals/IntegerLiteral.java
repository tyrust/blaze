package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

import java.math.BigInteger;
import java.math.BigDecimal;

import java.util.Map;
import java.util.HashMap;

/**
 * Represents an arbitrary-precision literal integer value, such as 5.
 */
public class IntegerLiteral extends LiteralExpression {
    private static Map<Identifier, BlazeExpression> _staticVariables = new HashMap<Identifier, BlazeExpression>();
    private BigInteger _value;
    
    public static Map<Identifier, BlazeExpression> getStaticVariables() throws RuntimeError {
        return _staticVariables;
    }
    
    protected Map<Identifier, BlazeExpression> getStaticVariablesReference() throws RuntimeError {
        return _staticVariables;
    }
    
    public IntegerLiteral(BigInteger value) {
        _value = value;
        _instanceVariables = new HashMap<Identifier, BlazeExpression>();
        // integers are frozen by default
        _frozen = true;
    }
    
    public IntegerLiteral(String value) {
        this(new BigInteger(value));
    }
    
    public LiteralExpression copy() {
        IntegerLiteral copy = new IntegerLiteral(_value);
        copy._instanceVariables = new HashMap<Identifier, BlazeExpression>(_instanceVariables);
        return copy;
    }
    
    /**
     * Basically, does integer arithmetic, plus string concatenation, etc.
     */
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        // As floats generalize integers, we could simply inherit these operations from them.
        // However, we have chosen to reimplement them here for integers primarily for efficiency reasons:
        // integer arithmetic can be done in hardware more efficiently than typecasting to floats,
        // doing floating point arithmetic, then casting back to integers.
        
        // If the operator is an AND or OR, we coerce to booleans
        if(operator == BinaryOperator.AND || operator == BinaryOperator.OR)
            return coerceTo(SyntaxTreeTypes.BOOLEAN).doBinaryOperation(context, operator, operand);
        
        // We immediately evaluate the operand here
        operand = operand.evaluate(context);
        
        // If the RHS is a floating point number, we must do floating point arithmetic, however.
        if(operand.getNodeType().equals(SyntaxTreeTypes.FLOAT))
            // Note that the operand is already evaluated here, so this is quite efficient
            // The float implementation of binary operations will attempt to re-evaluate it, which will
            // have no effect as the operand is already in simplest terms.
            return coerceTo(SyntaxTreeTypes.FLOAT).doBinaryOperation(context, operator, operand);
        
        // If the RHS is a list, swap roles
        if(operand.getNodeType().equals(SyntaxTreeTypes.LIST) && operator == BinaryOperator.TIMES)
            return operand.doBinaryOperation(context, operator, this);
        
        if(!operand.getNodeType().equals(getNodeType())) {
            switch(operator) {
                case EQ:    return new BooleanLiteral(false);
                case NEQ:   return new BooleanLiteral(true);
            }
        }
        
        // If the operand is a string, we coerce to strings
        // (unless it's multiplication, which has a special meaning)
        if(operand.getNodeType().equals(SyntaxTreeTypes.STRING)) {
            switch(operator) {
                case TIMES:
                    // swap roles, let the string handle this
                    return operand.doBinaryOperation(context, operator, this);
                default:
                    return coerceTo(SyntaxTreeTypes.STRING).doBinaryOperation(context, operator, operand);
            }
        }
        
        BigInteger rhs = ((IntegerLiteral)coerce(operand)).getValue();
        switch(operator) {
            case PLUS:      return new IntegerLiteral(_value.add(rhs));
            case MINUS:     return new IntegerLiteral(_value.subtract(rhs));
            case TIMES:     return new IntegerLiteral(_value.multiply(rhs));
            case DIVIDE:
                if(rhs.equals(BigInteger.ZERO))
                    throw new RuntimeError("Division by zero.");
                return new IntegerLiteral(_value.divide(rhs));
            case MOD:
                if(rhs.equals(BigInteger.ZERO))
                    throw new RuntimeError("Modulo by zero.");
                return new IntegerLiteral(_value.mod(rhs));
            
            case EQ:        return new BooleanLiteral(_value.compareTo(rhs) == 0);
            case NEQ:       return new BooleanLiteral(_value.compareTo(rhs) != 0);
            case LT:        return new BooleanLiteral(_value.compareTo(rhs) == -1);
            case LTE:       return new BooleanLiteral(_value.compareTo(rhs) <= 0);
            case GT:        return new BooleanLiteral(_value.compareTo(rhs) == 1);
            case GTE:       return new BooleanLiteral(_value.compareTo(rhs) >= 0);
        }
        
        return super.doBinaryOperation(context, operator, operand);
    }
    
    /**
     * Applies the unary operator to this integer value, such as - to 4 -> -4
     */
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        switch(operator) {
            case PLUS:      return new IntegerLiteral(_value);
            case MINUS:     return new IntegerLiteral(_value.negate());
            case NOT:       return coerceTo(SyntaxTreeTypes.BOOLEAN).doUnaryOperation(context, operator);
        }
        
        return super.doUnaryOperation(context, operator);
    }
    
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        if(newType.equals(SyntaxTreeTypes.NATIVE_OBJECT))
            return new NativeLiteral(_value);
        
        if(newType.equals(SyntaxTreeTypes.BOOLEAN))
            return new BooleanLiteral(!_value.equals(BigInteger.ZERO));
        
        if(newType.equals(SyntaxTreeTypes.FLOAT))
            return new FloatLiteral(new BigDecimal(_value));
        
        if(newType.equals(SyntaxTreeTypes.STRING))
            return new StringLiteral(_value.toString());
        
        return super.coerceTo(newType);
    }
    
    public Object coerceToNativeType() throws RuntimeError {
        return new Integer(_value.intValue());
    }
    
    /**
     * Returns the raw value of this integer literal.
     */
    public BigInteger getValue() {
        return _value;
    }
    
    public int hashCode() {
        return _value.hashCode();
    }
    
    public boolean equals(Object other) {
        return other instanceof IntegerLiteral && ((IntegerLiteral)other)._value.equals(_value);
    }
    
    public String toString() {
        return _value.toString();
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.INTEGER;
    }
}