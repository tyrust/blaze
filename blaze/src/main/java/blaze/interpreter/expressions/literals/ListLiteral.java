package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.math.BigInteger;

/**
 * Represents a literal list value, such as [1,2,"hello",3].
 */
public class ListLiteral extends LiteralExpression implements KeyValueMap {
    private static Map<Identifier, BlazeExpression> _staticVariables = new HashMap<Identifier, BlazeExpression>();
    private List<BlazeExpression> _list;
    
    public static Map<Identifier, BlazeExpression> getStaticVariables() throws RuntimeError {
        return _staticVariables;
    }
    
    protected Map<Identifier, BlazeExpression> getStaticVariablesReference() throws RuntimeError {
        return _staticVariables;
    }
    
    public ListLiteral(List<BlazeExpression> list) {
        _list = list;
        _instanceVariables = new HashMap<Identifier, BlazeExpression>();
        
        // lists are inherently mutable
        _frozen = false;
    }
    
    public LiteralExpression copy() {
        ListLiteral copy = new ListLiteral(new ArrayList<BlazeExpression>(_list));
        copy._instanceVariables = new HashMap<Identifier, BlazeExpression>(_instanceVariables);
        return copy;
    }
    
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        // If the operator is an AND or OR, we coerce to booleans
        if(operator == BinaryOperator.AND || operator == BinaryOperator.OR)
            return coerceTo(SyntaxTreeTypes.BOOLEAN).doBinaryOperation(context, operator, operand);
        
        // We immediately evaluate the operand here
        operand = operand.evaluate(context);
        
        // If the operator is TIMES and the RHS is a number, we duplicate the list
        // This enables the Pythonic syntax [1,2,3]*3 -> [1,2,3,1,2,3,1,2,3]
        // We even support floating point numbers, so that [1,2,3,4]*2.5 -> [1,2,3,4,1,2,3,4,1,2]
        // Similarly, [1,2,3,4]/2 -> [1,2]
        if(operator == BinaryOperator.TIMES || operator == BinaryOperator.DIVIDE) {
            double value = ((FloatLiteral)operand.coerceTo(SyntaxTreeTypes.FLOAT)).getValue().doubleValue();
            if(operator == BinaryOperator.DIVIDE)
                value = 1./value;
            
            int repeat = (int)value;
            double frac = value - repeat;
            
            List<BlazeExpression> newList = new ArrayList<BlazeExpression>();
            for(int i = 0; i < repeat; i++)
                newList.addAll(_list);
            int index = (int)(_list.size() * frac);
            newList.addAll(_list.subList(0, index));
            return new ListLiteral(newList);
        }
        
        // If the RHS is a string, we coerce to strings
        if(operand.getNodeType().equals(SyntaxTreeTypes.STRING))
            return coerceTo(SyntaxTreeTypes.STRING).doBinaryOperation(context, operator, operand);
        
        if(!operand.getNodeType().equals(getNodeType())) {
            switch(operator) {
                case EQ:    return new BooleanLiteral(false);
                case NEQ:   return new BooleanLiteral(true);
            }
        }
        
        // We only support addition and subtraction with lists
        // [1,2,3] + [4,5,6] -> [1,2,3,4,5,6]
        // [1,2,3] - [2,3]   -> [1]
        List<BlazeExpression> rhs = ((ListLiteral)coerce(operand)).getValue();
        switch(operator) {
            case PLUS: {
                List<BlazeExpression> newList = new ArrayList<BlazeExpression>(_list);
                newList.addAll(rhs);
                return new ListLiteral(newList);
            }
            case MINUS: {
                List<BlazeExpression> newList = new ArrayList<BlazeExpression>(_list);
                newList.removeAll(rhs);
                return new ListLiteral(newList);
            }
            
            case EQ:        return new BooleanLiteral(_list.equals(rhs));
            case NEQ:       return new BooleanLiteral(!_list.equals(rhs));
            
            // TODO: LT,GT,LTE,GTE using containment or something?
        }
        
        return super.doBinaryOperation(context, operator, operand);
    }
    
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        switch(operator) {
            // +[1,2,3] -> [1,2,3]; this simply has no effect
            case PLUS:      return new ListLiteral(_list);
            // -[1,2,3] -> [3,2,1]; this is a list reverse operation
            case MINUS:
                List<BlazeExpression> newList = new ArrayList<BlazeExpression>(_list);
                Collections.reverse(newList);
                return new ListLiteral(newList);
            case NOT:       return coerceTo(SyntaxTreeTypes.BOOLEAN).doUnaryOperation(context, operator);
        }
        
        return super.doUnaryOperation(context, operator);
    }
    
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        if(newType.equals(SyntaxTreeTypes.DICT)) {
            Map<BlazeExpression, BlazeExpression> map = new HashMap<BlazeExpression, BlazeExpression>();
            for(int i = 0; i < _list.size(); i++)
                map.put(new IntegerLiteral(BigInteger.valueOf(i)), _list.get(i));
            return new DictLiteral(map);
        }
        
        if(newType.equals(SyntaxTreeTypes.NATIVE_OBJECT))
            return new NativeLiteral(_list);
        
        if(newType.equals(SyntaxTreeTypes.BOOLEAN))
            return new BooleanLiteral(_list.size() > 0);
        
        if(newType.equals(SyntaxTreeTypes.STRING))
            return new StringLiteral(toString());
                
        return super.coerceTo(newType);
    }
    
    public Object coerceToNativeType() throws RuntimeError {
        Object[] nativeArr = new Object[_list.size()];
        for(int i = 0; i < _list.size(); i++)
            nativeArr[i] = _list.get(i).coerceToNativeType();
        return nativeArr;
    }
    
    public List<BlazeExpression> getValue() {
        return _list;
    }
    
    public boolean equals(Object other) {
        return other instanceof ListLiteral && ((ListLiteral)other)._list.equals(_list);
    }
    
    public int hashCode() {
        return _list.hashCode();
    }
    
    public String toString() {
        String s = "[";
        for(int i = 0; i < _list.size(); i++) {
            s += _list.get(i).toString();
            if(i+1 < _list.size())
                s += ", ";
        }
        return s + "]";
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.LIST;
    }
    
    private int getIndexFromKey(BlazeExpression key, boolean allowEnd) throws RuntimeError {
        if(key.getNodeType().equals(SyntaxTreeTypes.INTEGER)) {
            int index = ((IntegerLiteral)key).getValue().intValue();
            while(index < 0 && _list.size() > 0)
                index += _list.size();
            if(index < 0 || index >= _list.size())
                if(!allowEnd || index < 0)
                    throw new RuntimeError("list index out of bounds: " + index);
            return index;
        }
        throw new RuntimeError("invalid index for list: " + key);
    }
    
    public BlazeExpression get(BlazeExpression key) throws RuntimeError {
        return _list.get(getIndexFromKey(key, false));
    }
    
    public void put(BlazeExpression key, BlazeExpression value) throws RuntimeError {
        if(_frozen)
            throw new RuntimeError("list is frozen (immutable) -- it cannot be modified");
        int index = getIndexFromKey(key, true);
        if(index < _list.size())
            _list.set(index, value);
        else
            _list.add(value);
    }
    
    public void delete(BlazeExpression key) throws RuntimeError {
        if(_frozen)
            throw new RuntimeError("list is frozen (immutable) -- it cannot be deleted from");
        int index = getIndexFromKey(key, false);
        _list.remove(index);
    }
}