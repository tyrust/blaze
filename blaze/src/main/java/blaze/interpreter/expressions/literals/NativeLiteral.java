package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Constructor;
import org.apache.commons.lang3.reflect.MethodUtils;

import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

/**
 * Blaze representation of a native Java object.
 * Allows Blaze to interact with Java objects, call their methods, etc.
 */
public class NativeLiteral extends LiteralExpression {
    private static Map<Identifier, BlazeExpression> _staticVariables = new HashMap<Identifier, BlazeExpression>();
    Object _nativeObj;
    
    public static Map<Identifier, BlazeExpression> getStaticVariables() throws RuntimeError {
        return _staticVariables;
    }
    
    protected Map<Identifier, BlazeExpression> getStaticVariablesReference() throws RuntimeError {
        return _staticVariables;
    }
    
    public NativeLiteral(Object nativeObj) {
        _nativeObj = nativeObj;
        _instanceVariables = new HashMap<Identifier, BlazeExpression>();
        
        // native objects are inherently immutable
        // (at least in the "Java sense")
        _frozen = true;
    }
    
    public LiteralExpression copy() {
        NativeLiteral copy = new NativeLiteral(_nativeObj);
        copy._instanceVariables = new HashMap<Identifier, BlazeExpression>(_instanceVariables);
        return copy;
    }
    
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        if(operator == BinaryOperator.AND || operator == BinaryOperator.OR)
            return coerceTo(SyntaxTreeTypes.BOOLEAN).doBinaryOperation(context, operator, operand);
        
        operand = operand.evaluate(context);
        Object nativeOperand = operand.coerceToNativeType();
        
        if(_nativeObj == null) {
            switch(operator) {
                case EQ:    return new BooleanLiteral(nativeOperand == null);
                case NEQ:   return new BooleanLiteral(nativeOperand != null);
            }
        }
        
        switch(operator) {
            case EQ:    return new BooleanLiteral(_nativeObj.equals(nativeOperand));
            case NEQ:   return new BooleanLiteral(!_nativeObj.equals(nativeOperand));
            case AND:
            case OR:
                return coerceTo(SyntaxTreeTypes.BOOLEAN).doBinaryOperation(context, operator, operand);
        }
        
        return super.doBinaryOperation(context, operator, operand);
    }
    
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        if(operator == UnaryOperator.NOT)
            return coerceTo(SyntaxTreeTypes.BOOLEAN).doUnaryOperation(context, operator);
        return super.doUnaryOperation(context, operator);
    }
    
    // Calls the constructor of a class
    @SuppressWarnings("unchecked")
    public BlazeExpression instantiate(Environment context, List<BlazeExpression> args) throws RuntimeError, Signal {
        if(!(_nativeObj instanceof Class))
            throw new RuntimeError("cannot instantiate " + this + " -- it is not a native class");
        
        Object[] nativeArgs = new Object[args.size()];
        Class[] argClasses = new Class[args.size()];
        for(int j = 0; j < args.size(); j++) {
            nativeArgs[j] = args.get(j).evaluate(context).coerceToNativeType();
            argClasses[j] = nativeArgs[j].getClass();
        }
        
        try {
            Class clazz = (Class)_nativeObj;
            Constructor constructor = null;
            for(Constructor c : clazz.getConstructors()) {
                Class[] types = c.getParameterTypes();
                if(types.length != nativeArgs.length)
                    continue;
                boolean works = true;
                for(int i = 0; i < nativeArgs.length; i++) {
                    // try {
                    //     types[i].cast(nativeArgs[i]);
                    // } catch(ClassCastException ex) {
                    //     works = false;
                    //     break;
                    // }
                    
                    if(types[i] == int.class && argClasses[i] == Integer.class)
                        continue;
                    if(types[i] == boolean.class && argClasses[i] == Boolean.class)
                        continue;
                    if(types[i] == float.class && argClasses[i] == Double.class)
                        continue;
                    if(types[i] == double.class && argClasses[i] == Double.class)
                        continue;
                    if(types[i] == long.class && argClasses[i] == Integer.class)
                        continue;
                    if(types[i] == short.class && argClasses[i] == Integer.class)
                        continue;
                    if(types[i] == byte.class && argClasses[i] == Integer.class)
                        continue;
                    if(types[i] == char.class && argClasses[i] == Integer.class)
                        continue;
                    
                    if(!types[i].isAssignableFrom(argClasses[i])) {
                        works = false;
                        break;
                    }
                }
                if(works) {
                    constructor = c;
                    break;
                }
            }
            
            if(constructor == null) {
                String classes = "";
                for(int i = 0; i < argClasses.length; i++) {
                    classes += argClasses[i].getName();
                    if(i+1 < argClasses.length)
                        classes += ", ";
                }
                throw new RuntimeError("no constructor exists for " + this + " that accepts arguments " + classes);
            }
            
            return LiteralExpression.wrapNativeObject(constructor.newInstance(nativeArgs));
        } catch(InstantiationException ex) {
            throw new RuntimeError("could not instantiate " + this + " -- " + ex.getMessage());
        } catch(IllegalAccessException ex) {
            throw new RuntimeError("cannot access constructor for " + this);
        } catch(InvocationTargetException ex) {
            throw new RuntimeError("error while invoking constructor for " + this + " -- " + ex.getMessage());
        }
    }
    
    @SuppressWarnings("unchecked")
    public BlazeExpression getField(Identifier key) throws RuntimeError {
        if(_nativeObj == null)
            throw new RuntimeError("cannot access field " + key + " of " + _nativeObj);
        
        final String name = key.toString();
        final Class clazz;
        if(_nativeObj instanceof Class)
            clazz = (Class)_nativeObj;
        else if(_nativeObj instanceof Package) {
            try {
                return LiteralExpression.wrapNativeObject(Class.forName(((Package)_nativeObj).getName() + "." + key));
            } catch(ClassNotFoundException ex) {
                throw new RuntimeError("class " + key + " not found in package " + _nativeObj);
            }
        } else
            clazz = _nativeObj.getClass();
        
        // Check if this is a class
        try {
            Class clazz2 = Class.forName(clazz.getName() + "." + name);
            return LiteralExpression.wrapNativeObject(clazz2);
        } catch(ClassNotFoundException ex) {}
        
        // Check if this is a field
        try {
            Field field = clazz.getField(name);
            return LiteralExpression.wrapNativeObject(field.get(_nativeObj));
        } catch(NoSuchFieldException ex) {
        } catch(IllegalAccessException ex) {
            throw new RuntimeError("cannot access field " + name + " of " + _nativeObj);
        }
        
        // Otherwise, assume this is a method
        // We don't actually look up the method here; instead, we do this lazily
        // (this allows us to choose the correct method based on the provided arguments,
        // as Java often uses method overloading).
        // We convert this "method" into a Blaze closure that will find the correct
        // native method when called.
        return new ClosureLiteral(new ClosureLiteral.Callable() {
            public BlazeExpression call(List<BlazeExpression> args) throws RuntimeError {
                // Convert the argument list into a list of Java objects
                // Keep track of which classes in what order have been passed in
                Object[] nativeArgs = new Object[args.size()-1];
                Class[] argClasses = new Class[args.size()-1];
                // It is assumed that the 0th argument is the object itself
                // (as in a field call)
                // This is because if the user stores a native object in a variable
                // and calls it like a field call, this is indistinguishable from a
                // native call at the parser level, so we just keep things simple
                for(int j = 1; j < args.size(); j++) {
                    // (args already evaluated by the time we get here)
                    nativeArgs[j-1] = args.get(j).coerceToNativeType();
                    argClasses[j-1] = nativeArgs[j-1].getClass();
                }
                
                Method method = MethodUtils.getMatchingAccessibleMethod(clazz, name, argClasses);
                if(method == null)
                    throw new RuntimeError("method " + name + " not found in " + _nativeObj);
                
                try {
                    Object returnValue = method.invoke(_nativeObj, nativeArgs);
                    return LiteralExpression.wrapNativeObject(returnValue);
                } catch(IllegalAccessException ex) {
                    throw new RuntimeError("illegal access - " + ex);
                } catch(InvocationTargetException ex) {
                    throw new RuntimeError("error while invoking method " + method + " on " + this + " -- " + ex);
                }
            }
        });
    }
    
    public BlazeExpression coerceTo(String type) throws RuntimeError {
        if(type.equals(SyntaxTreeTypes.STRING))
            return new StringLiteral(_nativeObj + "");
        if(type.equals(SyntaxTreeTypes.BOOLEAN))
            return new BooleanLiteral(_nativeObj != null);
        return super.coerceTo(type);
    }
    
    public Object coerceToNativeType() throws RuntimeError {
        return _nativeObj;
    }
    
    public int hashCode() {
        return _nativeObj.hashCode();
    }
    
    public boolean equals(Object other) {
        return other instanceof NativeLiteral && ((NativeLiteral)other)._nativeObj.equals(_nativeObj);
    }
    
    public String toString() {
        return String.format("<native object: %s>", _nativeObj + "");
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.NATIVE_OBJECT;
    }
}