package blaze.interpreter.expressions.literals;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.InterpreterEngine;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.signals.BranchSignal;
import blaze.interpreter.signals.BranchValuesSignal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.FunctionCallExpression;
import blaze.interpreter.expressions.BinaryOperator;
import blaze.interpreter.expressions.UnaryOperator;
import blaze.interpreter.expressions.InvalidOperandError;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
 * Represents a closure. Contains a subexpression (the body) and arguments, and a reference to the environment that created it.
 * Upon being invoked, extends the passed-in environment with its own environment.
 */
public class ClosureLiteral extends LiteralExpression {
    public static interface Callable {
        public BlazeExpression call(List<BlazeExpression> args) throws RuntimeError;
    }
    
    private static Map<Identifier, BlazeExpression> _staticVariables = new HashMap<Identifier, BlazeExpression>();
    private List<Identifier> _argList;
    private Identifier _vararg;
    private BlazeExpression _body;
    private Callable _callable;
    private Environment _context, _liveEnvironment;
    private Identifier _identifier;
    
    public static Map<Identifier, BlazeExpression> getStaticVariables() throws RuntimeError {
        return _staticVariables;
    }
    
    protected Map<Identifier, BlazeExpression> getStaticVariablesReference() throws RuntimeError {
        return _staticVariables;
    }
    
    public ClosureLiteral(List<Identifier> argList, Identifier vararg, BlazeExpression body, Environment context) {
        _argList = argList;
        _vararg = vararg;
        _body = body;
        _context = context;
        _instanceVariables = new HashMap<Identifier, BlazeExpression>();
        
        // Closures are immutable by default
        _frozen = true;
    }
    
    public ClosureLiteral(Callable callable) {
        _callable = callable;
        
        _frozen = true;
    }
    
    public LiteralExpression copy() {
        LiteralExpression copy;
        if(_callable != null)
            copy = new ClosureLiteral(_callable);
        else
            copy = new ClosureLiteral(_argList, _vararg, _body, _context);
        copy._instanceVariables = new HashMap<Identifier, BlazeExpression>(_instanceVariables);
        return copy;
    }
    
    public List<Identifier> getParameters() {
        List<Identifier> params = new ArrayList<Identifier>(_argList);
        if(_vararg != null)
            params.add(_vararg);
        return params;
    }
    
    public Identifier getVariableArgument() {
        return _vararg;
    }
    
    /**
     * Set the closure's identifier
     * @param   id  The identifier for this closure.
     */
    public void setIdentifier(Identifier id) {
        _identifier = id;
    }
    
    /**
     * @return  The closure's identifier.
     */
    public Identifier getIdentifier() {
        return _identifier;
    }
    
    /**
     * @return  The closure's "live" environment -- that is, the environment in which it is being evaluated.
     *          (This is distinct from the closure's "context" environment, which is the one it was created in).
     */
    public Environment getLiveEnvironment() {
        return _liveEnvironment;
    }
    
    /**
     * Checks that the provided arguments are valid for this closure.
     * @param   args    Potential arguments for this closure.
     */
    public void checkArguments(List<BlazeExpression> args) throws RuntimeError {
        if(_callable != null)
            return;
        
        // checked at invoke time
        if(_vararg != null)
            return;
        
        if(_argList.size() > args.size())
            throw new RuntimeError("not enough arguments to " + this);
        if(_argList.size() < args.size())
            throw new RuntimeError("too many arguments to " + this);
    }
    
    /**
     * Calls the closure with the given arguments.
     * @param   arguments   List of expressions to bind as formal parameters.
     * @return              The result of running the closure.
     */
    public BlazeExpression invoke(List<BlazeExpression> arguments) throws RuntimeError, Signal {
        try {
            // If a callable was provided, this takes precedence over everything else.
            if(_callable != null)
                return _callable.call(arguments);
            
            // Extend the closure's environment to bind the arguments
            // Note that the evaluation of the arguments is delayed
            Environment newEnv = _context.extend();
            
            for(int i = 0; i < _argList.size(); i++)
                newEnv.bind(_argList.get(i), arguments.get(i));
            if(arguments.size() >= _argList.size() && _vararg != null) {
                List<BlazeExpression> remaining = new ArrayList<BlazeExpression>(arguments.subList(_argList.size(), arguments.size()));
                
                if(remaining.size() == 1 && remaining.get(0).getNodeType().equals(SyntaxTreeTypes.LIST))
                    // if a list was put in place of the vararg, we expand it one level and pass it in
                    // (if the intention was to pass in an actual list, you'd have to double-list it, like [[x]])
                    newEnv.bind(_vararg, remaining.get(0));
                else
                    // otherwise, we wrap these in a list
                    newEnv.bind(_vararg, new ListLiteral(remaining));
            }
            
            _liveEnvironment = newEnv;
            return _body.evaluate(newEnv);
        } catch(BranchSignal sig) {
            List<BlazeExpression> possibleValues = new ArrayList<BlazeExpression>();
            for(Environment env : sig.getParallelEnvironments())
                possibleValues.add((BlazeExpression)env.getCachedReturnValue(_body));
            
            // If we trap a branch signal in a closure call, we shouldn't handle it as we would in any other
            // expression; this is because in traveling from the closure environment to the outer environment
            // in which the closure was invoked, our perspective changes entirely (the closure environment
            // is a child of the one it was created in).
            // We instead send a different signal that includes what return values the closure ended up with
            // in its parallel execution.
            // We don't have to send the closure along with this signal because it is known to the caller
            // (the caller is guaranteed to be a FunctionCallExpression, as there is no other way a closure
            // can be invoked).
            throw new BranchValuesSignal(possibleValues);
        }
    }
    
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        // For now, closures support no binary operations other than boolean ones.
        operand = operand.evaluate(context);
        
        switch(operator) {
            case EQ:    return new BooleanLiteral(this == operand);
            case NEQ:   return new BooleanLiteral(this != operand);
            case AND:
            case OR:
                return coerceTo(SyntaxTreeTypes.BOOLEAN).doBinaryOperation(context, operator, operand);
        }
        
        return super.doBinaryOperation(context, operator, operand);
    }
    
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        // The only unary operator supported is NOT
        if(operator == UnaryOperator.NOT)
            return coerceTo(SyntaxTreeTypes.BOOLEAN).doUnaryOperation(context, operator);
        
        return super.doUnaryOperation(context, operator);
    }
    
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        if(newType.equals(SyntaxTreeTypes.NATIVE_OBJECT))
            return new NativeLiteral(this);
        
        if(newType.equals(SyntaxTreeTypes.BOOLEAN))
            return new BooleanLiteral(true);
        
        return super.coerceTo(newType);
    }
    
    // Closures are simply represented as themselves in simplest native terms
    // Obviously, they can't exactly be used for many things in this form, but...
    public Object coerceToNativeType() {
        return this;
    }
    
    public String toString() {
        String name = _identifier == null ? "(anonymous function)" : "function " + _identifier;
        
        if(_callable != null)
            return name + ": ??? -> { <native code> }";
        
        String args = "";
        for(int i = 0; i < _argList.size(); i++) {
            args += _argList.get(i).toString();
            if(i+1 < _argList.size())
                args += ", ";
        }
        return String.format("%s: %s -> %s", name, args, _body.toString());
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.CLOSURE;
    }
}