package blaze.interpreter.expressions.literals;

import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.grammar.SyntaxTreeTypes;
import blaze.concurrent.ConcurrencyEngine;

import java.util.List;
import java.util.ArrayList;

public class ChannelLiteral extends LiteralExpression {
    protected List<ClosureLiteral> _listeners = new ArrayList<ClosureLiteral>();
    boolean _isParallel = false;
    
    public void addListener(Identifier variable, BlazeExpression body, Environment context) {
        List<Identifier> argList = new ArrayList<Identifier>();
        argList.add(variable);
        ClosureLiteral listener = new ClosureLiteral(argList, null, body, context);
        _listeners.add(listener);
    }
    
    public LiteralExpression copy() {
        ChannelLiteral newChannel = new ChannelLiteral();
        newChannel._listeners = new ArrayList<ClosureLiteral>(_listeners);
        newChannel._isParallel = _isParallel;
        return newChannel;
    }
    
    public void parallelize() {
        _isParallel = true;
    }
    
    public void push(BlazeExpression value) throws RuntimeError, Signal {
        for(ClosureLiteral listener : _listeners) {
            List<BlazeExpression> arguments = new ArrayList<BlazeExpression>();
            arguments.add(value);
            if(_isParallel)
                ConcurrencyEngine.invokeInFuture(listener, arguments);
            else
                listener.invoke(arguments);
        }
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.CHANNEL;
    }
    
    public String toString() {
        if(_isParallel)
            return "<parallel channel>";
        return "<channel>";
    }
}