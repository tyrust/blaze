package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.expressions.literals.ClosureLiteral;

import java.util.List;

/**
 * A lambda expression. Generates a new closure and returns it, with a reference to the environment it was created in.
 */
public class LambdaExpression extends BlazeExpression {
    private List<Identifier> _argList;
    private Identifier _vararg;
    private BlazeExpression _body;
    
    public LambdaExpression(List<Identifier> argList, Identifier vararg, BlazeExpression body) {
        _argList = argList;
        _vararg = vararg;
        _body = body;
    }
    
    public LambdaExpression(List<Identifier> argList, BlazeExpression body) {
        this(argList, null, body);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        return new ClosureLiteral(_argList, _vararg, _body, context);
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.LAMBDA;
    }
    
    public String toString() {
        String args = "";
        for(int i = 0; i < _argList.size(); i++) {
            args += _argList.get(i).toString();
            if(i+1 < _argList.size())
                args += ", ";
        }
        if(_vararg != null) {
            if(!args.equals(""))
                args += ", ";
            args += _vararg.toString() + "...";
        }
        return String.format("%s -> %s", args, _body.toString());
    }
}