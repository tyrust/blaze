package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.InterpreterEngine;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.signals.ReturnSignal;
import blaze.interpreter.signals.BranchSignal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.runtime.Console;

import java.util.List;

/**
 * A body of a function or program or whatever; essentially a list of sub-expressions.
 * Simply calls each of its sub-expressions and returns when it encounters a return signal.
 */
public class BodyExpression extends BlazeExpression {
    private List<BlazeExpression> _expressions;
    
    // This indicates whether or not the body is part of the callstack.
    // If this is the body of something like an if block, returns should be propagated upward.
    // If this is the body of a lambda, it should catch the return signal.
    private boolean _canReturn;
    
    public BodyExpression(List<BlazeExpression> expressions, boolean canReturn) {
        _expressions = expressions;
        _canReturn = canReturn;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        try {
            // Iterate through each line of the body and run it
            for(int i = 0; i < _expressions.size(); i++)
                _expressions.get(i).evaluate(context);
        } catch(ReturnSignal sig) {
            if(_canReturn)
                return sig.getReturnValue();
            else
                throw sig;
        }
        return new NullLiteral();
    }
    
    public List<BlazeExpression> getExpressions() {
        return _expressions;
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.BODY;
    }
    
    public String toString() {
        String s = "{\r\n";
        for(BlazeExpression ex : _expressions)
            s += "\t" + ex + "\r\n";
        s += "}";
        return s;
    }
    
    public String toIndentedString() {
        if(_expressions.size() == 1)
            return Console.indent(String.format("{ %s }", _expressions.get(0).toString()));
        
        Console.dedent();
        String s = Console.indent("{\r\n");
        Console.indent();
        for(BlazeExpression ex : _expressions)
            s += ex.toIndentedString() + "\r\n";
        Console.dedent();
        s += Console.indent("}");
        Console.indent();
        return s;
    }
}