package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.grammar.SyntaxError;

/**
 * A binary operator which can be applied to two or more expressions
 */
public enum BinaryOperator implements Operator {
    PLUS(SyntaxTreeTypes.BINARY_PLUS),
    MINUS(SyntaxTreeTypes.BINARY_MINUS),
    TIMES(SyntaxTreeTypes.BINARY_TIMES),
    DIVIDE(SyntaxTreeTypes.BINARY_DIVIDE),
    MOD(SyntaxTreeTypes.BINARY_MOD),
    
    EQ(SyntaxTreeTypes.BINARY_EQ),
    NEQ(SyntaxTreeTypes.BINARY_NEQ),
    LT(SyntaxTreeTypes.BINARY_LT),
    LTE(SyntaxTreeTypes.BINARY_LTE),
    GT(SyntaxTreeTypes.BINARY_GT),
    GTE(SyntaxTreeTypes.BINARY_GTE),
    AND(SyntaxTreeTypes.BINARY_AND),
    OR(SyntaxTreeTypes.BINARY_OR);
    
    private String _token;
    
    private BinaryOperator(String token) {
        _token = token;
    }
    
    public String getToken() {
        return _token;
    }
    
    public String toString() {
        return _token;
    }
    
    /**
     * Finds and returns the binary operator corresponding to a raw AST token.
     * @param   token       Raw AST node type, such as "+"
     * @return              Corresponding binary operator, such as BinaryOperator.PLUS
     */
    public static BinaryOperator getOperatorForToken(String token) {
        for(BinaryOperator op : BinaryOperator.values())
            if(op.getToken().equals(token))
                return op;
        return null;
    }
    
    public static BinaryOperator getOperatorForCompoundOperator(String operator) {
        if(operator.equals(SyntaxTreeTypes.BINARY_COMPOUND_PLUS))
            return PLUS;
        if(operator.equals(SyntaxTreeTypes.BINARY_COMPOUND_MINUS))
            return MINUS;
        if(operator.equals(SyntaxTreeTypes.BINARY_COMPOUND_TIMES))
            return TIMES;
        if(operator.equals(SyntaxTreeTypes.BINARY_COMPOUND_DIVIDE))
            return DIVIDE;
        if(operator.equals(SyntaxTreeTypes.BINARY_COMPOUND_MOD))
            return MOD;
        return null;
    }
}