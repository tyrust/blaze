package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.expressions.literals.DictLiteral;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * A dict expression. Consists internally of two lists, for initialization keys and values.
 * Upon evaluation, creates a dict literal that actually maps the keys to values.
 */
public class DictExpression extends BlazeExpression {
    private List<BlazeExpression> _keys, _values;
    
    public DictExpression(List<BlazeExpression> keys, List<BlazeExpression> values) {
        _keys = keys;
        _values = values;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        Map<BlazeExpression, BlazeExpression> map = new HashMap<BlazeExpression, BlazeExpression>();
        for(int i = 0; i < _keys.size(); i++) {
            BlazeExpression key = _keys.get(i).evaluate(context);
            BlazeExpression val = _values.get(i).evaluate(context);
            map.put(key, val);
        }
        
        return new DictLiteral(map);
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.DICT;
    }
    
    public String toString() {
        String s = "[";
        for(int i = 0; i < _keys.size(); i++) {
            s += _keys.get(i) + ": " + _values.get(i);
            if(i+1 < _keys.size())
                s += ", ";
        }
        return s + "]";
    }
}