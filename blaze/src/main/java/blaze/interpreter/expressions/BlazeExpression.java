package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.InterpreterEngine;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.signals.BranchSignal;
import blaze.interpreter.signals.ReturnSignal;
import blaze.interpreter.expressions.ReturnStatement;
import blaze.interpreter.expressions.literals.ClosureLiteral;
import blaze.interpreter.expressions.literals.NativeLiteral;
import blaze.interpreter.expressions.literals.ParallelLiteral;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.runtime.Console;

import java.util.List;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;

import java.io.Serializable;

/**
 * Represents an abstract expression in the tree of expressions that constitutes a program.
 */
public abstract class BlazeExpression implements Serializable {
    private boolean _isStatement = false;
    
    /**
     * Sets whether or not this expression is a "statement"
     */
    public void setIsStatement(boolean statement) {
        _isStatement = statement;
    }
    
    /**
     * @return Whether or not the expression is a statement.
     */
    public boolean isStatement() {
        return _isStatement;
    }
    
    /**
     * Returns the node type for this expression.
     * @return The node type in the AST, such as "LAMBDA"
     */
    public abstract String getNodeType();
    
    /**
     * Convenience method; returns the Blaze type for this expression's node type.
     * @return Blaze type as a string.
     */
    public String getBlazeType() {
        return SyntaxTreeTypes.getBlazeType(getNodeType());
    }
    
    /**
     * Evaluates this expression node in a given context.
     * @param   context     The environment in which to evaluate the expression.
     * @return              An expression representing the return value.
     */
    public BlazeExpression evaluate(Environment context) throws RuntimeError, Signal {
        if(_isStatement)
            InterpreterEngine.setExpressionForEnvironment(this, context);
        
        Object cached = context.getCachedReturnValue(this);
        if(cached != null) {
            if(cached instanceof Signal)
                throw (Signal)cached;
            return (BlazeExpression)cached;
        }
        
        try {
            BlazeExpression evaluated = evaluateInternal(context);
            if(evaluated.getNodeType().equals(SyntaxTreeTypes.PARALLEL))
                evaluated = evaluated.evaluate(context);
            context.cacheReturnValue(this, evaluated);
            return evaluated;
        } catch(BranchSignal sig) {
            List<Object> results = InterpreterEngine.evaluateConcurrently(this, sig.getParallelEnvironments());
            
            if(!_isStatement) {
                List<Environment> parallelEnvironments = new ArrayList<Environment>();
                for(int i = 0; i < results.size(); i++) {
                    Environment environment = sig.getParallelEnvironments().get(i); //context.extend();
                    if(results.get(i) instanceof BlazeExpression) {
                        BlazeExpression result = (BlazeExpression)results.get(i);
                        if(result.getNodeType().equals(SyntaxTreeTypes.PARALLEL))
                            result = result.evaluate(environment);
                        environment.cacheReturnValue(this, result);
                    } else
                        environment.cacheReturnValue(this, results.get(i));
                    parallelEnvironments.add(environment);
                }
                throw new BranchSignal(parallelEnvironments);
            }
            
            context.collectParallelExtensions();
            
            if(results.size() > 0) {
                List<BlazeExpression> returnValues = new ArrayList<BlazeExpression>();
                List<BlazeExpression> returnSignalValues = new ArrayList<BlazeExpression>();
                
                for(Object obj : results) {
                    if(obj instanceof BlazeExpression)
                        returnValues.add((BlazeExpression)obj);
                    else if(obj instanceof ReturnSignal)
                        returnSignalValues.add(((ReturnSignal)obj).getReturnValue());
                }
                
                if(returnValues.size() > 0 && returnSignalValues.size() > 0)
                    throw new RuntimeError("Invalid state -- a statement branched in ways that both returned and did not return from a function. This is currently not supported.");
                
                if(returnSignalValues.size() > 0) {
                    ReturnSignal returnSig = new ReturnSignal(new ParallelLiteral(returnSignalValues));
                    context.cacheReturnValue(this, returnSig);
                    throw returnSig;
                }
                
                ParallelLiteral parallel = new ParallelLiteral(returnValues);
                context.cacheReturnValue(this, parallel);
                return parallel;
            }
            return new NullLiteral();
        } catch(ConcurrentModificationException ex) {
            throw new RuntimeError("multiple workers attempted to modify " + this + " at the same time");
        }
    }
    
    /**
     * The actual implementation of the evaluate function.
     * (should never be called directly; only call the wrapping evaluate() function).
     */
    protected abstract BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal;
    
    /**
     * Returns the result of applying a binary operator (such as + - / etc) to this expression and another.
     * Raises a runtime error if the operator cannot be applied.
     * @param   context     The environment in which to do the operation.
     * @param   operator    A BinaryOperator to apply
     * @param   operand     The other argument to the operator
     * @return              The resultant value of applying the operator
     */
    public BlazeExpression doBinaryOperation(Environment context, BinaryOperator operator, BlazeExpression operand) throws RuntimeError, Signal {
        // By default, simply raise an exception indicating that this expression does not allow binary operations
        throw new InvalidOperandError(operator, this);
    }
    
    /**
     * Returns the result of applying a unary operator (such as + - etc) to this expression.
     * Raises a runtime error if the operator cannot be applied.
     * @param   context     The environment in which to do the operation.
     * @param   operator    A UnaryOperator to apply
     * @return              The resultant value of applying the operator
     */
    public BlazeExpression doUnaryOperation(Environment context, UnaryOperator operator) throws RuntimeError, Signal {
        // By default, simply raise an exception indicating that this expression does not allow unary operations
        throw new InvalidOperandError(operator, this);
    }
    
    /**
     * Coerces this expression into one of a different SyntaxTreeType.
     * Raises an exception if the coercion is not possible.
     * @param   newType     A string representing the AST node type that the expression should be coerced into.
     * @return              A new expression representing the coerced value.
     */
    public BlazeExpression coerceTo(String newType) throws RuntimeError {
        // By default, raise an exception indicating that this expression cannot be coerced into the desired type.
        throw new RuntimeError(getBlazeType() + " cannot be coerced to " + SyntaxTreeTypes.getBlazeType(newType));
    }
    
    /**
     * Convenience method; coerces the given expression into one of this expression's type.
     * @param   expr    The expression to coerce.
     * @return          The coerced expression.
     */
    protected BlazeExpression coerce(BlazeExpression expr) throws RuntimeError {
        return expr.coerceTo(getNodeType());
    }
    
    /**
     * Coerces this literal into the appropriate Java type.
     * For example, StringLiteral("hello") -> String("hello")
     * @return A pojo representation of this literal.
     */
    public Object coerceToNativeType() throws RuntimeError {
        throw new RuntimeError(getBlazeType() + " cannot be used in native call");
    }
    
    /**
     * Returns a human-readable representation of this expression.
     * This is generally the program output, and so it should be as minimalistic and readable as possible.
     * @return A human-readable string representation of this expression.
     */
    public abstract String toString();
    
    public String toIndentedString() {
        return Console.indent(toString());
    }
}