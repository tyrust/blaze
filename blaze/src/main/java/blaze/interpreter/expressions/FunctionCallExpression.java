package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.InterpreterEngine;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.signals.BranchSignal;
import blaze.interpreter.signals.BranchValuesSignal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.expressions.literals.ClosureLiteral;

import java.util.List;
import java.util.LinkedList;

/**
 * A function call expression. Generates a new environment that binds a closure's formal parameters, then invokes it.
 */
public class FunctionCallExpression extends BlazeExpression {
    private BlazeExpression _function;
    private Identifier _field;
    private List<BlazeExpression> _argList;
    private boolean _nativeCall;
    
    public FunctionCallExpression(BlazeExpression function, Identifier field, List<BlazeExpression> argList, boolean nativeCall) {
        _function = function;
        _field = field;
        _argList = argList;
        _nativeCall = nativeCall;
    }
    
    public FunctionCallExpression(BlazeExpression function, Identifier field, List<BlazeExpression> argList) {
        this(function, field, argList, false);
    }
    
    public FunctionCallExpression(BlazeExpression function, List<BlazeExpression> argList, boolean nativeCall) {
        this(function, null, argList, nativeCall);
    }
    
    public FunctionCallExpression(BlazeExpression function, List<BlazeExpression> argList) {
        this(function, null, argList, false);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        try {
            // We evaluate the function here.
            BlazeExpression function = _function.evaluate(context);
            
            List<BlazeExpression> argList = _argList;
            
            // If this is a field call, we get the field and call it
            if(_field != null) {
                GetExpression get = new GetExpression(function, _field);
                List<BlazeExpression> newArgList = new LinkedList<BlazeExpression>(_argList);
                // Put the reference to `self` in the list
                // (notice that it is only evaluated once)
                newArgList.add(0, function);
                argList = newArgList;
                function = get.evaluateInternal(context);
                // FunctionCallExpression newCall = new FunctionCallExpression(get, newArgList);
                // return newCall.evaluateInternal(context);
            }
            
            // Otherwise, if it isn't a closure, this is a runtime error.
            if(!function.getNodeType().equals(SyntaxTreeTypes.CLOSURE))
                throw new RuntimeError(function.toString() + " is not callable");
            
            ClosureLiteral closure = (ClosureLiteral)function;
            closure.checkArguments(argList);
            
            List<BlazeExpression> evaluatedArgs = new LinkedList<BlazeExpression>();
            // If it's a native call, we also provide the object in the first argument
            if(_nativeCall)
                evaluatedArgs.add(function);
            for(BlazeExpression arg : argList)
                evaluatedArgs.add(arg.evaluate(context));
            
            InterpreterEngine.pushClosureOnCallStack(closure);
            BlazeExpression returnValue = closure.invoke(evaluatedArgs);
            InterpreterEngine.popClosureOffCallStack();
            
            return returnValue;
        } catch(BranchValuesSignal sig) {
            List<Environment> parallelEnvironments = new LinkedList<Environment>();
            for(BlazeExpression value : sig.getPossibleValues()) {
                Environment env = context.extend();
                env.cacheReturnValue(this, value);
                parallelEnvironments.add(env);
            }
            throw new BranchSignal(parallelEnvironments);
        }
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.FUNCTION_CALL;
    }
    
    public String toString() {
        String args = "";
        for(int i = 0; i < _argList.size(); i++) {
            args += _argList.get(i).toString();
            if(i+1 < _argList.size())
                args += ", ";
        }
        String fn = "";
        if(_nativeCall)
            fn += "native ";
        fn += _function;
        if(_field != null)
            fn += "." + _field;
        
        // Parallellizes desugar into a lambda wrapped in a function call 
        // as part of a workaround to create the parallel in an isolated
        // environment. To make this pretty-print correctly, we avoid printing
        // the following "()" here.
        if(_function.getNodeType().equals(SyntaxTreeTypes.PARALLELIZE))
            return fn;
        return String.format("%s(%s)", fn, args);
    }
}