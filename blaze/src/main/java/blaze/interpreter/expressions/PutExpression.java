package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.grammar.SyntaxError;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.expressions.literals.*;

import java.util.Map;

/**
 * Puts a value for a key into a key-value map.
 */
public class PutExpression extends BlazeExpression {
    private BlazeExpression _map;
    private BlazeExpression _key;
    private BlazeExpression _value;
    private Identifier _fieldKey;
    private String _prototypeType;
    
    public PutExpression(BlazeExpression map, BlazeExpression key, BlazeExpression value) {
        _map = map;
        _key = key;
        _value = value;
    }
    
    public PutExpression(BlazeExpression map, Identifier fieldKey, BlazeExpression value) {
        _map = map;
        _fieldKey = fieldKey;
        _value = value;
    }
    
    public PutExpression(String prototypeType, Identifier fieldKey, BlazeExpression value) {
        _prototypeType = prototypeType;
        _fieldKey = fieldKey;
        _value = value;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        BlazeExpression value = _value.evaluate(context);
        
        if(_prototypeType != null) {
            // This means this is a prototype put expression
            // i.e. of the form list.prototype.length = ...
            
            // FIXME: there is probably a better way to do this
            Map<Identifier, BlazeExpression> vars = null;
            if(_prototypeType.equals(SyntaxTreeTypes.STRING))
                vars = StringLiteral.getStaticVariables();
            if(_prototypeType.equals(SyntaxTreeTypes.LIST))
                vars = ListLiteral.getStaticVariables();
            if(_prototypeType.equals(SyntaxTreeTypes.DICT))
                vars = DictLiteral.getStaticVariables();
            if(_prototypeType.equals(SyntaxTreeTypes.BOOLEAN))
                vars = BooleanLiteral.getStaticVariables();
            if(_prototypeType.equals(SyntaxTreeTypes.FLOAT))
                vars = FloatLiteral.getStaticVariables();
            if(_prototypeType.equals(SyntaxTreeTypes.INTEGER))
                vars = IntegerLiteral.getStaticVariables();
            if(_prototypeType.equals(SyntaxTreeTypes.NATIVE_OBJECT))
                vars = NativeLiteral.getStaticVariables();
            if(_prototypeType.equals(SyntaxTreeTypes.CLOSURE))
                vars = ClosureLiteral.getStaticVariables();
            
            if(vars == null)
                throw new RuntimeError("type " + SyntaxTreeTypes.getBlazeType(_prototypeType) + " has no prototype");
            
            vars.put(_fieldKey, value);
            
            // If the value is a closure, we name it here
            if(value.getNodeType().equals(SyntaxTreeTypes.CLOSURE))
                ((ClosureLiteral)value).setIdentifier(new Identifier(SyntaxTreeTypes.getBlazeType(_prototypeType) + ".prototype." + _fieldKey));
            
            return value;
        }
        
        BlazeExpression map = _map.evaluate(context);
        
        if(_fieldKey != null) {
            if(map instanceof BlazeObject) {
                ((BlazeObject)map).setField(_fieldKey, value);
                return value;
            } else
                throw new RuntimeError(map + " is not a Blaze object; cannot assign to its fields");
        }
        
        if(map instanceof KeyValueMap) {
            BlazeExpression key = _key.evaluate(context);
            ((KeyValueMap)map).put(key, value);
            // Allows for C-style assignment chaining
            return value;
        }
        
        throw new RuntimeError(map + " does not support key assignment");
    }
    
    public String getNodeType() {
        if(_prototypeType != null)
            return SyntaxTreeTypes.PROTOTYPE_PUT_FIELD;
        if(_fieldKey != null)
            return SyntaxTreeTypes.PUT_FIELD;
        return SyntaxTreeTypes.PUT;
    }
    
    public String toString() {
        if(_prototypeType != null)
            return String.format("%s.prototype.%s = %s", _prototypeType, _fieldKey.toString(), _value.toString());
        if(_fieldKey != null)
            return String.format("%s.%s = %s", _map.toString(), _fieldKey.toString(), _value.toString());
        return String.format("%s[%s] = %s", _map.toString(), _key.toString(), _value.toString());
    }
}