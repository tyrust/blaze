package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;

/**
 * A typecast expression. Performs the type coercion and returns the converted expression.
 */
public class TypecastExpression extends BlazeExpression {
    private String _targetType;
    private BlazeExpression _expression;
    
    public TypecastExpression(String targetType, BlazeExpression expression) {
        _targetType = targetType;
        _expression = expression;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        return _expression.evaluate(context).coerceTo(_targetType);
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.TYPECAST;
    }
    
    public String toString() {
        return String.format("%s(%s)", SyntaxTreeTypes.getBlazeType(_targetType), _expression.toString());
    }
}