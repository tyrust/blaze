package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.expressions.literals.LiteralExpression;
import blaze.interpreter.expressions.literals.NativeLiteral;
import blaze.interpreter.expressions.literals.NullLiteral;

import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Field;

/**
 * A native reference expression. Finds the class/field by traversing the package declaration.
 */
public class NativeReferenceExpression extends BlazeExpression {
    private List<String> _packageDeclaration;
    
    public NativeReferenceExpression(List<String> packageDeclaration) {
        _packageDeclaration = packageDeclaration;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        int i = 0;
        String className = "";
        Class clazz = null;
        
        // First check if this symbol has been imported
        List<String> decl = context.getDeclarationForNativeSymbol(_packageDeclaration.get(0));
        if(decl != null) {
            decl = new ArrayList<String>(decl); // clone
            if(decl.size() > 1)
                decl.addAll(_packageDeclaration.subList(1, _packageDeclaration.size()));
        } else
            decl = _packageDeclaration;
        
        // Keep iterating through the packages until the class is found
        for(; i < decl.size(); i++) {
            if(i != 0)
                className += ".";
            className += decl.get(i);
            try {
                clazz = Class.forName(className);
                break;
            } catch(ClassNotFoundException ex) {
            }
        }
        i++;
        
        // If it's not a class, it might be a package
        if(clazz == null) {
            Package pkg = Package.getPackage(className);
            if(pkg != null)
                return LiteralExpression.wrapNativeObject(pkg);
        }
        
        // If it's not a package, try the package imports
        if(clazz == null) {
            // If the class isn't found yet, try the package imports
            for(List<String> pkg : context.getNativePackageImports()) {
                List<String> newDecl = new ArrayList<String>(pkg);
                boolean stop = true;
                // make sure we don't try too hard and get things like java.io.java.io...
                for(int j = 0; j < pkg.size(); j++) {
                    if(!pkg.get(j).equals(decl.get(j))) {
                        stop = false;
                        break;
                    }
                }
                if(stop)
                    break;
                newDecl.addAll(decl);
                try {
                    return new NativeReferenceExpression(newDecl).evaluateInternal(context);
                } catch(RuntimeError err) {
                } catch(Signal sig) {
                    throw sig;
                }
            }
            
            throw new RuntimeError("native symbol " + decl + " cannot be found (did you forget to import a package?)");
        }
        
        // If we're out of package components at this point, we just wanted the class
        if(i >= decl.size())
            return LiteralExpression.wrapNativeObject(clazz);
        
        Object field = null;
        // Now that we've found the class, everything remaining until the final component must be a public field
        for(; i < decl.size()-1; i++) {
            try {
                Field f = clazz.getField(decl.get(i));
                field = f.get(clazz);
                clazz = field.getClass();
            } catch(NoSuchFieldException ex) {
                throw new RuntimeError("field " + decl.get(i) + " not found in " + clazz);
            } catch(IllegalAccessException ex) {
                throw new RuntimeError("field " + decl.get(i) + " cannot be accessed in " + clazz);
            }
        }
        
        // For the last component, we package this in a NativeLiteral and use standard Blaze field accessors
        String last = decl.get(decl.size()-1);
        NativeLiteral literal = new NativeLiteral(field != null ? field : clazz);
        return literal.getField(new Identifier(last));
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.NATIVE_REFERENCE;
    }
    
    public String toString() {
        return String.format("<native reference %s>", _packageDeclaration);
    }
}