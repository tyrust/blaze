package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.ListLiteral;
import blaze.interpreter.expressions.literals.ChannelLiteral;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.runtime.Console;

/**
 * A when statement, of the form `when x <- y { ... }`
 */
public class WhenStatement extends BlazeExpression {
    private Identifier _variable;
    private BlazeExpression _channel, _body;
    
    public WhenStatement(Identifier variable, BlazeExpression channel, BlazeExpression body) {
        _variable = variable;
        _channel = channel;
        _body = body;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        BlazeExpression channel = _channel.evaluate(context);
        if(channel instanceof ChannelLiteral)
            ((ChannelLiteral)channel).addListener(_variable, _body, context);
        else
            throw new RuntimeError("cannot listen on object of type " + channel.getBlazeType() + " -- must be channel");
        // whens return nothing
        return new NullLiteral();
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.WHEN;
    }
    
    public String toString() {
        return String.format("when %s <- %s\r\n\t%s", _variable.toString(), _channel.toString(), _body.toString());
    }
    
    public String toIndentedString() {
        String s = Console.indent("when " + _variable + " <- " + _channel + "\r\n");
        Console.indent();
        s += _body.toIndentedString();
        Console.dedent();
        return s;
    }
}