package blaze.interpreter.expressions;

/**
 * An interface representing a generic unary or binary operator.
 */
public interface Operator {
    /**
     * Returns the raw AST token corresponding to this operator.
     * @return Raw AST node type, such as "+"
     */
    public String getToken();
}