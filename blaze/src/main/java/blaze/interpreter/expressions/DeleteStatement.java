package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.KeyValueMap;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;

/**
 * A delete statement. Deletes a field or index in a key-value map.
 */
public class DeleteStatement extends BlazeExpression {
    private BlazeExpression _variable;
    private BlazeExpression _index;
    private Identifier _field;
    
    public DeleteStatement(BlazeExpression variable, BlazeExpression index) {
        _variable = variable;
        _index = index;
        setIsStatement(true);
    }
    
    public DeleteStatement(BlazeExpression variable, Identifier field) {
        _variable = variable;
        _field = field;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        BlazeExpression var = _variable.evaluate(context);
        
        if(_field != null) {
            if(var instanceof BlazeObject)
                ((BlazeObject)var).deleteField(_field);
            else
                throw new RuntimeError("cannot delete field from non-object");
        }
        
        if(_index != null) {
            if(var instanceof KeyValueMap)
                ((KeyValueMap)var).delete(_index.evaluate(context));
            else
                throw new RuntimeError("cannot delete from " + var + " -- object is not indexable");
        }
        
        return new NullLiteral();
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.DELETE;
    }
    
    public String toString() {
        if(_index != null)
            return "delete " + _variable + "[" + _index + "]";
        else
            return "delete " + _variable + "." + _field;
    }
}