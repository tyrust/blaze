package blaze.interpreter.expressions;

import blaze.interpreter.RuntimeError;

public class InvalidOperandError extends RuntimeError {
    public InvalidOperandError(Operator operator, BlazeExpression operand) {
        super(String.format("Invalid operand '%s' to operator %s", operand.toString(), operator.getToken()));
    }
}