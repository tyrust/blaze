package blaze.interpreter.expressions;

import blaze.interpreter.RuntimeError;
import blaze.interpreter.environment.Identifier;

/**
 * An abstract interface to Blaze's "OOP-esque" design.
 * Enables getting/setting of fields on an object.
 */
public interface BlazeObject {
    public BlazeExpression getField(Identifier id) throws RuntimeError;
    public void setField(Identifier id, BlazeExpression value) throws RuntimeError;
    public void deleteField(Identifier id) throws RuntimeError;
}