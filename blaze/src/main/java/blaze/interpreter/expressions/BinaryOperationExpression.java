package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.environment.Environment;

import java.util.List;

/**
 * An expression involving a binary operator applied in a left-associative manner
 */
public class BinaryOperationExpression extends BlazeExpression {
    private BinaryOperator _operator;
    private BlazeExpression _lhs, _rhs;
    
    public BinaryOperationExpression(BinaryOperator operator, BlazeExpression lhs, BlazeExpression rhs) {
        _operator = operator;
        _lhs = lhs;
        _rhs = rhs;
    }
    
    /**
     * Applies the operator.
     * @return The result of applying the operator to the operands.
     */
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        // We evaluate only the left-hand expression at this time. The RHS is evaluated lazily.
        // This enables short-circuit ANDs/ORs, etc.
        BlazeExpression lhs = _lhs.evaluate(context);
        return lhs.doBinaryOperation(context, _operator, _rhs);
    }
    
    public String getNodeType() {
        return _operator.getToken();
    }
    
    public String toString() {
        return _lhs + " " + _operator + " " + _rhs;
    }
}