package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.expressions.literals.NativeLiteral;

import java.util.List;
import java.util.LinkedList;

/**
 * An instantiation expression. Instantiates a new object by calling its constructor and returning it.
 */
public class InstantiationExpression extends BlazeExpression {
    private BlazeExpression _class;
    private List<BlazeExpression> _argList;
    
    public InstantiationExpression(BlazeExpression clazz, List<BlazeExpression> argList) {
        _class = clazz;
        _argList = argList;
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        // We evaluate the function here. If it isn't a native, this is a runtime error.
        // TODO: If we support structs/classes, should handle them here
        BlazeExpression clazz = _class.evaluate(context);
        
        if(!clazz.getNodeType().equals(SyntaxTreeTypes.NATIVE_OBJECT))
            throw new RuntimeError(clazz + " cannot be instantiated");
        
        return ((NativeLiteral)clazz).instantiate(context, _argList);
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.INSTANTIATE;
    }
    
    public String toString() {
        String args = "";
        for(int i = 0; i < _argList.size(); i++) {
            args += _argList.get(i).toString();
            if(i+1 < _argList.size())
                args += ", ";
        }
        return String.format("new %s(%s)", _class.toString(), args);
    }
}