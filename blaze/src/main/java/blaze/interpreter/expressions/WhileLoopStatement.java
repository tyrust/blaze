package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.BooleanLiteral;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;
import blaze.runtime.Console;

/**
 * A while loop, of the form `while condition { body }`
 */
public class WhileLoopStatement extends BlazeExpression {
    private BlazeExpression _condition, _body;
    
    public WhileLoopStatement(BlazeExpression condition, BlazeExpression body) {
        _condition = condition;
        _body = body;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        while(true) {
            // check the condition
            Environment env = context.extend();
            
            boolean condition = ((BooleanLiteral)_condition.evaluate(env).coerceTo(SyntaxTreeTypes.BOOLEAN)).getValue();
            if(!condition)
                break;
            _body.evaluate(env);
        }
        
        // loops return nothing
        return new NullLiteral();
    }
    
    public String getNodeType() {
        return SyntaxTreeTypes.WHILE_LOOP;
    }
    
    public String toString() {
        return String.format("while %s\r\n\t%s", _condition.toString(), _body.toString());
    }
    
    public String toIndentedString() {
        String s = Console.indent(String.format("while %s\r\n", _condition.toString()));
        Console.indent();
        s += _body.toIndentedString();
        Console.dedent();
        return s;
    }
}