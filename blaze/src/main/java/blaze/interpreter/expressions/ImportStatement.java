package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.expressions.literals.NullLiteral;
import blaze.interpreter.environment.Environment;

import java.util.List;

/**
 * An import statement. Adds the import to the environment it's run within.
 */
public class ImportStatement extends BlazeExpression {
    private List<String> _importDeclaration;
    private String _filePath;
    private boolean _nativeImport, _packageImport, _libraryImport;
    
    // Blaze imports
    public ImportStatement(String filePath, boolean libraryImport) {
        _filePath = filePath;
        _libraryImport = libraryImport;
        setIsStatement(true);
    }
    
    // Native imports
    public ImportStatement(List<String> importDecl, boolean packageImport) {
        _importDeclaration = importDecl;
        _nativeImport = true;
        _packageImport = packageImport;
        setIsStatement(true);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        if(_nativeImport) {
            if(_packageImport)
                context.importNativePackage(_importDeclaration);
            else
                context.importNativeSymbol(_importDeclaration);
        } else {
            if(_libraryImport)
                context.importLibraryPackage(_filePath);
            else
                context.importFile(_filePath);
        }
        
        return new NullLiteral();
    }
    
    public String getNodeType() {
        if(_nativeImport)
            return SyntaxTreeTypes.NATIVE_IMPORT;
        return SyntaxTreeTypes.IMPORT;
    }
    
    public String toString() {
        String s = "import ";
        if(_nativeImport)
            s += "native " + _importDeclaration;
        else {
            if(!_libraryImport)
                s += "\"" + _filePath + "\"";
            else
                s += _filePath;
        }
        if(_packageImport)
            s += ".*";
        return s;
    }
}