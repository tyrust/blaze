package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.grammar.SyntaxError;

/**
 * A unary operator which can be applied to an expression.
 */
public enum UnaryOperator implements Operator {
    PLUS(SyntaxTreeTypes.UNARY_PLUS),
    MINUS(SyntaxTreeTypes.UNARY_MINUS),
    NOT(SyntaxTreeTypes.UNARY_NOT);
    
    private String _token;
    
    private UnaryOperator(String token) {
        _token = token;
    }
    
    public String getToken() {
        return _token;
    }
    
    public String toString() {
        return _token;
    }
    
    /**
     * Finds and returns the unary operator corresponding to a raw AST token.
     * @param   token       Raw AST node type, such as "+"
     * @return              Corresponding unary operator, such as UnaryOperator.PLUS
     */
    public static UnaryOperator getOperatorForToken(String token) {
        for(UnaryOperator op : UnaryOperator.values())
            if(op.getToken().equals(token))
                return op;
        return null;
    }
}