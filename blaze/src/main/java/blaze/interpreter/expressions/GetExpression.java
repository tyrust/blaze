package blaze.interpreter.expressions;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.expressions.literals.KeyValueMap;

/**
 * Gets a value for a particular key from a key-value map.
 */
public class GetExpression extends BlazeExpression {
    private BlazeExpression _map;
    private BlazeExpression _key;
    private Identifier _fieldKey;
    
    public GetExpression(BlazeExpression map, BlazeExpression key) {
        _map = map;
        _key = key;
    }
    
    public GetExpression(BlazeExpression map, Identifier fieldKey) {
        _map = map;
        _fieldKey = fieldKey;
    }
    
    public PutExpression convertToPutExpression(BlazeExpression value) {
        if(_key != null)
            return new PutExpression(_map, _key, value);
        else
            return new PutExpression(_map, _fieldKey, value);
    }
    
    protected BlazeExpression evaluateInternal(Environment context) throws RuntimeError, Signal {
        BlazeExpression map = _map.evaluate(context);
        
        if(_fieldKey != null) {
            if(map instanceof BlazeObject)
                return ((BlazeObject)map).getField(_fieldKey);
            else
                throw new RuntimeError(map + " is not a Blaze object; cannot access its fields");
        }
        
        if(map instanceof KeyValueMap)
            return ((KeyValueMap)map).get(_key.evaluate(context));
        throw new RuntimeError(map.toString() + " does not support key lookup");
    }
    
    public String getNodeType() {
        if(_fieldKey != null)
            return SyntaxTreeTypes.GET_FIELD;
        return SyntaxTreeTypes.GET;
    }
    
    public String toString() {
        if(_fieldKey != null)
            return String.format("%s.%s", _map.toString(), _fieldKey.toString());
        return String.format("%s[%s]", _map.toString(), _key.toString());
    }
}