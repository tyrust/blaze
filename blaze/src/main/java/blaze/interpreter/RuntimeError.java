package blaze.interpreter;

import blaze.interpreter.environment.Environment;

/**
 * A generic runtime error.
 */
public class RuntimeError extends Exception {
    private Environment _context;
    
    public RuntimeError(String reason) {
        this(reason, null);
    }
    
    public RuntimeError(String reason, Environment context) {
        super(reason);
        _context = context;
    }
    
    /**
     * @return The context in which this error occurred.
     */
    public Environment getContextEnvironment() {
        return _context;
    }
}