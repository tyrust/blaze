package blaze.interpreter.environment;

import blaze.grammar.ParseEngine;
import blaze.grammar.SyntaxError;
import blaze.interpreter.InterpreterEngine;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.literals.ParallelLiteral;
import blaze.interpreter.RuntimeError;
import blaze.interpreter.signals.Signal;

import blaze.runtime.Configuration;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;

import java.io.File;
import java.io.Serializable;

/**
 * The environment model of lexical scoping.
 * Essentially holds a set of variable bindings and a reference to the enclosing environment.
 * Additionally, our environments include imported native files and some callstack information.
 * Environments also contain a lot of contextual information, including lock stacks for "with"
 * statements, parallel extensions for concurrency via the "multiverse" model we've devised.
 * Includes a great deal of caching for performance reasons and for making multiverses work.
 */
public class Environment implements Serializable {
    private Environment _parent;
    private Set<List<String>> _nativeImports = new HashSet<List<String>>();
    private Map<String, List<String>> _nativeSymbolImports = new HashMap<String, List<String>>();
    private Map<Identifier, BlazeExpression> _bindings = new HashMap<Identifier, BlazeExpression>();
    private Set<String> _importedPaths = new HashSet<String>();
    private Map<BlazeExpression, Object> _cache = new HashMap<BlazeExpression, Object>();
    private Map<Identifier, BlazeExpression> _sidewaysCache = new HashMap<Identifier, BlazeExpression>();
    private Set<Environment> _parallelExtensions = new HashSet<Environment>();
    private boolean _isParallelExtension = false;
    
    public Environment() { }
    
    /**
     * Imports a native package; i.e. of the form java.io.*
     * @param   packageDecl     The package declaration, in the form of a list of component strings.
     */
    public void importNativePackage(List<String> packageDecl) {
        _nativeImports.add(packageDecl);
    }
    
    /**
     * Imports a native symbol, such as java.io.BufferedReader
     * @param   symbolDecl      The symbol declaration, in the form of a list of component strings.
     */
    public void importNativeSymbol(List<String> symbolDecl) throws RuntimeError {
        String symbolName = symbolDecl.get(symbolDecl.size()-1);
        if(_nativeSymbolImports.get(symbolName) != null)
            throw new RuntimeError("native import " + symbolDecl + " conflicts with existing native import: " + _nativeSymbolImports.get(symbolName));
        _nativeSymbolImports.put(symbolName, symbolDecl);
    }
    
    /**
     * Imports a builtin Blaze library package.
     * @param   name            The name of the library package.
     */
    public void importLibraryPackage(String name) throws RuntimeError {
        String path = Configuration.getCompiledPathForLibraryPackage(name);
        if(new File(path).exists())
            importFile(path);
        else {
            path = Configuration.getPathForLibraryPackage(name);
            if(new File(path).exists())
                importFile(path);
            else
                throw new RuntimeError("package " + name + " does not exist");
        }
    }
    
    /**
     * Imports a code file as a library.
     * @param   filePath        Path to the file to import.
     */
    public void importFile(String filePath) throws RuntimeError {
        if(_importedPaths.contains(filePath))
            return;
        try {
            String absolutePath = Configuration.getAbsolutePath(filePath);
            File file = new File(absolutePath);
            if(file.exists()) {
                Configuration.pushWorkingDirectory(file.getParent());
                BlazeExpression lib = ParseEngine.parseCodeFile(absolutePath);
                InterpreterEngine.runProgram(lib, this);
                _importedPaths.add(filePath);
                Configuration.popWorkingDirectory();
            } else
                throw new RuntimeError("cannot open file at " + absolutePath);
        } catch(SyntaxError err) {
            err.printStackTrace();
            throw new RuntimeError("imported file at " + filePath + " contains a syntax error");
        } catch(Signal sig) {
            // discard this signal; imported files cannot signal outside of themselves
            sig.printStackTrace();
        }
    }
    
    /**
     * @return  The symbol declaration for a given symbol from the native import table, if it exists.
     *          null if not found.
     */
    public List<String> getDeclarationForNativeSymbol(String symbol) {
        List<String> decl = _nativeSymbolImports.get(symbol);
        if(decl == null) {
            if(getParent() == null)
                return null;
            else
                return getParent().getDeclarationForNativeSymbol(symbol);
        }
        return decl;
    }
    
    /**
     * @return the list of native package imports in this environment.
     */
    public Set<List<String>> getNativePackageImports() {
        Set<List<String>> imports = new HashSet<List<String>>(_nativeImports);
        if(getParent() != null)
            imports.addAll(getParent().getNativePackageImports());
        return imports;
    }
    
    /**
     * @param   expression      The expression to search for in the cache.
     * @return  The cached return value, or null if not found.
     */
    public Object getCachedReturnValue(BlazeExpression expression) {
        Object cached = _cache.get(expression);
        if(cached == null && getParent() != null)
            return getParent().getCachedReturnValue(expression);
        return cached;
    }
    
    /**
     * @param   expression      The expression that was evaluated.
     * @param   returnValue     The result of evaluating the expression.
     */
    public void cacheReturnValue(BlazeExpression expression, Object returnValue) {
        _cache.put(expression, returnValue);
    }
    
    /**
     * Binds an identifier to an expression.
     * @param   id      The identifier for this binding.
     * @param   expr    The expression that this identifier is bound to in this scope.
     */
    public void bind(Identifier id, BlazeExpression expr) throws RuntimeError {
        if(id.isLimitedToLocalEnvironment() || _isParallelExtension) {
            // just set the binding, don't bother recursing upward
            _bindings.put(id, expr);
            return;
        }
        
        // check if this binding exists in a parent
        Environment nearest = find(id);
        if(nearest == null)
            // if not, we will just create it
            nearest = this;
        
        nearest._bindings.put(id, expr);
    }
    
    /**
     * Returns the nearest environment containing an identifier.
     * Returns null if the identifier is not bound at all.
     * @return The nearest environment containing an identifier.
     */
    private Environment find(Identifier id) {
        if(_bindings.get(id) != null)
            return this;
        if(getParent() != null)
            return getParent().find(id);
        return null;
    }
    
    /**
     * The parallel equivalent of an environment lookup.
     * Searches through the environment and its parallel extensions for a value, and collapses all of the values
     * an ID can take on in these multiverses into a single parallel literal.
     * See parallelExtend() and collapseParallelExtensions() for more explanation on how this model works.
     * @param   id      An identifier to search for.
     * @return          An expression for the value, possibly a parallel literal or null if not found.
     */
    public BlazeExpression lookSideways(Identifier id) {
        BlazeExpression cached = _sidewaysCache.get(id);
        if(cached != null)
            return cached;
        
        BlazeExpression expr = _bindings.get(id);
        if(expr != null)
            return expr;
        
        List<BlazeExpression> possibleValues = new LinkedList<BlazeExpression>();
        for(Environment parallelExtension : _parallelExtensions) {
            expr = parallelExtension.lookSideways(id);
            if(expr != null)
                possibleValues.add(expr);
        }
        if(possibleValues.size() == 0)
            return null;
        cached = new ParallelLiteral(possibleValues);
        _sidewaysCache.put(id, cached);
        return cached;
    }
    
    /**
     * Searches for the binding for an identifier in the environment.
     * Constant time lookup within each environment, and traverses the parent hierarchy until it is found.
     * Raises a runtime error if the binding is not found.
     * @param   id      The identifier to search for.
     * @return          The bound expression for the identifier, if it exists.
     */
    public BlazeExpression lookup(Identifier id) throws RuntimeError {
        // First, try finding the symbol in the symbol table
        // or in the parallel extensions of this environment.
        BlazeExpression expr = lookSideways(id); //_bindings.get(id);
        // BlazeExpression expr = _bindings.get(id);
        if(expr == null) {
            // If that fails, try the parents.
            if(getParent() == null)
                throw new RuntimeError(String.format("symbol '%s' not found", id.toString()));
            return getParent().lookup(id);
        }
        return expr;
    }
    
    /**
     * Extends this environment and creates a child environment that inherits all of the bindings from this environment.
     * @return  A new environment whose parent is this environment.
     */
    public Environment extend() {
        Environment child = new Environment();
        child._parent = this;
        return child;
    }
    
    /**
     * Extends the environment "sideways" as opposed to "downward." This is the "multiverse" concept.
     * Creates a parallel extension of the environment, which can be thought of as the same environment in a different
     * "dimension." The parallel extension inherits all of the bindings/hierarchy of this environment, but can change
     * its bindings at a later time. (See collectParallelExtensions() for an explanation of how these changes across
     * parallel extensions can be worked with from the primary environment).
     */
    public Environment parallelExtend() {
        Environment env = extend();
        env._isParallelExtension = true;
        _parallelExtensions.add(env);
        return env;
    }
    
    /**
     * Collapses the parallel extensions of an environment into the primary environment.
     * From an abstract perspective, the environment can still be thought of as existant in multiple "planes." However,
     * in terms of the data structures, the parallel extensions of the environment have their bindings "bubbled up" into
     * the main environment. Bindings in the parallel extensions overwrite those in the main environment (though these
     * environments are "children" in the data structure sense, it is more appropriate to think of them as the same
     * environment in different dimensions), and are collapsed into a parallel literal, representing the fact that the
     * values for a binding are different in different environments.
     */
    public void collectParallelExtensions() throws RuntimeError {
        Map<Identifier, List<BlazeExpression>> parallelBindings = new HashMap<Identifier, List<BlazeExpression>>();
        
        for(Environment parallelEnv : _parallelExtensions) {
            for(Identifier id : parallelEnv._bindings.keySet()) {
                if(parallelBindings.get(id) == null)
                    parallelBindings.put(id, new LinkedList<BlazeExpression>());
                parallelBindings.get(id).add(parallelEnv.lookup(id));
            }
        }
        
        for(Identifier id : parallelBindings.keySet())
            bind(id, new ParallelLiteral(parallelBindings.get(id)));
        
        _parallelExtensions.clear();
    }
    
    /**
     * @return The parent of this environment.
     */
    public Environment getParent() {
        return _parent;
    }
}