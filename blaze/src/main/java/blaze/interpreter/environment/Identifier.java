package blaze.interpreter.environment;

import java.io.Serializable;

/**
 * Represents a variable name or other identifier
 */
public class Identifier implements Serializable {
    private String _id;
    private boolean _forceLocal;
    
    public Identifier(String id) {
        this(id, false);
    }
    
    public Identifier(String id, boolean forceLocal) {
        _id = id;
        _forceLocal = forceLocal;
    }
    
    public boolean isLimitedToLocalEnvironment() {
        return _forceLocal;
    }
    
    public int hashCode() {
        return _id.hashCode();
    }
    
    public boolean equals(Object other) {
        return other instanceof Identifier && ((Identifier)other)._id.equals(_id);
    }
    
    public String toString() {
        return _id;
    }
}