package blaze.interpreter;

import blaze.grammar.SyntaxTreeTypes;
import blaze.interpreter.environment.Environment;
import blaze.interpreter.environment.Identifier;
import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.expressions.literals.ClosureLiteral;
import blaze.interpreter.signals.Signal;
import blaze.interpreter.signals.BranchSignal;
import blaze.runtime.Console;

import blaze.concurrent.ConcurrencyEngine;
import blaze.concurrent.BranchActorTask;
import static blaze.concurrent.BranchActor.BranchActorMessage;

import java.util.Stack;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;

/**
 * Contains all of the context information required to interpret an expression.
 * Higher-level interface to expression evaluation.
 */
public class InterpreterEngine {
    private static Environment _globalEnvironment;
    private static Stack<ClosureLiteral> _callStack;
    private static Map<Environment, BlazeExpression> _envExprs;
    
    /**
     * Initializes the interpreter.
     * Sets up the global environment, the callstack, and imports the default packages.
     */
    public static void startup() {
        // the lang package is loaded by default
        try {
            _globalEnvironment = new Environment();
            _callStack = new Stack<ClosureLiteral>();
            _envExprs = new HashMap<Environment, BlazeExpression>();
            
            _globalEnvironment.importLibraryPackage("lang");
        } catch(RuntimeError err) {
            err.printStackTrace();
        }
    }
    
    /**
     * Runs an expression containing a parallel literal concurrently, by branching out and exploring
     * each value the parallel can take. Joins at the end of the expression and awaits all of the results.
     * Returns the list of return values.
     * @param   expression      An expression involving a particular parallel literal.
     * @param   context         The contextual information required to evaluate this expression.
     * @param   parallel        The parallel we are collapsing.
     */
    public static List<Object> evaluateConcurrently(BlazeExpression expression, List<Environment> possibleEnvironments) throws RuntimeError, Signal {
        List<BranchActorTask> tasks = new LinkedList<BranchActorTask>();
        
        for(Environment environment : possibleEnvironments)
            tasks.add(new BranchActorTask(expression, environment));
        
        Iterable<Object> returnValues = ConcurrencyEngine.runConcurrently(tasks);
        List<Object> returnMe = new LinkedList<Object>();
        
        for(Object obj : returnValues) {
            if(obj instanceof RuntimeError)
                throw (RuntimeError)obj;
            if(obj instanceof BranchActorMessage)
                continue;
            returnMe.add(obj);
        }
        
        return returnMe;
    }
    
    /**
     * Runs a single expression in the global environment.
     * @param   expr    The expression to run.
     * @return          The result of evaluating the expression.
     */
    public static BlazeExpression interpretExpression(BlazeExpression expr) throws RuntimeError, Signal {
        return expr.evaluate(_globalEnvironment);
    }
    
    /**
     * Runs a program, providing a reference to the global environment.
     * @param   program     The BlazeExpression representing the entire program.
     */
    public static void runProgram(BlazeExpression program) throws RuntimeError, Signal {
        runProgram(program, _globalEnvironment);
    }
    
    /**
     * Runs a program in a particular environment.
     * @param   program     The BlazeExpression representing the entire program.
     * @param   context     An environment in which to run the program.
     */
    public static void runProgram(BlazeExpression program, Environment context) throws RuntimeError, Signal {
        program.evaluate(context);
    }
    
    /**
     * Returns a reference to the global environment.
     * @return The global environment.
     */
    public static Environment getGlobalEnvironment() {
        return _globalEnvironment;
    }
    
    /**
     * Push a closure onto the callstack.
     * @param   closure
     */
    public static void pushClosureOnCallStack(ClosureLiteral closure) {
        _callStack.push(closure);
    }
    
    /**
     * Pop a closure off of the callstack.
     */
    public static void popClosureOffCallStack() {
        _callStack.pop();
    }
    
    /**
     * Push an expression onto the evaluated expression stack.
     */
    public static void setExpressionForEnvironment(BlazeExpression expr, Environment context) {
        _envExprs.put(context, expr);
    }
    
    /**
     * Print the current callstack.
     */
    public static void printStackTrace() {
        System.out.println("Stack trace:");
        
        while(!_callStack.isEmpty()) {
            ClosureLiteral closure = _callStack.pop();
            Identifier id = closure.getIdentifier();
            String name = id != null ? id.toString() : "(anonymous function)";
            Console.printIndentedError("in function " + name + ":");
            
            Environment env = closure.getLiveEnvironment();
            if(env != null) {
                BlazeExpression expr = _envExprs.get(env);
                if(expr != null) {
                    Console.indent();
                    Console.printIndentedError(expr.toIndentedString());
                    Console.dedent();
                }
            }
        }
        
        Console.printIndentedError("in main program:");
        BlazeExpression expr = _envExprs.get(_globalEnvironment);
        if(expr != null) {
            Console.indent();
            Console.printIndentedError(expr.toIndentedString());
            Console.dedent();
        }
        
        _envExprs.clear();
    }
}