package blaze.interpreter.signals;

import blaze.interpreter.expressions.BlazeExpression;

import java.util.List;

/**
 * Traverses between one context and another, carrying branching information.
 * Specifically, when a closure runs in parallel and ends up with a list of
 * parallel return values, a BranchValuesSignal can carry these values from the
 * context of the closure (the closure's environment) to the contex of the
 * caller (a FunctionCallExpression) which can trap the signal and retrieve the
 * parallel return values.
 */
public class BranchValuesSignal extends Signal {
    private List<BlazeExpression> _possibleValues;
    
    public BranchValuesSignal(List<BlazeExpression> possibleValues) {
        _possibleValues = possibleValues;
    }
    
    /**
     * @return The list of possible values that resulted from branching.
     */
    public List<BlazeExpression> getPossibleValues() {
        return _possibleValues;
    }
}