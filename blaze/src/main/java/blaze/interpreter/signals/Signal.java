package blaze.interpreter.signals;

/**
 * A signal that is propagated upward from the point of execution where it is sent until it is trapped.
 * Essentially an exception but without the negative connotation.
 */
public abstract class Signal extends Exception { }