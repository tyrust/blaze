package blaze.interpreter.signals;

import blaze.interpreter.expressions.BlazeExpression;

/**
 * Indicates that the current function has returned a value.
 * The function must halt execution here and propagate the return value up the callstack.
 */
public class ReturnSignal extends Signal {
    BlazeExpression _returnValue;
    
    public ReturnSignal(BlazeExpression returnValue) {
        _returnValue = returnValue;
    }
    
    /**
     * @return The associated return value.
     */
    public BlazeExpression getReturnValue() {
        return _returnValue;
    }
    
    public String toString() {
        return "SIGRETURN[" + _returnValue + "]";
    }
}