package blaze.interpreter.signals;

import blaze.interpreter.expressions.BlazeExpression;
import blaze.interpreter.environment.Environment;

import java.util.List;
import java.util.ArrayList;

/**
 * Indicates that the current expression contains a Parallel that needs to be branched.
 * The function must halt execution here and propagate the signal up the callstack.
 */
public class BranchSignal extends Signal {
    private List<Environment> _parallelEnvironments;
    private List<ReturnSignal> _returnSignals;
    
    public BranchSignal(List<Environment> environments) {
        this(environments, new ArrayList<ReturnSignal>());
    }
    
    public BranchSignal(List<Environment> environments, List<ReturnSignal> returnSignals) {
        _parallelEnvironments = environments;
        _returnSignals = returnSignals;
    }
    
    /**
     * @return The associated parallel environments.
     */
    public List<Environment> getParallelEnvironments() {
        return _parallelEnvironments;
    }
    
    public List<ReturnSignal> getReturnSignals() {
        return _returnSignals;
    }
    
    public String toString() {
        return String.format("SIGBRANCH[%d envs]", _parallelEnvironments.size());
    }
}