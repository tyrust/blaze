import sbt._
import sbt.Keys._
import akka.sbt.AkkaKernelPlugin

object BlazeBuild extends Build {

  lazy val blaze = Project(
    id = "blaze",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := "blaze",
      organization := "blaze",
      version := "0.1-SNAPSHOT",
      scalaVersion := "2.10.0",
      resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases",
      javacOptions ++= Seq("-Xlint:unchecked"),
      libraryDependencies ++= Dependencies.blaze
    ) ++ AkkaKernelPlugin.distSettings
  )
  
  object Dependencies {
    import Dependency._
    val blaze = Seq(akkaActor, akkaKernel, akkaSlf4j, logback, antlr, jline, commons, cli)
  }
  
  object Dependency {
    object V {
        val Akka = "2.2-M1"
    }
    
    val akkaActor   = "com.typesafe.akka" % "akka-actor_2.10"   % V.Akka
    val akkaKernel  = "com.typesafe.akka" % "akka-kernel_2.10"  % V.Akka
    val akkaSlf4j   = "com.typesafe.akka" % "akka-slf4j_2.10"   % V.Akka
    val logback     = "ch.qos.logback"    % "logback-classic"   % "1.0.0"
    val antlr       = "org.antlr"         % "antlr4"            % "4.0"
    val jline       = "jline"             % "jline"             % "2.10"
    val commons     = "org.apache.commons" % "commons-lang3" % "3.1"
    val cli         = "commons-cli"       % "commons-cli"       % "1.2"
  }
}
