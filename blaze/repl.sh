#!/bin/bash
set -e
sbt dist
java -cp target/dist/deploy/*:target/dist/lib/* blaze.Blaze --repl