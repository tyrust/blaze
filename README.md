# Blaze

A cool new interpreted language, written in Java + Akka, that makes concurrent programming a breeze!