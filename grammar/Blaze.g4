grammar Blaze;

options {
    language = Java;
}

@header { package blaze.grammar; }

program:    body ;
body:       (separator? statement (separator statement)* separator?)? ;
block:      (separator? statement (separator statement)* separator?)? ;
blockOrOneLiner: separator? ('{' block '}' | statement) separator? ;

// FIXME: I have no idea why this has to be a parser rule
// instead of a lexer rule, but it won't work if it's
// a lexer rule...?
separator
    :   ('\r'? '\n')+
    |   ';'+
    ;

statement
    :   importStatement
    |   functionDeclaration
    |   ifStatement
    |   loopStatement
    |   withStatement
    |   returnStatement
    |   freezeStatement
    |   deleteStatement
    |   whenStatement
    |   push
    |   expression
    ;

importStatement
    locals [ boolean hasWildcard = false ]
    :   'import' 'native' nativePackage=packageDeclaration
            ('.' '*' { $hasWildcard = true; })?                 # NativeImport
    |   'import' path=STRING                                    # RelativeImport
    |   'import' path=~('"'|'\''|'\r'|'\n')+                    # LibraryImport
    ;

ifStatement
    :   'if' expression consequent=blockOrOneLiner
        ('else' alternative=blockOrOneLiner)?
    ;

loopStatement
    :   'for' var=ID in=ID expression blockOrOneLiner
            { $in.getText().equals("in") }?             # ForLoop
    |   'while' expression blockOrOneLiner              # WhileLoop
    ;

withStatement
    :   'with' expression blockOrOneLiner
    ;

whenStatement
    :   'when' ID '<-' expression blockOrOneLiner
    ;

returnStatement
    :   'return' expression?
    ;

freezeStatement
    :   'freeze' expression
    ;

deleteStatement
    :   'delete' variable '.' ID                # FieldDelete
    |   'delete' variable '[' expression ']'    # IndexDelete
    ;

packageDeclaration
    :   ID ('.' ID)*
    ;

expression
    :                   operator=('+'|'-'|'not')     rhs=expression
    |   lhs=expression  operator=('*'|'/'|'%')       rhs=expression
    |   lhs=expression  operator=('+'|'-')           rhs=expression
    |   lhs=expression  operator=('<'|'>'|'<='|'>=') rhs=expression
    |   lhs=expression  operator=('=='|'!=')         rhs=expression
    |   lhs=expression  operator='and'               rhs=expression
    |   lhs=expression  operator='or'                rhs=expression
    |   assignment
    |   instantiation
    |   atom
    |   lambda
    ;

literal
    :   INT
    |   FLOAT
    |   STRING
    |   BOOL
    |   listLiteral
    |   dictLiteral
    |   parallelLiteral
    |   NULL
    ;

listLiteral:    'list'? '[' (expression (',' expression)*)? ']' ;
dictLiteral:    'dict'? '[' (keys+=expression ':' values+=expression (',' keys+=expression ':' values+=expression)*)? ']' ;
parallelLiteral:    '|' (expression (',' expression)*)? '|' ;

functionDeclaration
    :   variable '(' argumentList ')' '->' separator? '{' body '}'
    |   variable '(' argumentList ')' '->' separator? expression
    ;

lambda
    :   argumentList '->' separator? '{' body '}'   # FullLambda
    |   argumentList '->' separator? expression     # OneLineLambda
    |   '{' body '}'                                # ZeroArgumentLambda
    ;

meltedCopy
    :   'melted' '(' expression ')'
    ;

typecast
    :   blazeType '(' expression ')'
    ;

argumentList
    :   (args+=ID (',' args+=ID)*)? (',' vararg=ID '...')?
    |   '(' (args+=ID (',' args+=ID)*)? (',' vararg=ID '...')? ')'
    |   vararg=ID '...'
    |   '(' vararg=ID '...' ')'
    ;

push
    :   channel=expression '<-' values+=expression (',' values+=expression)*
    ;

assignment
    :   blazeType '.' 'prototype' '.' ID '=' expression             # PrototypeAssignment
    |   variable '=' expression                                     # VariableAssignment
    |   variable operator=('+='|'-='|'*='|'/='|'%=') expression     # VariableCompoundAssignment
    |   'local' ID '=' expression                                   # LocalVariableAssignment
    ;

indexable
    :   '(' expression ')'              # ExpressionIndexable
    |   typecast                        # TypecastIndexable
    |   literal                         # LiteralIndexable
    |   ID                              # DereferenceIndexable
    |   nativeReference                 # NativeIndexable
    ;

nativeReference
    :   'native' packageDeclaration
    ;

variable
    :   variable '.' ID                 # FieldLookupVariable
    |   variable '[' expression ']'     # IndexLookupVariable
    |   indexable                       # IndexableVariable
    ;

callable
    :   '(' expression ')'
    |   parallelize
    |   variable
    ;

callParameters
    :   '(' (expression (',' expression)*)? ')'
    ;

parallelize
    :   '/' expression '/'
    ;

call
    :   callable '.' ID callParameters  # FieldCall
    |   callable callParameters         # CallableCall
    |   nativeReference callParameters  # NativeCall
    |   call callParameters             # MultiCall
    |   call '.' ID callParameters      # ChainCall
    ;

instantiation
    :   'new' nativeReference callParameters?   # NativeInstantiation
    ;

atom
    :   meltedCopy
    |   typecast
    |   parallelize
    |   call
    |   variable
    |   'new' channel=ID { $channel.getText().equals("Channel") }?
    |   '(' expression ')'
    ;

blazeType
    :   'str'
    |   'int'
    |   'float'
    |   'dict'
    |   'list'
    |   'bool'
    |   'nulltype'
    |   'closure'
    |   'native'
    |   '__blaze_literal'
    ;

/** LEXER RULES **/

INT:    DIGIT+ ;

FLOAT
    :   DIGIT* '.' DIGIT+
//    |   DIGIT+ '.'
    ;

BOOL
    :   'true'
    |   'false'
    ;

STRING
    :   '"' (ESCAPE_SEQ | ~["\\\r\n])* '"'
    |   '\'' (ESCAPE_SEQ | ~('\\'|'\''|'\r'|'\n'))* '\''
    ;

NULL:   'null';
ID:     [a-zA-Z_][a-zA-Z0-9_]* ;

WHITESPACE: [ \t]+                      -> channel(HIDDEN) ;
COMMENT:    '/*' .*? '*/'               -> skip ;
LINE_COMMENT: '//' ~('\r'|'\n')*        -> skip ;
ESCAPED_NEWLINE: '\\' ('\r'? '\n')      -> skip ;

fragment DIGIT:         [0-9] ;
fragment ESCAPE_SEQ:    '\\' ([nrtbf"\\]|'\'') ;
